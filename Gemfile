source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', :git => 'https://github.com/rails/rails.git', :branch => '4-0-stable'

# Use sqlite3 as the database for Active Record
gem 'sqlite3'
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
gem 'therubyracer'
gem 'less-rails'
gem 'bourbon'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

gem 'devise', :git => 'https://github.com/plataformatec/devise.git'
gem 'activeadmin', :git => 'https://github.com/gregbell/active_admin.git'
gem 'entypo-rails'
gem 'rails-i18n', '~> 4.0.0.pre'
gem 'message_block', "~> 2.0"
gem 'wicked'
gem 'simple_form'
gem 'country_select'
gem "paperclip", :git => "git://github.com/thoughtbot/paperclip.git"
gem 'acts-as-taggable-on'
gem "cocoon"
gem 'nested_form'
gem 'ember-rails'
gem 'ember-source'
gem 'active_model_serializers', github: 'rails-api/active_model_serializers'
gem 'remotipart', '~> 1.2'
gem 'jquery-fileupload-rails'

gem 'sentient_user', git: "git://github.com/bokmann/sentient_user.git"
gem 'clerk', git: "git://github.com/house9/clerk.git"  
gem 'paranoia', :github => 'radar/paranoia', :branch => 'rails4'
# gem 'paranoia', :github => 'van5150/paranoia', :branch => 'rails4'
gem 'paper_trail', '>= 3.0.0.beta1'
# gem 'jquery-turbolinks'
# gem 'turbolinks-redirect'
# gem 'acts_as_commentable'
gem 'kaminari'
gem 'momentjs-rails'
gem 'roo'
gem 'state_machine'
gem 'impressionist', :git => 'https://github.com/charlotte-ruby/impressionist.git'
gem 'acts-as-messageable', :git => 'https://github.com/LTe/acts-as-messageable.git'
gem 'calendar_date_select'
gem 'ancestry'
gem "activeadmin-sortable-tree", :github => "nebirhos/activeadmin-sortable-tree", :branch => "master"
gem 'public_activity'
gem 'acts_as_follower'
gem "ckeditor", :git => "https://github.com/galetahub/ckeditor.git"
# gem 'pg_search'
gem 'postmark-rails', '~> 0.5.2'
# gem 'que'
# gem 'whenever', :require => false
gem 'sidekiq'
gem 'sidetiq'
gem 'imgkit'
gem 'nokogiri'
gem 'table_print'

# gem 'mongo', :git => 'https://github.com/mongodb/mongo-ruby-driver.git', :branch => '1.x-stable'
gem 'bson_ext'
gem 'mongoid', :git => 'https://github.com/mongoid/mongoid.git'

gem 'sinatra', require: false
gem 'slim'
gem 'httparty'
gem 'rmagick'
gem 'mimemagic'
gem 'awesome_print'
gem 'nested-hstore'
gem 'airbrake'
gem 'attr_encrypted'
gem 'by_star', :git => "git://github.com/radar/by_star"
gem 'browser'

gem 'acts_as_commentable_with_threading'
gem 'high_voltage', '~> 2.1.0'
gem "ransack", github: "activerecord-hackery/ransack", branch: "rails-4"
gem 'premailer-rails'
gem 'rails_email_preview', '~> 0.2.19'
gem 'fastimage'
# gem 'forty_facets'


# gem "redis", "~> 3.0.1"

# gem 'redis-objects'
# gem 'pg_array_parser'
# gem 'facets'
# gem 'surus'
# gem "mini_magick"


#gem 'carmen-rails', '~> 1.0.0', github: 'jim/carmen-rails'
#gem 'protected_attributes'
#gem 'r18n-rails'


# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
gem 'unicorn'
gem 'unicorn-worker-killer'
gem 'exception_handler'
# gem 'pghero'
# gem 'mechanize'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
