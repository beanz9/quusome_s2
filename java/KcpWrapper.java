// import com.kcp.*;
// package com.kcp;
import java.util.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class KcpWrapper {

	// public static final String g_conf_home_dir = "/vagrant_data/escrow_ax_hub_linux_jsp";
	public static final String g_conf_home_dir = "/home/quusome/quusome_s2/escrow_ax_hub_linux_jsp";

	// public static final String g_conf_gw_url = "testpaygw.kcp.co.kr";
	// public static final String g_conf_js_url = "https://pay.kcp.co.kr/plugin/payplus_test_un.js";
	// public static final String g_conf_site_cd = "T0007";
	// public static final String g_conf_site_key = "4Ho4YsuOZlLXUZUdOxM1Q7X__";

	public static final String g_conf_gw_url = "paygw.kcp.co.kr";
	public static final String g_conf_js_url = "https://pay.kcp.co.kr/plugin/payplus_un.js";
	public static final String g_conf_site_cd = "B2833";
	public static final String g_conf_site_key = "04TlEzuhh9tl7C6duXRCvnT__";

	public static final String g_conf_site_name = "QUUSOME";
	public static final String g_conf_log_level = "3";
	public static final String g_conf_gw_port = "8090";
	public static final String module_type = "01";
	public static final int g_conf_tx_mode = 0;  
	public static final boolean g_conf_server = false;

	private String req_tx;						//요청 종류
	private String tran_cd;						//처리 종류
	private String cust_ip;						//요청 IP
	private String ordr_idxx;					//쇼핑몰 주문번호
	private String good_name;					//상품명
	private String good_mny;					//결제 총금액
	private String res_cd;						//응답코드
	private String res_msg;						//응답 메세지
	private String escw_yn;						//에스크로 사용여부
	private String tno;								//KCP 거래 고유 번호
	private String vcnt_yn;						//가상계좌 에스크로 사용 유무
	private String buyr_name;					//주문자명
	private String buyr_tel1;					//주문자 전화번호
	private String buyr_tel2;					//주문자 핸드폰 번호 
	private String buyr_mail;					//주문자 E-mail 주소 
	private String mod_type;					//변경TYPE(승인취소시 필요)
	private String mod_desc;					//변경사유
	private String panc_mod_mny;			//부분취소 금액
	private String panc_rem_mny;			//부분취소 가능 금액
	private String mod_tax_mny;				//공급가 부분 취소 요청 금액
	private String mod_vat_mny; 			//부과세 부분 취소 요청 금액
	private String mod_free_mny;			//비과세 부분 취소 요청 금액
	private String use_pay_method; 		//결제 방법
	private String bSucc;							//업체 DB 처리 성공 여부
	private String app_time;					//승인시간 (모든 결제 수단 공통)
	private String amount;						//KCP 실제 거래금액
	private String total_amount;			//복합결제시 총 거래금액
	private String coupon_mny;				//쿠폰금액
	private String card_cd;						//신용카드 코드
	private String card_name;					//신용카드 명
	private String app_no;						//신용카드 승인번호
	private String noinf;							//신용카드 무이자 여부
	private String quota;							//신용카드 할부개월
	private String partcanc_yn;				//부분취소 가능유무
	private String card_bin_type_01;  //카드구분1
	private String card_bin_type_02;	//카드구분2
	private String card_mny;					//카드금액
	private String bank_name;					//은행명
	private String bank_code;					//은행코드
	private String bk_mny;						//계좌이체결제금액
	private String bankname;					//입금할 은행명
	private String depositor;					//입금할 계좌 예금주 성명
	private String account;						//입금할 계좌 번호
	private String va_date;						//가상계좌 입금마감시간
	private String pnt_issue;					//결제 포인트사 코드
	private String pnt_amount;				//적립금액 or 사용금액
	private String pnt_app_time;			//승인시간
	private String pnt_app_no;				//승인번호
	private String add_pnt;						//발생 포인트
	private String use_pnt;						//사용가능 포인트
	private String rsv_pnt; 					//총 누적 포인트
	private String commid;						//통신사코드
	private String mobile_no;					//휴대폰번호
	private String shop_user_id;			//가맹점 고객 아이디
	private String tk_van_code;				//발급사코드
	private String tk_app_no;					//승인번호
	private String cash_yn;						//현금 영수증 등록 여부
	private String cash_authno;				//현금 영수증 승인 번호
	private String cash_tr_code;			//현금 영수증 발행 구분
	private String cash_id_info;			//현금 영수증 등록 번호
	private String escw_used;					//에스크로 사용 여부
	private String pay_mod;						//에스크로 결제처리 모드
	private String deli_term;					//배송 소요일
	private String bask_cntx;					//장바구니 상품 개수
	private String good_info;					//장바구니 상품 상세 정보
	private String rcvr_name;					//수취인 이름
	private String rcvr_tel1;					//수취인 전화번호
	private String rcvr_tel2;					//수취인 휴대폰번호
	private String rcvr_mail;					//수취인 E-Mail
	private String rcvr_zipx;					//수취인 우편번호
	private String rcvr_add1;					//수취인 주소
	private String rcvr_add2;					//수취인 상세주소

	private String enc_data;
	private String enc_info;

	private String mod_mny;
	private String rem_mny;

	private String refund_account;
	private String refund_nm;
	private String deli_numb;
	private String deli_corp;


	public static void main(String[] args) {
	
		// System.out.println("===========");
		// System.out.println(args[0]);
		// System.out.print(args[0]);

		// com.kcp.C_PP_CLI c_PayPlus = new com.kcp.C_PP_CLI();
		
		// String json_str = "{\"pay_method\":\"100000000000\",\"ordr_idxx\":\"970b34dd-b856-4eb7-9419-4e9d082c7a3a\",\"good_name\":\"운동화\",\"good_mny\":\"1004\",\"buyr_name\":\"홍길동\",\"buyr_mail\":\"test@test.co.kr\",\"buyr_tel1\":\"02-2108-1000\",\"buyr_tel2\":\"010-0000-0000\",\"rcvr_name\":\"홍길순\",\"rcvr_tel1\":\"02-2108-1000\",\"rcvr_tel2\":\"010-0000-0000\",\"rcvr_mail\":\"honggilsoon@kcp.co.kr\",\"rcvr_zipx\":\"157864\",\"rcvr_add1\":\"서울시 구로구\",\"rcvr_add2\":\"170-5 우림E-biz센터\",\"req_tx\":\"pay\",\"site_cd\":\"T0000\",\"site_name\":\"Quusome\",\"quotaopt\":\"12\",\"currency\":\"WON\",\"module_type\":\"01\",\"escw_used\":\"Y\",\"pay_mod\":\"Y\",\"deli_term\":\"03\",\"bask_cntx\":\"3\",\"good_info\":\"seq=0\\u001fordr_numb=970b34dd-b856-4eb7-9419-4e9d082c7a3a-0\\u001fgood_name=Coy Story Off-the-Shoulder Ivo\\u001fgood_cntx=1\\u001fgood_amtx=70900\",\"res_cd\":\"0000\",\"res_msg\":\"정상처리\",\"tno\":\"\",\"trace_no\":\"T0000773OM16Jefc\",\"enc_info\":\"1ehdM3A0SDcZzmNlqQ5qCPykLoSckPBLsiyHokshVkYQZd38CjdVNJtYG2aadP73iCUzuOkOElhIFvtf2AnKdtkylodDAwwvgWstZUNdP1gIDeeIrTeAdey9YGf7eb8vRt6TlX995NXZEEKGDBCVRHDVxEbhEhRqbm.YWLbA-gyhkpGT0O0QwOXIQ0x.31vPLk6-J0igvXN__\",\"enc_data\":\"1ktXPpPJsCXR06kU-hBLITwM1wYcUczzExonlWrZ8rfmVDik.LYLzTYcvwQsEywGtteOSP7045zhLr1axx2-WF0vhjZh2.WpTY9-ABwRL2TNLHf.TODGxnK5XGDrFtNzFVozkSbY6QtKCJmVW4mWAP56xsgoZT8U5n7nNZGrE44sddOvfiiy0aODOBU4QDP8.Tin2xnmwJrfMy7eL7UI4b9GozdZYDrpTSxjXGigtLLWA0giaDz2SL8M7GJDO2oUqMm94kZ8ayX81t9L07nXWOKO0jnULxB-lhVAeosunEWRJ9d1o9jtlzt2r2lCLFsdG2c4HluYlL2zEZd6JxzpHIs36JMlQPF1Pd-skEjhwwrAGQdNTtSlXcCaTt2.9-9ilV1YoGNuJlf5TpUTIZS5KVgcsE1n-tS3-wvaR6knzXGtfeuCflCFUf8xThenyRDjhIJfgtPGQxftvJsRHXsd1U5QetUauzJt9H4.XGRrnwet0KGFUhmj3.QpQ9lVFCb1XdiA2IKgyCkFLxrYihF273CUcqwoW-CE9tTvFkgBcsgteLMSpMXqPx.n.Q7j9YJLIS0VkNg4-oV2qRrkqgze-ay-Fc5A0SbSK6JqKTSj1kraGYEr.DILT-cmZkvvo5wDU1ESV.tuq3ysRt.0RIRfV2uZ9RI72JDP9v4lVmzgMTKtnk78lyMvJq3Dn.66Dnt7mGT64mW1FCpFP65ae86GWCNqzESbvGxPiMLQKsrK2P4zrFDFJELftgV9s1eMg-mlMTPdnBSZATTGjcD88sf2aCXk0erWSavkEUB68dDu9UxRSrBbU1Xxdk.v8PPzKkzAAm8cxk3Jx8xUS8s4XMhGB.zSFLyJP24ri6Lm8VU5i70rD9z0q0LAqCywrMMH5aSeh-gKOO-FdbW2nj-fekYQVxWRvU0BPHZt1W5TTtk91xp7oTZ5hOXZZFhVI12IkrNUIrW8ST6LFAkLJ9rQq6YMupCV7LZcCbcAonjWjC5iGg857CeiOlmOwfEW28H9S8b3BfTJo7zSDKNVtheelAHnWMF.ltUO1B4IFxm95MR6XtLfu32B1ts0ZWMnPFk7yFuSzd9tDshVbaJ0m3fXOm.U3DqKgdmnlvridWmlGy6JwK4ol844F1rvP7SpxKSsQfBKdP5lNBQ9JhxRrbAUHx3j5lwoRUm4oPpqlcPOk9lTdI2f_\",\"ret_pay_method\":\"card\",\"tran_cd\":\"00100000\",\"bank_name\":\"\",\"bank_issu\":\"\",\"use_pay_method\":\"100000000000\",\"cash_tsdtime\":\"\",\"cash_yn\":\"\",\"cash_authno\":\"\",\"cash_tr_code\":\"\",\"cash_id_info\":\"\",\"good_expr\":\"0\",\"shop_user_id\":\"van5150\",\"pt_memcorp_cd\":\"\"}";

		// if(json_str.equals(args[0])) {
		// 	System.out.println("true");
		// 	System.out.println("true");
		// 	System.out.println("true");
		// 	System.out.println("true");
		// 	System.out.println("true");
		// }
		// String obj = "{ \"test\":\"1\" }";
		// System.out.println(json_str);

		Object obj = JSONValue.parse(args[0]);
		// System.out.println(obj);
		// System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		JSONObject json = (JSONObject)obj;
		// System.out.println(json);

		KcpWrapper k = new KcpWrapper(json);
		k.pay_execute();
		// System.out.println("|||||||||||||||||");
		
		System.out.println(k.result());
	}

	public KcpWrapper(JSONObject obj) {

		this.req_tx = f_get_parm((String)obj.get("req_tx"));
		this.tran_cd = f_get_parm((String)obj.get("tran_cd"));
		this.cust_ip = f_get_parm((String)obj.get("cust_ip"));
		this.ordr_idxx = f_get_parm((String)obj.get("ordr_idxx"));
		this.good_name = f_get_parm((String)obj.get("good_name"));
		this.good_mny = f_get_parm((String)obj.get("good_mny"));
		this.res_cd = "";
		this.res_msg = "";
		this.escw_yn = "";
		this.tno = f_get_parm((String)obj.get("tno"));
		this.vcnt_yn = f_get_parm((String)obj.get("vcnt_yn"));
		this.buyr_name = f_get_parm((String)obj.get("buyr_name"));
		this.buyr_tel1 = f_get_parm((String)obj.get("buyr_tel1"));
		this.buyr_tel2 = f_get_parm((String)obj.get("buyr_tel2"));
		this.buyr_mail = f_get_parm((String)obj.get("buyr_mail"));
		this.mod_type = f_get_parm((String)obj.get("mod_type"));
		this.mod_desc = f_get_parm((String)obj.get("mod_desc"));
		this.panc_mod_mny = "";
		this.panc_rem_mny = "";
		this.mod_tax_mny = f_get_parm((String)obj.get("mod_tax_mny"));
		this.mod_vat_mny = f_get_parm((String)obj.get("mod_vat_mny"));
		this.mod_free_mny = f_get_parm((String)obj.get("mod_free_mny"));
		this.use_pay_method = f_get_parm((String)obj.get("use_pay_method"));
		this.bSucc = "";
		this.app_time = "";
		this.amount = "";
		this.total_amount = "0";
		this.coupon_mny = "";
		this.card_cd = "";
		this.card_name = "";
		this.app_no = "";
		this.noinf = "";
		this.quota = "";
		this.partcanc_yn = "";
		this.card_bin_type_01 = "";
		this.card_bin_type_02 = "";
		this.card_mny = "";
		this.bank_name = "";
		this.depositor = "";
		this.account = "";
		this.va_date = "";
		this.pnt_issue = "";
		this.pnt_amount = "";
		this.pnt_app_time = "";
		this.pnt_app_no = "";
		this.add_pnt = "";
		this.use_pnt = "";
		this.rsv_pnt = "";
		this.commid = "";
		this.mobile_no = "";
		this.shop_user_id = f_get_parm((String)obj.get("shop_user_id"));
		this.tk_van_code = "";
		this.tk_app_no = "";
		this.cash_yn = f_get_parm((String)obj.get("cash_yn"));
		this.cash_authno = "";
		this.cash_tr_code = f_get_parm((String)obj.get("cash_tr_code"));
		this.cash_id_info = f_get_parm((String)obj.get("cash_id_info"));
		this.escw_used = f_get_parm((String)obj.get("escw_used"));
		this.pay_mod = f_get_parm((String)obj.get("pay_mod"));
		this.deli_term = f_get_parm((String)obj.get("deli_term"));
		this.bask_cntx = f_get_parm((String)obj.get("bask_cntx"));
		this.good_info = f_get_parm((String)obj.get("good_info"));
		this.rcvr_name = f_get_parm((String)obj.get("rcvr_name"));
		this.rcvr_tel1 = f_get_parm((String)obj.get("rcvr_tel1"));
		this.rcvr_tel2 = f_get_parm((String)obj.get("rcvr_tel2"));
		this.rcvr_mail = f_get_parm((String)obj.get("rcvr_mail"));
		this.rcvr_zipx = f_get_parm((String)obj.get("rcvr_zipx"));
		this.rcvr_add1 = f_get_parm((String)obj.get("rcvr_add1"));
		this.rcvr_add2 = f_get_parm((String)obj.get("rcvr_add2"));

		this.enc_data = f_get_parm((String)obj.get("enc_data"));
		this.enc_info = f_get_parm((String)obj.get("enc_info"));

		this.refund_account = f_get_parm((String)obj.get("refund_account"));
		this.refund_nm = f_get_parm((String)obj.get("refund_nm"));
		this.deli_numb = f_get_parm((String)obj.get("deli_numb"));
		this.deli_corp = f_get_parm((String)obj.get("deli_corp"));
	}  

	public void pay_execute() {
		/* ============================================================================== */
    /* =   02. 인스턴스 생성 및 초기화(변경 불가)                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   결제에 필요한 인스턴스를 생성하고 초기화 합니다.                         = */
    /* =   ※ 주의 ※ 이 부분은 변경하지 마십시오                                   = */
    /* = -------------------------------------------------------------------------- = */

    com.kcp.C_PP_CLI c_PayPlus = new com.kcp.C_PP_CLI();

    c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_tx_mode );
    c_PayPlus.mf_init_set();

     /*
    윈도우버전과 유닉스 버전은 샘플이 다르기 때문에 반드시 OS별 샘플을 구분하여 다운로드하시기 바랍니다. 
    Windows 의 경우 : pp_cli_exe.exe 파일 이용 
    UNIX 계열의 경우 : pp_cli 파일 이용 

    pp_ax_hub 페이지에서 mf_init 하는 인자값 개수 상이 
    // Unix / Linux Platform 
    c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_tx_mode ); 
    // Windows Platform 
    c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_key_dir, g_conf_log_dir, g_conf_tx_mode );
    */

    /* = -------------------------------------------------------------------------- = */
    /* =   02. 인스턴스 생성 및 초기화 END                                          = */
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   03. 처리 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */
    /* = -------------------------------------------------------------------------- = */
    /* =   03-1. 승인 요청 정보 설정                                                = */
    /* = -------------------------------------------------------------------------- = */

    if (req_tx.equals("pay")) {
    	c_PayPlus.mf_set_enc_data(this.enc_data, this.enc_info);

      /* 1004원은 실제로 업체에서 결제하셔야 될 원 금액을 넣어주셔야 합니다. 결제금액 유효성 검증
      if(good_mny.trim().length() > 0)
      {
          int ordr_data_set_no;

          ordr_data_set_no = c_PayPlus.mf_add_set( "ordr_data" );

          c_PayPlus.mf_set_us( ordr_data_set_no, "ordr_mony", good_mny );
          c_PayPlus.mf_set_us( ordr_data_set_no, "ordr_idxx", ordr_idxx );
      }
      */
      if(good_mny.trim().length() > 0)
      {
          int ordr_data_set_no;

          ordr_data_set_no = c_PayPlus.mf_add_set( "ordr_data" );

          c_PayPlus.mf_set_us( ordr_data_set_no, "ordr_mony", good_mny );
          c_PayPlus.mf_set_us( ordr_data_set_no, "ordr_idxx", ordr_idxx );
      }
    }

    /* = -------------------------------------------------------------------------- = */
    /* =   03-2. 취소 및 매입 요청 정보 설정                                        = */
    /* = -------------------------------------------------------------------------- = */

    else if (req_tx.equals("mod")) {
      int mod_data_set_no;

      tran_cd = "00200000";
      mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

      c_PayPlus.mf_set_us( mod_data_set_no, "tno",        this.tno       ); // KCP 원거래 거래번호
      c_PayPlus.mf_set_us( mod_data_set_no, "mod_type",   mod_type                           ); // 원거래 변경 요청 종류
      c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip",     cust_ip                            ); // 변경 요청자 IP
      c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc",   this.mod_desc  ); // 변경 사유

      if ( mod_type.equals( "STPC" ) ) // 부분취소의 경우
      {
          c_PayPlus.mf_set_us( mod_data_set_no, "mod_mny", this.mod_mny ); // 취소요청금액
          c_PayPlus.mf_set_us( mod_data_set_no, "rem_mny", this.rem_mny ); // 취소가능잔액

          //복합거래 부분 취소시 주석을 풀어 주시기 바랍니다.
          //c_PayPlus.mf_set_us( mod_data_set_no, "tax_flag",     "TG03"                       ); // 복합과세 구분
          //c_PayPlus.mf_set_us( mod_data_set_no, "mod_tax_mny",  mod_tax_mny                  ); // 공급가 부분 취소 요청 금액
          //c_PayPlus.mf_set_us( mod_data_set_no, "mod_vat_mny",  mod_vat_mny                  ); // 부과세 부분 취소 요청 금액
          //c_PayPlus.mf_set_us( mod_data_set_no, "mod_free_mny", mod_free_mny                 ); // 비과세 부분 취소 요청 금액
      }
		}

		/* = -------------------------------------------------------------------------- = */
    /* =   03-3. 에스크로 상태변경 요청                                             = */
    /* = -------------------------------------------------------------------------- = */

    else if ( req_tx.equals( "mod_escrow" ) )
    {
        int     mod_data_set_no;

        tran_cd = "00200000";
        mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

        c_PayPlus.mf_set_us( mod_data_set_no, "tno",        this.tno );      // KCP 원거래 거래번호
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_type",   mod_type                            );      // 원거래 변경 요청 종류
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip",     cust_ip                             );      // 변경 요청자 IP
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc",   this.mod_desc );      // 변경 사유

        if ( mod_type.equals("STE1") )                                                                  // 상태변경 타입이 [배송요청]인 경우
        {
            c_PayPlus.mf_set_us( mod_data_set_no, "deli_numb", this.deli_numb );   // 운송장 번호
            c_PayPlus.mf_set_us( mod_data_set_no, "deli_corp", this.deli_corp );   // 택배 업체명
        }

        if ( mod_type.equals("STE2") || mod_type.equals("STE4") )                                       // 상태변경 타입이 [즉시취소] 또는 [취소]인 계좌이체, 가상계좌의 경우
        {
            if ( vcnt_yn.equals("Y") )
            {
                c_PayPlus.mf_set_us( mod_data_set_no, "refund_account", this.refund_account );  // 환불수취계좌번호
                c_PayPlus.mf_set_us( mod_data_set_no, "refund_nm",      this.refund_nm );  // 환불수취계좌주명
                c_PayPlus.mf_set_us( mod_data_set_no, "bank_code",      this.bank_code );  // 환불수취은행코드
            }
        }
    }

    /* = -------------------------------------------------------------------------- = */
    /* =   03. 에스크로 상태변경 요청 END                                           = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   04. 실행                                                                 = */
    /* = -------------------------------------------------------------------------- = */
    if ( tran_cd.length() > 0 )
    {
        c_PayPlus.mf_do_tx( g_conf_site_cd, g_conf_site_key, tran_cd, "", ordr_idxx, g_conf_log_level, "0" );

        res_cd  = c_PayPlus.m_res_cd;  // 결과 코드
        res_msg = c_PayPlus.m_res_msg; // 결과 메시지
       
    }
    else
    {
        c_PayPlus.m_res_cd  = "9562";
        c_PayPlus.m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
    }

    /* = -------------------------------------------------------------------------- = */
    /* =   04. 실행 END                                                             = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   06. 승인 결과 값 추출                                                    = */
    /* = -------------------------------------------------------------------------- = */
    if ( req_tx.equals( "pay" ) )
    {
        if ( res_cd.equals( "0000" ) )
        {
            tno    = c_PayPlus.mf_get_res( "tno"    ); // KCP 거래 고유 번호
            amount = c_PayPlus.mf_get_res( "amount" ); // KCP 실제 거래 금액
            pnt_issue = c_PayPlus.mf_get_res( "pnt_issue" ); // 결제 포인트사 코드
            coupon_mny = c_PayPlus.mf_get_res( "coupon_mny" ); // 쿠폰금액

    /* = -------------------------------------------------------------------------- = */
    /* =   06-1. 신용카드 승인 결과 처리                                            = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals( "100000000000" ) )
            {
                card_cd   = c_PayPlus.mf_get_res( "card_cd"   ); // 카드사 코드
                card_name = c_PayPlus.mf_get_res( "card_name" ); // 카드사 명
                app_time  = c_PayPlus.mf_get_res( "app_time"  ); // 승인시간
                app_no    = c_PayPlus.mf_get_res( "app_no"    ); // 승인번호
                noinf     = c_PayPlus.mf_get_res( "noinf"     ); // 무이자 여부
                quota     = c_PayPlus.mf_get_res( "quota"     ); // 할부 개월 수
                partcanc_yn = c_PayPlus.mf_get_res( "partcanc_yn" ); // 부분취소 가능유무
                card_bin_type_01 = c_PayPlus.mf_get_res( "card_bin_type_01" ); // 카드구분1
                card_bin_type_02 = c_PayPlus.mf_get_res( "card_bin_type_02" ); // 카드구분2
                card_mny = c_PayPlus.mf_get_res( "card_mny" ); // 카드결제금액

                /* = -------------------------------------------------------------- = */
                /* =   06-1.1. 복합결제(포인트+신용카드) 승인 결과 처리             = */
                /* = -------------------------------------------------------------- = */
                if ( pnt_issue.equals( "SCSK" ) || pnt_issue.equals( "SCWB" ) )
                {
                    pnt_amount   = c_PayPlus.mf_get_res( "pnt_amount"   ); // 적립금액 or 사용금액
                    pnt_app_time = c_PayPlus.mf_get_res( "pnt_app_time" ); // 승인시간
                    pnt_app_no   = c_PayPlus.mf_get_res( "pnt_app_no"   ); // 승인번호
                    add_pnt      = c_PayPlus.mf_get_res( "add_pnt"      ); // 발생 포인트
                    use_pnt      = c_PayPlus.mf_get_res( "use_pnt"      ); // 사용가능 포인트
                    rsv_pnt      = c_PayPlus.mf_get_res( "rsv_pnt"      ); // 총 누적 포인트
                    total_amount = amount + pnt_amount;                    // 복합결제시 총 거래금액
                }
            }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-2. 계좌이체 승인 결과 처리                                            = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals("010000000000") )
            {
                app_time  = c_PayPlus.mf_get_res( "app_time"  ); // 승인시간
                bank_name = c_PayPlus.mf_get_res( "bank_name" ); // 은행명
                bank_code = c_PayPlus.mf_get_res( "bank_code" ); // 은행코드
                bk_mny    = c_PayPlus.mf_get_res( "bk_mny"    ); // 계좌이체결제금액
            }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-3. 가상계좌 승인 결과 처리                                            = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals( "001000000000" ) )
            {
                bankname  = c_PayPlus.mf_get_res( "bankname"  ); // 입금할 은행 이름
                depositor = c_PayPlus.mf_get_res( "depositor" ); // 입금할 계좌 예금주
                account   = c_PayPlus.mf_get_res( "account"   ); // 입금할 계좌 번호
                va_date   = c_PayPlus.mf_get_res( "va_date"   ); // 가상계좌 입금마감시간
            }

	/* = -------------------------------------------------------------------------- = */
    /* =   06-4. 포인트 승인 결과 처리                                              = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals( "000100000000" ) )
            {
                pnt_amount   = c_PayPlus.mf_get_res( "pnt_amount"   ); // 적립금액 or 사용금액
	            pnt_app_time = c_PayPlus.mf_get_res( "pnt_app_time" ); // 승인시간
	            pnt_app_no   = c_PayPlus.mf_get_res( "pnt_app_no"   ); // 승인번호
	            add_pnt      = c_PayPlus.mf_get_res( "add_pnt"      ); // 발생 포인트
	            use_pnt      = c_PayPlus.mf_get_res( "use_pnt"      ); // 사용가능 포인트
                rsv_pnt      = c_PayPlus.mf_get_res( "rsv_pnt"      ); // 총 누적 포인트
            }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-5. 휴대폰 승인 결과 처리                                              = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals( "000010000000" ) )
            {
                app_time = c_PayPlus.mf_get_res( "hp_app_time" ); // 승인 시간
                commid	 = c_PayPlus.mf_get_res( "commid"	   ); // 통신사 코드
                mobile_no= c_PayPlus.mf_get_res( "mobile_no"   ); // 휴대폰 번호
            }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-6. 상품권 승인 결과 처리                                              = */
    /* = -------------------------------------------------------------------------- = */
            if ( use_pay_method.equals( "000000001000" ) )
            {
                app_time    = c_PayPlus.mf_get_res( "tk_app_time" ); // 승인 시간
                tk_van_code = c_PayPlus.mf_get_res( "tk_van_code" ); // 발급사 코드
                tk_app_no   = c_PayPlus.mf_get_res( "tk_app_no"   ); // 승인 번호
            }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-7. 현금영수증 승인 결과 처리                                          = */
    /* = -------------------------------------------------------------------------- = */
            cash_authno = c_PayPlus.mf_get_res( "cash_authno" ); // 현금영수증 승인번호
        }

    /* = -------------------------------------------------------------------------- = */
    /* =   06-8. 에스크로 여부 결과 처리                                            = */
    /* = -------------------------------------------------------------------------- = */
        escw_yn  = c_PayPlus.mf_get_res( "escw_yn" ); // 에스크로 여부
    }
    /* = -------------------------------------------------------------------------- = */
    /* =   06-9. 부분취소 결과 처리                                                 = */
    /* = -------------------------------------------------------------------------- = */
    if ( req_tx.equals( "mod" ) )
    {
        if ( res_cd.equals( "0000" ) )
        {
            if ( mod_type.equals ( "STPC") )
            {
                amount		 = c_PayPlus.mf_get_res( "amount"       ); // 총 금액
                panc_mod_mny = c_PayPlus.mf_get_res( "panc_mod_mny" ); // 부분취소 요청금액
                panc_rem_mny = c_PayPlus.mf_get_res( "panc_rem_mny" ); // 부분취소 가능금액
            }
        }
    }
    /* = -------------------------------------------------------------------------- = */
    /* =   06. 승인 결과 처리 END                                                   = */
    /* ============================================================================== */


    /* = ========================================================================== = */
    /* =   07. 승인 및 실패 결과 DB 처리                                            = */
    /* = -------------------------------------------------------------------------- = */
    /* =      결과를 업체 자체적으로 DB 처리 작업하시는 부분입니다.                 = */
    /* = -------------------------------------------------------------------------- = */

    if ( req_tx.equals( "pay" ) )
    {

    /* = -------------------------------------------------------------------------- = */
    /* =   07-1. 승인 결과 DB 처리(res_cd == "0000")                                = */
    /* = -------------------------------------------------------------------------- = */
    /* =        각 결제수단을 구분하시어 DB 처리를 하시기 바랍니다.                 = */
    /* = -------------------------------------------------------------------------- = */
        if ( res_cd.equals( "0000" ) )
        {
            // 07-1-1. 신용카드
            if ( use_pay_method.equals( "100000000000" ) )
            {
                // 07-1-1-1. 복합결제(신용카드+포인트)
                if ( pnt_issue.equals( "SCSK" ) || pnt_issue.equals( "SCWB" ) )
                {

                }
            }

            // 07-1-2. 계좌이체
            if ( use_pay_method.equals("010000000000") )
            {

            }
            // 07-1-3. 가상계좌
            if ( use_pay_method.equals("001000000000") )
            {

            }
            // 07-1-4. 포인트
            if ( use_pay_method.equals("000100000000") )
            {

            }
            // 07-1-5. 휴대폰
            if ( use_pay_method.equals("000010000000") )
            {

            }
            // 07-1-6. 상품권
            if ( use_pay_method.equals("000000001000") )
            {

            }

        /* = -------------------------------------------------------------------------- = */
        /* =   07-2. 승인 실패 DB 처리(res_cd != "0000")                                = */
        /* = -------------------------------------------------------------------------- = */
            if( !"0000".equals ( res_cd ) )
            {

            }
        }
    }
    /* = -------------------------------------------------------------------------- = */
    /* =   07. 승인 및 실패 결과 DB 처리 END                                        = */
    /* = ========================================================================== = */

    /* = ========================================================================== = */
    /* =   08. 승인 결과 DB 처리 실패시 : 자동취소                                  = */
    /* = -------------------------------------------------------------------------- = */
    /* =      승인 결과를 DB 작업 하는 과정에서 정상적으로 승인된 건에 대해         = */
    /* =      DB 작업을 실패하여 DB update 가 완료되지 않은 경우, 자동으로          = */
    /* =      승인 취소 요청을 하는 프로세스가 구성되어 있습니다.                   = */
    /* =                                                                            = */
    /* =      DB 작업이 실패 한 경우, bSucc 라는 변수(String)의 값을 "false"        = */
    /* =      로 설정해 주시기 바랍니다. (DB 작업 성공의 경우에는 "false" 이외의    = */
    /* =      값을 설정하시면 됩니다.)                                              = */
    /* = -------------------------------------------------------------------------- = */

    // 승인 결과 DB 처리 에러시 bSucc값을 false로 설정하여 거래건을 취소 요청
    bSucc = "";

    if (req_tx.equals("pay") )
    {
        if (res_cd.equals("0000") )
        {
            if ( bSucc.equals("false") )
            {
                int mod_data_set_no;

                c_PayPlus.mf_init_set();

                tran_cd = "00200000";
	/* ============================================================================== */
    /* =   07-1.자동취소시 에스크로 거래인 경우                                     = */
    /* = -------------------------------------------------------------------------- = */
            String bSucc_mod_type; // 즉시 취소 시 사용하는 mod_type

                if ( escw_yn.equals("Y") && use_pay_method.equals("001000000000") )
                {
                    bSucc_mod_type = "STE5"; // 에스크로 가상계좌 건의 경우 가상계좌 발급취소(STE5)
                }
                else if ( escw_yn.equals("Y") )
                {
                    bSucc_mod_type = "STE2"; // 에스크로 가상계좌 이외 건은 즉시취소(STE2)
                }
                else
                {
                    bSucc_mod_type = "STSC"; // 에스크로 거래 건이 아닌 경우(일반건)(STSC)
                }
	/* = ---------------------------------------------------------------------------- = */
    /* =   07-1. 자동취소시 에스크로 거래인 경우 처리 END                             = */
    /* = ============================================================================== */

                mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

                c_PayPlus.mf_set_us( mod_data_set_no, "tno",      tno                          ); // KCP 원거래 거래번호
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_type", bSucc_mod_type               ); // 원거래 변경 요청 종류
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip",   cust_ip                      ); // 변경 요청자 IP
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc", "가맹점 결과 처리 오류 - 가맹점에서 취소 요청"  ); // 변경 사유

                c_PayPlus.mf_do_tx( g_conf_site_cd, g_conf_site_key, tran_cd, "", ordr_idxx, g_conf_log_level, "0" );

                res_cd  = c_PayPlus.m_res_cd;
                res_msg = c_PayPlus.m_res_msg;
            }
        }
    }
        // End of [res_cd = "0000"]
    /* = ---------------------------------------------------------------------------- = */
    /* =   07. 승인 결과 DB 처리 END                                                  = */
    /* = ============================================================================ = */

    ///* ============================================================================== */
    ///* =   08. 폼 구성 및 결과페이지 호출                                           = */
    ///* = -------------------------------------------------------------------------- = */

	}

	public String result() {
		Map<String, String> obj = new LinkedHashMap<String, String>();
		obj.put("site_cd", this.g_conf_site_cd);
		obj.put("req_tx", this.req_tx);
		obj.put("use_pay_method", this.use_pay_method);
		obj.put("bSucc", this.bSucc);
		obj.put("panc_mod_mny", this.panc_mod_mny);
		obj.put("panc_rem_mny", this.panc_rem_mny);
		obj.put("mod_type", this.mod_type);
		obj.put("amount", this.amount);
		obj.put("res_cd", this.res_cd);
		obj.put("res_msg", this.res_msg);
		obj.put("ordr_idxx", this.ordr_idxx);
		obj.put("tno", this.tno);
		obj.put("good_mny", this.good_mny);
		obj.put("good_name", this.good_name);
		obj.put("buyr_name", this.buyr_name);
		obj.put("buyr_tel1", this.buyr_tel1);
		obj.put("buyr_tel2", this.buyr_tel2);
		obj.put("buyr_mail", this.buyr_mail);
		obj.put("app_time", this.app_time);
		// 신용카드 정보
		obj.put("card_cd", this.card_cd);
		obj.put("card_name", this.card_name);
		obj.put("noinf", this.noinf);
		obj.put("app_no", this.app_no);
		obj.put("quota", this.quota);
		obj.put("partcanc_yn", this.partcanc_yn);
		obj.put("card_bin_type_01", this.card_bin_type_01);
		obj.put("card_bin_type_02", this.card_bin_type_02);
		// 계좌이체 정보
		obj.put("bank_name", this.bank_name);
		obj.put("bank_code", this.bank_code);
		// 가상계좌 정보
		obj.put("bankname", this.bankname);
		obj.put("depositor", this.depositor);
		obj.put("account", this.account);
		obj.put("va_date", this.va_date);
		// 포인트 정보
		obj.put("pnt_issue", this.pnt_issue);
		obj.put("pnt_app_time", this.pnt_app_time);
		obj.put("pnt_app_no", this.pnt_app_no);
		obj.put("pnt_amount", pnt_amount);
		obj.put("add_pnt", this.add_pnt);
		obj.put("use_pnt", this.use_pnt);
		obj.put("rsv_pnt", this.rsv_pnt);
		// 휴대폰 정보
		obj.put("commid", this.commid);
		obj.put("mobile_no", this.mobile_no);
		// 상품권 정보 
		obj.put("tk_van_code", this.tk_van_code);
		obj.put("tk_app_no", this.tk_app_no);
		// 현금영수증 정보
		obj.put("cash_yn", this.cash_yn);
		obj.put("cash_authno", this.cash_authno);
		obj.put("cash_tr_code", this.cash_tr_code);
		obj.put("cash_id_info", this.cash_id_info);
		// 에스크로 정보
		obj.put("escw_yn", this.escw_yn);
		obj.put("deli_term", this.deli_term);
		obj.put("bask_cntx", this.bask_cntx);
		obj.put("good_info", this.good_info);
		obj.put("rcvr_name", this.rcvr_name);
		obj.put("rcvr_tel1", this.rcvr_tel1);
		obj.put("rcvr_tel2", this.rcvr_tel2);
		obj.put("rcvr_mail", this.rcvr_mail);
		obj.put("rcvr_zipx", this.rcvr_zipx);
		obj.put("rcvr_add1", this.rcvr_add1);
		obj.put("rcvr_add2", this.rcvr_add2);

		return JSONValue.toJSONString(obj);

	}

	public void a() {
		System.out.print(g_conf_home_dir);
	}

	private static String f_get_parm(String val) {
    if (val == null) val = "";
    return val;
  }

}