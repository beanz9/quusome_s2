# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/home/quusome/quusome_s2"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/home/quusome/quusome_s2/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/home/quusome/quusome_s2/log/unicorn.log"
stdout_path "/home/quusome/quusome_s2/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.quusome_s2.sock"

# Number of processes
# worker_processes 4
worker_processes 8

# Time-out
timeout 30