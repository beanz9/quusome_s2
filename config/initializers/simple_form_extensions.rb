module SimpleForm
  module Inputs
    class FacebookInput < Base
      def input
        "http://www.facebook.com/ #{@builder.text_field(attribute_name, input_html_options)}".html_safe
      end
    end

    class TwitterInput < Base
      def input
        "http://www.twitter.com/ #{@builder.text_field(attribute_name, input_html_options)}".html_safe
      end
    end
  end
end