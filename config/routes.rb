# require 'sidekiq/web'
require 'sidetiq/web'

QuusomeS2::Application.routes.draw do

  mount RailsEmailPreview::Engine, at: 'emails'

  # authenticate :user, lambda {|user| user.seller_user? } do
  #   mount PgHero::Engine, at: "pghero"
  # end

  mount Sidekiq::Web, at: '/sidekiq'
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => {:registrations => "users"}

  root 'featured#index'

  get 'browser', :to => 'browser#index', :as => 'browser'
  
  # get 'users/user_info', :to => 'mypage#user_info'


  # devise_scope :user do
  #   get 'sign_up', :to => 'users#sign_up'
  #   get 'sign_up/seller', :to => 'users#new'
  #   get 'sign_up/normal', :to => 'users#new'
  #   get 'sign_in', :to => 'devise/sessions#new'
  # end


  resources :profile

  # resources :products
  #post 'products/basic', :to => 'products#create'

  #resources :myshop
  #get 'myshop', :to => 'dashboard#index'
  # get 'myshop/products', :to => 'products#index'

  namespace :myshop do
    root :to => 'dashboard#index'
    resources :products
    resources :dashboard
    resources :shopping_mall_configs
    resources :suppliers
    resources :exchange_rates
    resources :international_delivery_charges
    resources :saved_money_rates
    resources :featured_collections
    resources :featured_products
    resources :featured_product_category
    resources :blog_categories
    resources :blogs

    # get '/featured_product_category', :to => 'featured_product_category#index'
    get '/featured_product_category/new', :to => 'featured_product_category#new'
    # get '/featured_product_category/:id', :to => 'featured_product_category#show'
    # get '/featured_product_category/:id/edit', :to => 'featured_product_category#edit'
    # post '/featured_product_category', :to => 'featured_product_category#create', :as => 'featured_product_categories'
    put '/featured_product_category', :to => 'featured_product_category#update', :as => 'featured_product_categories'

    post 'featured/create'

    get 'orders/change_state/:id', :to => 'orders#change_state', :as => 'change_state'
    get 'orders/take_back_change_state/:id', :to => 'orders#take_back_change_state', :as => 'take_back_change_state'
    get 'orders/take_back_receives', :to => 'orders#take_back_receives', :as => 'take_back_receive'

    put 'orders/cancel/:id', :to => 'orders#cancel', :as => 'order_cancel'
    put 'orders/local_order/:id', :to => 'orders#local_order', :as => 'local_order'
    put 'orders/local_delivery/:id', :to => 'orders#local_delivery', :as => 'local_delivery'
    put 'orders/international_delivery/:id', :to => 'orders#international_delivery', :as => 'international_delivery'
    put 'orders/take_back_complete/:id', :to => 'orders#take_back_complete', :as => 'take_back_complete'
    put 'orders/take_back_cancel/:id', :to => 'orders#take_back_cancel', :as => 'take_back_cancel'
    put 'orders/take_back_confirm/:id', :to => 'orders#take_back_confirm', :as => 'take_back_confirm'
    put 'orders/refund/:id', :to => 'orders#refund', :as => 'refund'
    put 'orders/refund_hold/:id', :to => 'orders#refund_hold', :as => 'refund_hold'

    post 'orders/give_saved_money/:id', :to => 'orders#give_saved_money', :as => 'give_saved_money'

    get 'inquiry_comment', :to => 'inquiry_comment#index', :as => 'inquiry_comment'
    get 'inquiry_comment/:id', :to => 'inquiry_comment#show', :as => 'inquiry_comment_show'
    post 'inquiry_comment/:id', :to => 'inquiry_comment#create', :as => 'inquiry_comment_reply_create'
    delete 'inquiry_comment/:id', :to => 'inquiry_comment#destroy', :as => 'inquiry_comment_reply_destroy'
    get 'profit_withdraw_requests', :to => 'profit_withdraw_requests#index', :as => 'profit_withdraw_requests'
    put 'profit_withdraw_requests/update_state/:id', :to => 'profit_withdraw_requests#update_state', :as => 'profit_withdraw_requests_update_state'

    resources :orders 

  end

  resources :rootshop do
    resources :comments
  end

  # resources :collectionshop do
  #   resources :item
  # end


  get 'collectionshop/:user_id/(:collection_id)', :to => 'collectionshop#show', :as => 'collectionshop'
  post 'collectionshop/follow/:collection_id', :to => 'collectionshop#follow', :as => 'collectionshop_follow'
  post 'collectionshop/unfollow/:collection_id', :to => 'collectionshop#unfollow', :as => 'collectionshop_unfollow'
  get 'collectionshop', :to => 'collectionshop#index', :as => 'collectionshop_index'
  get 'collection_manage', :to => 'collectionshop#collection_manage'
  get 'collection_manage/:collection_id', :to => 'collectionshop#collection_manage_show', :as => 'collection_manage_show'

  #컬렉션 이름 변경
  put 'collection_manage/name_change/:collection_id/', :to => 'collectionshop#collection_name_change', :as => 'collection_name_change'

  #컬렉션 삭제
  delete 'collection_manage/remove_collection/:collection_id', :to => 'collectionshop#remove_collection', :as => 'remove_collection'
  
  #컬렉션 아이템 삭제
  delete 'collection_manage/remove_item/:item_id', :to => 'collectionshop#remove_collection_item', :as => 'remove_collection_item'

  #컬렉션 이미지 변경
  post 'collection_manage/image_update/:collection_id', :to => 'collectionshop#collection_image_update', :as => 'collection_image_update'

  get 'collectionshop/:user_id/item/:item_id', :to => 'item#show', :as => 'collection_item_show'
  post 'collectionshop_comment/:collection_id/comments', :to => 'comments#collectionshop_create', :as => 'collectionshop_comment_add'
  delete 'collectionshop_comment/:collection_id/comments/:comment_id', :to => 'comments#collectionshop_destroy', :as => 'collectionshop_comment_remove'
  get 'collectionshop_comment/:collection_id/comments', :to => 'comments#collectionshop_index', :as => 'collectionshop_comments_index'

  get 'rootshop/:id/inquiry_comment', :to => 'comments#inquiry_comment_index'
  post 'rootshop/:id/inquiry_comment', :to => 'comments#inquiry_comment_create', :as => 'inquiry_comment_create'
  delete 'rootshop/:id/inquiry_comment/:comment_id', :to => 'comments#inquiry_comment_destroy', :as => 'inquiry_comment_destroy'
  # get 'collections/:user_id', :to => 'collections#index', :as => 'collections'
  get 'category_child_2', :to => 'rootshop#category_child_2'
  get 'category_child_3', :to => 'rootshop#category_child_3'

  get 'mypage_inquiry_comment', :to => 'inquiry_comment#index', :as => 'mypage_inquiry_comment'
  delete 'mypage_inquiry_comment/:id', :to => 'inquiry_comment#destroy', :as => 'mypage_inquiry_comment_destroy'

  get 'mypage_saved_money', :to => 'saved_money#index', :as => 'mypage_saved_money'

  post 'mypage_withdrawal_request', :to => 'profits#withdrawal_request', :as => 'mypage_withdrawal_request'

  get 'carts', :to => 'line_items#index', :as => 'cart'
  get 'cart_check', :to => 'line_items#check', :as => 'cart_check'
  post 'carts', :to => 'line_items#create', :as => 'cart_add'
  put 'update_quantity', :to => 'line_items#update_quantity', :as => 'update_quantity'
  put 'update_option', :to => 'line_items#update_option', :as => 'update_option'
  delete 'delete_line_item/:line_item_id', :to => 'line_items#destroy', :as => 'delete_line_item'
  delete 'delete_all_line_item', :to => 'line_items#destroy_all', :as => 'delete_all_line_item'


  get 'orders/new', :to => 'orders#new', :as => 'new_orders'
  post 'orders', :to => 'orders#create', :as => 'orders'
  get 'orders/complete', :to => 'orders#complete', :as => 'complete_order'
  get 'orders/failure_order', :to => 'orders#failure_order', :as => 'failure_order'
  get 'orders', :to => 'orders#index'

  put 'orders/cancel/:id', :to => 'orders#cancel', :as => 'front_order_cancel'
  get 'orders/take_back_request/:id', :to => 'orders#take_back_request', :as => 'front_take_back_request'
  post 'orders/take_back_request/:id', :to => 'orders#create_take_back_request', :as => 'front_create_take_back_request'
  put 'orders/ship_complete/:id', :to => 'orders#ship_complete', :as => 'front_ship_complete'

  get 'profits', :to => 'profits#index'
  post 'profits/withdraw', :to => 'profits#withdrawal_request', :as => 'profits_withdraw'

  

  get 'products', :to => 'otherproducts#index', :as => 'other_products'
  get 'include_collections/:product_id', :to => 'includecollections#index', :as => 'include_collections'

  post 'collection_item_add', :to => 'rootshop#collection_item_add', :as => 'collection_item_add'
  delete 'collection_item_remove', :to => 'rootshop#collection_item_remove', :as => 'collection_item_remove'
  post 'collection_add', :to => 'rootshop#collection_add', :as => 'collection_add'
  delete 'collection_remove/:collection_id', :to => 'rootshop#collection_remove', :as => 'collection_remove'

  get 'zipcodes_search', :to => 'zipcodes#search', :as => 'zipcodes_search'

  get 'mypage', :to => 'mypage#user_info'
  get 'mypage/user_info', :to => 'mypage#user_info'


  post 'upload', :to => 'uploads#create'
  # patch 'upload', :to => 'uploads#create'

  delete 'upload/:id', :to => 'uploads#destroy', :as => 'upload_destroy'
  post 'avatar_update', :to => 'uploads#avatar_update'

  post 'request_user_destroy', :to => 'request_user_destroy#create', :as => 'request_user_destroy'
  delete 'cancel_user_destroy', :to => 'request_user_destroy#destroy', :as => 'cancel_user_destroy'

  get 'users/require_sign_in', :to => 'redirect_myshop_notices#require_sign_in', :as => 'require_sign_in'
  get 'users/require_seller_role', :to => 'redirect_myshop_notices#require_seller_role', :as =>'require_seller_role'
  #get 'myshop', :to => 'myshop#index', constraints: lambda { |request| request.format == "text/html" }
  
  #get 'profile/new', :to => 'profile#new'
  #get 'profile/option', :to => 'profile#option'

  get 'featured', :to => 'featured#index', :as => 'featured_index'
  get 'hot_collections', :to => 'featured#hot_collections', :as => 'hot_collections'

  resources :test
  get 'calculate', :to => 'test#calculate'
  get 'not_available_image', :to => 'test#not_available_image'
  get 'not_available_product', :to => 'test#not_available_product'

  get 'pages/faq' => 'high_voltage/pages#show', id: 'faq'
  get 'pages/clause' => 'high_voltage/pages#show', id: 'clause'

  get 'blogs' => 'blogs#index'
  get 'blogs/:id' => 'blogs#show'

  get 'board/:type' => 'board#index'
  get 'board/:type/new' => 'board#new'
  get 'board/:type/:id' => 'board#show'
  get 'board/:type/:id/edit' => 'board#edit'
  post 'board/:type' => 'board#create', :as => 'board_create'
  delete 'board/:type/:id' => 'board#destroy', :as => 'board_destroy'
  put 'board/:type/:id/edit' => 'board#update', :as => 'board_update'

  post 'board/:type/:id/comments', :to => 'board#comment_create', :as => 'board_comment_create'
  delete 'board/:type/:id/comments/:comment_id', :to => 'board#comment_destroy', :as => 'board_comment_destroy'






 
  # get '404', :to => 'high_voltage/pages#show', id: 'page_not_found'
  # get '500', :to => 'high_voltage/pages#show', id: 'internal_server_error'
end
