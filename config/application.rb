require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module QuusomeS2
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    
    config.encoding = "utf-8"
    config.i18n.available_locales = [:ko, :en]
    config.time_zone = 'Seoul'
    # config.active_record.default_timezone = 'Seoul'

    config.to_prepare do
        Devise::SessionsController.layout "rootshop" 
        # Devise::SessionsController.layout "devise"
        Devise::RegistrationsController.layout "rootshop" 
        # Devise::ConfirmationsController.layout "devise"
        # Devise::UnlocksController.layout "devise"            
        Devise::PasswordsController.layout "rootshop" 
    end

    # config.action_mailer.delivery_method = :postmark
    # config.action_mailer.postmark_settings = { :api_key => "30a28778-c7d8-459c-8358-2b4c2ae39472" }
        
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.default_url_options = { :host => 'quusome.com' }

    config.action_mailer.smtp_settings = {
      :address              => "smtp.mandrillapp.com",
      :port                 => 587,
      :domain               => 'quusome.com',
      :user_name            => 'van5150@gmail.com',
      :password             => 'rK0-yQ575BIo373bBRNI7w',
      :authentication       => 'plain',
      :enable_starttls_auto => true  
    }

    config.generators do |g|
        g.orm :active_record
    end

    config.active_record.schema_format = :sql
    # config.exceptions_app = self.routes
  
  end
end
