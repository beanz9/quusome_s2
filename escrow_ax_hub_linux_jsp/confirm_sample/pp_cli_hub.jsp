<%@ page language="java" contentType="text/html;charset=euc-kr"%>

<%
        /* ============================================================================== */
        /* =   PAGE : 에스크로 구매확인 후 취소 요청 및 결과 처리 PAGE                  = */
        /* = -------------------------------------------------------------------------- = */
        /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
        /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do                    = */
        /* = -------------------------------------------------------------------------- = */
        /* =   Copyright (c)  2013   KCP Inc.   All Rights Reserverd.                   = */
        /* ============================================================================== */
                                                                                            
                                                                                            
        /* ============================================================================== */
        /* =   환경 설정 파일 Include                                                   = */
        /* = -------------------------------------------------------------------------- = */
        /* =   ※ 필수                                                                  = */
        /* =   테스트 및 실결제 연동시 site_conf_inc.asp파일을 수정하시기 바랍니다.     = */
        /* = -------------------------------------------------------------------------- = */ 
%>
<%@ page import="com.kcp.*" %>
<%@ page import="java.net.URLEncoder"%>
<%@ include file="../cfg/site_conf_inc.jsp"%>
<%!
    /* ============================================================================== */
    /* =   null 값을 처리하는 메소드                                                = */
    /* = -------------------------------------------------------------------------- = */
        public String f_get_parm( String val )
        {
          if ( val == null ) val = "";
          return  val;
        }
	/* ------------------------------------------------------------------------------ */
	/* =   null 값을 처리하는 메소드 END                                            = */
    /* ============================================================================== */
%>

<%
    request.setCharacterEncoding ( "euc-kr" ) ;
    /* ============================================================================== */
    /* =   02. 지불 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */
        String req_tx          = f_get_parm( request.getParameter("req_tx"         ) ); // 요청종류
        String cust_ip         = f_get_parm( request.getRemoteAddr()                 ); // 요청 IP
        String tran_cd         = "";
        String res_cd          = "";                                                    // 응답코드
        String res_msg         = "";                                                    // 응답메시지
    /* ============================================================================== */
        String mod_method      = f_get_parm( request.getParameter("mod_method"     ) ); // 결제수단 
        String tno             = f_get_parm( request.getParameter("tno"            ) ); // 거래번호
        String mod_desc        = f_get_parm( request.getParameter("mod_desc"       ) ); // 취소사유
        String mod_depositor   = f_get_parm( request.getParameter("mod_depositor"  ) ); // 환불계좌주명(환불시에만 사용)
        String mod_account     = f_get_parm( request.getParameter("mod_account"    ) ); // 환불계좌번호(환불시에만 사용)
        String mod_bankcode    = f_get_parm( request.getParameter("mod_bankcode"   ) ); // 환불은행코드(환불시에만 사용)
        String mod_type        = f_get_parm( request.getParameter("mod_type"       ) ); // 취소구분
        String mod_sub_type    = f_get_parm( request.getParameter("mod_sub_type"   ) ); // 취소상세구분
        String sub_mod_type    = f_get_parm( request.getParameter("sub_mod_type"   ) ); // 취소유형
    /* = -------------------------------------------------------------------------- = */
    /* =   01. 지불 요청 정보 설정 END                                              = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   02. 인스턴스 생성 및 초기화(변경 불가)                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   결제에 필요한 인스턴스를 생성하고 초기화 합니다.                         = */
    /* =   ※ 주의 ※ 이 부분은 변경하지 마십시오                                   = */
    /* = -------------------------------------------------------------------------- = */

    C_PP_CLI c_PayPlus = new C_PP_CLI();

    c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_tx_mode );
    c_PayPlus.mf_init_set();

    /* = -------------------------------------------------------------------------- = */
    /* =   02. 인스턴스 생성 및 초기화 END                                          = */
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   03. 처리 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */
    /* = -------------------------------------------------------------------------- = */
    /* =   03-1. 에스크로 상태변경 요청                                             = */
    /* = -------------------------------------------------------------------------- = */
    if ( req_tx.equals( "mod_escrow" ) )
    {
		int     mod_data_set_no;

		tran_cd = "70200200";
		mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

        c_PayPlus.mf_set_us( mod_data_set_no, "tno"             , tno           ); // KCP 원거래 거래번호
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip"          , cust_ip       ); // 변경 요청자 IP
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc"        , mod_desc      ); // 변경 사유
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_type"        , mod_type      ); // 원거래 변경 요청 종류
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc_cd"     , "CA06"        ); // 취소사유코드
        c_PayPlus.mf_set_us( mod_data_set_no, "sub_mod_type"    , sub_mod_type  ); // 취소상세구분
        c_PayPlus.mf_set_us( mod_data_set_no, "mod_sub_type"    , mod_sub_type  ); // 취소유형

        /* 가상계좌 환불 */
        if ( mod_method.equals ("VCNT")){
            c_PayPlus.mf_set_us( mod_data_set_no, "mod_bankcode"	, mod_bankcode	);
            c_PayPlus.mf_set_us( mod_data_set_no, "mod_account"		, mod_account   );
            c_PayPlus.mf_set_us( mod_data_set_no, "mod_depositor"	, mod_depositor );
        }        


    }

    /* = -------------------------------------------------------------------------- = */
    /* =   03. 에스크로 상태변경 요청 END                                           = */
    /* ============================================================================== */

    /* ============================================================================== */
    /* =   04. 실행                                                                 = */
    /* = -------------------------------------------------------------------------- = */
    if ( tran_cd.length() > 0 )
    {
        c_PayPlus.mf_do_tx( g_conf_site_cd, g_conf_site_key, tran_cd, "", "", g_conf_log_level, "0" );

		res_cd  = c_PayPlus.m_res_cd;  // 결과 코드
		res_msg = c_PayPlus.m_res_msg; // 결과 메시지
    }

    else
    {
        c_PayPlus.m_res_cd  = "9562";
        c_PayPlus.m_res_msg = "연동 오류|tran_cd값이 설정되지 않았습니다.";
    }

    /* = -------------------------------------------------------------------------- = */
    /* =   04. 실행 END                                                             = */
    /* ============================================================================== */

    /* ================================================================================== */
    /* =   05.구매확인 후 취소 성공 결과 처리										    = */
    /* = ------------------------------------------------------------------------------ = */
        if ( req_tx.equals( "mod" ) )
        {
            if ( res_cd.equals( "0000" ) )
            {         
            } // End of [res_cd = "0000"]

    /* = -------------------------------------------------------------------------- = */
    /* =   05.구매확인 후 취소 실패 결과 처리                                       = */
    /* = -------------------------------------------------------------------------- = */
            else
            {
            }
        } // End of Proces 

    ///* ============================================================================== */
    ///* =   05. 폼 구성 및 결과페이지 호출                                           = */
    ///* = -------------------------------------------------------------------------- = */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>*** KCP [AX-HUB Version] ***</title>
        <script type="text/javascript">
            function goResult()
            {
				document.mod_info.submit();
				openwin.close();
            }

            // 결제 중 새로고침 방지 샘플 스크립트
            function noRefresh()
            {
                /* CTRL + N키 막음. */
                if ((event.keyCode == 78) && (event.ctrlKey == true))
                {
                    event.keyCode = 0;
                    return false;
                }
                /* F5 번키 막음. */
                if(event.keyCode == 116)
                {
                    event.keyCode = 0;
                    return false;
                }
            }
            document.onkeydown = noRefresh ;
        </script>
    </head>

    <body onload="goResult();" >
    <form name="mod_info" method="post" action="./result.jsp">
        <input type="hidden" name="res_cd"            value="<%=res_cd%>">            <!-- 결과 코드 -->
        <input type="hidden" name="res_msg"           value="<%=res_msg%>">           <!-- 결과 메세지 -->
    </form>
    </body>
</html>
