class NormalProfile < ActiveRecord::Base
  
  acts_as_paranoid
  has_paper_trail


  belongs_to :user
  has_many :selected_interest_categories
  has_many :selected_talent_categories
  has_many :product_categories, :through => :selected_interest_categories
  has_many :talent_categories, :through => :selected_talent_categories

  validates :gender, :presence => true,  if: :on_basic_step?
  validates :age, :presence => true, if: :on_basic_step? 
  validates :country, :presence => true, if: :on_basic_step?
  validates :product_category_ids, :presence => true, if: :on_basic_step?  
  validates :talent_category_ids, :presence => true, if: :on_basic_step?  
  validate :talent_category_max_validate  if :on_basic_step? 


  attr_accessor :status

  do_not_validate_attachment_file_type :avatar

  has_attached_file :avatar, 
    :styles => { :medium => "300x300#", :thumb => "100x100#" }, 
    :convert_options => {
      :medium => "-background white -gravity center -extent 300x300",
      :thumb => "-background white -gravity center -extent 100x100"
    },
    :default_url => 'quusome_profile.png'


  def small_image_url
    self.avatar.url(:thumb) 
  end

  def image_url
    self.avatar.url
  end


private
	def on_basic_step?
		self.status == 'basic'
	end

	def talent_category_max_validate
    errors.add(:talent_category_ids, 
    	I18n.t('activerecord.errors.models.normal_profile.attributes.talent_category_ids.talent_category_max_validate')) if self.talent_category_ids.count > 2
  end
end
