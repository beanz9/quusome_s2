class ProductChargeRange < ActiveRecord::Base
	

	belongs_to :product_category

	def from_price_currency_comma
		self.from_price.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
	end

	def to_price_currency_comma
		self.to_price.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
	end
end
