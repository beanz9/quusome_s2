class SelectedTalentCategory < ActiveRecord::Base

	acts_as_paranoid
  has_paper_trail
  
  belongs_to :normal_profile
  belongs_to :talent_category
end
