class ProductOption < ActiveRecord::Base
  belongs_to :product
  has_many :product_option_items, dependent: :destroy
  
  accepts_nested_attributes_for :product_option_items, allow_destroy: true, 
		reject_if: lambda { |product_option_item| product_option_item[:title].blank? }
end
