class ScrapyItem
	include Mongoid::Document

	field :title
	field :sub_title
	field :url
	field :domain
	field :desc
	field :currency
	field :price
	field :options
	field :images
	field :status
	field :created_at
	field :created_by
	field :category
	field :brand
	field :breadcrumb
	field :county
	field :is_done
	field :inventory
	field :size_table_html
	field :is_sale
end