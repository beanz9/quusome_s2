class OrdersItem < ActiveRecord::Base

	track_who_does_it
	acts_as_paranoid
	has_paper_trail
	
	belongs_to :order
  belongs_to :product
  belongs_to :product_option
  belongs_to :user, :foreign_key => "created_by_id"
  has_many :change_states
  has_many :take_back_requests

  # after_create :update_product_amount

  scope :search_product_title, ->(title) { where("product_title like ?", "%#{title}%") if title.present? } 
  scope :search_state, ->(state) { where("state = ?", state) if state.present? }
  # scope :search_date , ->(start_date, end_date) { where(:created_at => start_date..end_date) }

  def self.search_date(start_date, end_date)

  	if start_date.present? && end_date.present?
  		# where("orders_items.created_at >= ? and orders_items.created_at <= ?", start_date, end_date)
  		where("orders_items.created_at" => start_date..end_date)
  	elsif start_date.present? && !end_date.present?
  		where("orders_items.created_at >= ?", start_date)
  	elsif !start_date.present? && end_date.present?
  		where("orders_items.created_at <= ?", end_date)
  	else
  		where("1=1")
  	end
  end

  state_machine :initial => :order_completed do

		#주문확인
		# event :order_confirm do
		# 	transition :order_completed => :order_confirmed
		# end

		#주문취소
		event :order_cancel do
			transition :order_completed => :order_canceled
		end

		#해외주문접수
		event :local_order do
			transition :order_completed => :local_ordered
		end

		#현지배송
		event :local_delivery do
			transition :local_ordered => :local_delivered
		end

		#국제배송완료
		event :international_delivery do
			transition :local_delivered => :international_deliverd
		end


		#국내배송

		#배송준비
		# event :prepare_delivery do
		# 	transition :order_completed => :prepare_deliverd
		# end

		# #배송중
		# event :doing_delivery do
		# 	transition :prepare_deliverd => :doing_deliverd
		# end

		# #배송완료
		# event :delivery_complete do
		# 	transition :doing_deliverd => :delivery_completed
		# end











		#배송준비
		# event :ship_prepare do
		# 	transition :order_confirmed => :ship_prepared
		# end

		#배송
		# event :ship do
		# 	transition :ship_prepared => :shipping
		# end

		#배송완료
		# event :ship_complete do
		# 	transition :shipping => :ship_completed
		# end

		#반품신청
		# event :take_back_request do
		# 	transition :international_deliverd => :take_back_requested
		# end

		#반품접수요청
		event :take_back_receive do
			transition :international_deliverd => :take_back_received
		end

		# #반품접수완료
		event :take_back_complete do
			transition :take_back_received => :take_back_completed
		end

		#반품 반려
		event :take_back_cancel do
			transition :take_back_received => :take_back_canceled
		end

		#상품 검수
		event :take_back_confirm do
			transition :take_back_completed => :take_back_confirmed
		end

		#환불
		event :refund do
			transition :take_back_confirmed => :refunded
		end

		#환불 보류
		event :refund_hold do
			transition :take_back_confirmed => :refund_held
		end


	end

	def self.grouped_orders_item(user_id)
		
		where(:user_id => user_id).
		order("created_at DESC").
		# page(1).per(10).
		group("product_id, product_title, product_option_id, product_option_title, order_id, state, created_at, product_featured_small_image_url").
		select("max(id) as id, product_id, product_title, product_option_id, product_option_title, product_featured_small_image_url, 
			 order_id, sum(price) as price, 
			state, count(1) as quantity")
	end

	def update_product_amount 
		product = self.product

		if self.product_option_id.present?
			product_option = ProductOption.find(self.product_option_id)
			product_option.amount = product_option.amount.to_i - 1
			product_option.save
		else
			product.amount = product.amount.to_i - 1
			product.save
		end
		
		
	end


end
