class ShoppingMallConfig < ActiveRecord::Base

	# attr_accessible :delivery_charge

	def self.cached_config(domain)
		Rails.cache.fetch([self, domain]) { find_by_domain(domain) }
	end
end
