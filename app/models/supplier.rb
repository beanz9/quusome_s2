class Supplier < ActiveRecord::Base

	track_who_does_it

	validates :name, :presence => true
end
