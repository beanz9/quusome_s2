class Upload < ActiveRecord::Base

	has_many :products
	has_many :product_images, :through => :products

  do_not_validate_attachment_file_type :file

	has_attached_file :file, 
    :styles => { 
      :medium => "300x300>", 
      :thumb => "100x100>",
      :rootshop_index => "280x",
      :original => "600x600>" 
    }, 
    :convert_options => {
      :medium => "-background white -gravity center -extent 300x300",
      :thumb => "-background white -gravity center -extent 100x100",
      :original => "-background white -gravity center -extent 600x600"
    },
    :default_url => "/images/:style/missing.png"

  # validates_attachment_content_type :file, :content_type => %w(image/jpeg image/jpg image/png)
  # has_attached_file :file

  def image_url
   	self.file.url
  end

  def small_image_url
  	self.file.url(:thumb)
  end

  def rootshop_index_url
    self.file.url(:rootshop_index)
  end
end
