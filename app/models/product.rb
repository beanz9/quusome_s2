class Product < ActiveRecord::Base

	# include PgSearch
	serialize :multi_price, ActiveRecord::Coders::NestedHstore
	serialize :inventory, ActiveRecord::Coders::NestedHstore
	serialize :size_table, ActiveRecord::Coders::NestedHstore

	track_who_does_it
	acts_as_paranoid 
	has_paper_trail

	acts_as_taggable
  acts_as_taggable_on :keywords

  acts_as_commentable
  is_impressionable
  
  scope :only_public,  -> { where(:is_public => true) }

	has_many :selected_category_on_products
	has_many :product_images, dependent: :destroy, :order => 'sort'
	has_many :upload, :through => :product_images
	has_many :product_options, dependent: :destroy
	has_many :collection_items, dependent: :destroy
	has_many :line_items, dependent: :destroy
	has_many :orders_items
	belongs_to :user, :foreign_key => 'created_by_id'
	has_many :reviews
	has_one :supplier

	# pg_search_scope :search,
	# 								:against => {
 #                    :title => 'A',
 #                    :detail_info => 'B'
 #                  },
 #                  :using => {
 #                    :tsearch => {:prefix => true}
 #                  }


	


	accepts_nested_attributes_for :product_images, allow_destroy: true, 
		reject_if: lambda { |product_image| product_image[:upload_id].blank? }
	accepts_nested_attributes_for :product_options, allow_destroy: true, 
		reject_if: lambda { |product_option| product_option[:title].blank? }
	#attr_accessor :tag_list

	def featured_image_url
		self.product_images[0].upload.file.url
	end

	def featured_medium_image_url
		self.product_images.order("created_at ASC")[0].upload.file.url(:medium) if product_images[0]
	end

	def featured_small_image_url
		self.product_images.order("created_at ASC")[0].upload.file.url(:thumb) if product_images[0]
	end

	def featured_rootshop_index_image_url
		self.product_images.order("created_at ASC")[0].upload.file.url(:rootshop_index) if product_images[0]
	end

	# def price_currency_comma
	# 	self.price.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
	# end

	def deleted?
		self.deleted_at != nil
	end

	

	def multi_price_final

		multi_price_final = self.multi_price.deep_dup
		multi_price_final["original"] = charge(multi_price_final["original"].gsub(',','').to_f)
		if multi_price_final["sale_color"]
			multi_price_final["sale_color"].keys.each do |key|
				multi_price_final["sale_color"][key] = charge(multi_price_final["sale_color"][key].gsub(',','').to_f)
			end
		end

		multi_price_final
	end

	def get_quusome_charge
		get_price(option) * 0.1
	end

	def add_categories(category)
		categories_will_change!
	  update_attributes categories: categories.push(category)
	end

	def get_price(option)
		if !self.multi_price["sale_color"].empty?
			self.multi_price["sale_color"][option].gsub(',','').to_f
		else
			self.multi_price["original"].gsub(',','').to_f
		end
	end

	def get_original_price(option)
		if !self.multi_price["sale_color"].empty?
			self.multi_price["sale_color"][option].gsub(',','').to_f
		else
			self.multi_price["original"].gsub(',','').to_f
		end
	end

	def get_price_final(option)

		if self.is_crawled == 'true'
			if !self.multi_price["sale_color"].empty?
				charge(self.multi_price["sale_color"][option].gsub(',','').to_f)
			else
				charge(self.multi_price["original"].gsub(',','').to_f)
			end
		else
			if self.is_sale = 'true'
				self.sale_price.to_i
			else
				self.price.to_i
			end
		end

	end

	private


	def charge(price)
		#쇼핑몰 설정 정보
	
		# shopping_mall = ShoppingMallConfig.find_by_domain(self.domain)
		shopping_mall = ShoppingMallConfig.cached_config(self.domain)
		p "<>>>>>>>"
		p self.domain
		p shopping_mall
		#환율
		# exchange_rate = ExchangeRate.where(:currency => shopping_mall.currency).order("created_at DESC").first
		exchange_rate = ExchangeRate.cached_rate(shopping_mall.currency)

		#이 상품의 카테고리 무게
		if self.category_str != nil
			# product_weight = ProductCategory.where(:code => self.category_str).first
			product_weight = ProductCategory.cached_category(self.category_str)
		else
			product_weight = nil
		end

		if product_weight == nil
			weight = 1
		else
			weight = product_weight.weight
		end
		#국제 배송료
		# international_delivery_charge = InternationalDeliveryCharge.where(:weight => weight, :location => shopping_mall.location).first
		international_delivery_charge = InternationalDeliveryCharge.cached_charge(weight, shopping_mall.location)
		p "!!!!!!"

		p weight
		p shopping_mall.location
		p international_delivery_charge

		delivery_charge = 0


		if price < shopping_mall.free_condition

			delivery_charge = shopping_mall.delivery_charge
		end

		# price = (price + delivery_charge) * exchange_rate.price
		price = (price + (price * 0.1) + delivery_charge) * exchange_rate.price

		price = price + international_delivery_charge.price
		(price * 0.01).ceil * 100
		# price_currency_comma((price * 0.01).ceil * 100)
	end

	def price_currency_comma(price)
		price.to_s.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
	end


end
