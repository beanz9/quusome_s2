class TalentCategory < ActiveRecord::Base

	has_many :selected_talent_caterories
	has_many :normal_profiles, :through => :selected_talent_caterories
	
end
