class Order < ActiveRecord::Base
	track_who_does_it
	acts_as_paranoid
	has_paper_trail

	attr_encrypted :rrn_1, :key => "beanz9quusomerrn1111"
	attr_encrypted :rrn_2, :key => "beanz9quusomerrn2222"

	has_many :orders_items, dependent: :destroy
	belongs_to :user, :foreign_key => "created_by_id"
	has_one :payment_result_data

	after_create :create_orders_items_cart, :if => :order_type_cart?
	after_create :create_order_items_direct, :if => :order_type_direct?

	attr_accessor :virtual_order_type, 
		:virtual_product_id, 
		:virtual_product_option_id, 
		:virtual_quantity, 
		:virtual_cart_ids,
		:virtual_collection_item_id,
		:virtual_saved_money,
		:current_user_id 


	def get_orders_items
		OrdersItem.
		where(:created_by_id => self.created_by_id, :order_id => self.id).
		page(1).per(10).
		group("product_id, product_title, product_option_id, product_option_id, order_id, state, product_featured_small_image_url").
		select("product_id, product_title, product_option_id, product_option_title, product_featured_small_image_url,
			 order_id, sum(price) as price, 
			 state, count(1) as quantity")
	end
	

protected



	def order_type_cart?
		!self.virtual_order_type.present? or self.virtual_order_type == 'select'
	end

	def order_type_direct?
		self.virtual_order_type == 'direct'
	end

	# def order_type_select?
	# 	self.virtual_order_type == 'select'
	# end

	

	#상품페이지에서 즉시 구매
	def create_order_items_direct

		option_str = ""
		price_option = ""
		virtual_product_option_id.each do |o|
			option_id, option_item_id = o.split('_')
			option = ProductOption.where(:id => option_id.to_i).first
			option_item = ProductOptionItem.where(:id => option_item_id).first
			option_str = option_str + "'#{option.title}' => '#{option_item.title}', "
			price_option = option_item.title if option.title == 'color'
		end

		product = Product.find(self.virtual_product_id)
		exchange_rate = ExchangeRate.where(:currency => product.currency).order("created_at DESC").first
		used_saved_money = self.get_saved_money_from_order(product.id, self.virtual_saved_money)
		price = product.get_price_final(price_option)
		saved_money_rate = SavedMoneyRate.order("created_at DESC").first.saved_money_rate

		# if price < used_saved_money
		# 	temp_saved_money = price
		# else
		# 	temp_saved_money = used_saved_money
		# end

		(1..virtual_quantity.to_i).each do
			
			# shopping_mall = ShoppingMallConfig.find_by_domain(product.domain)
			
			# product_weight = ProductCategory.find_by_code(product.category_str).weight
			# international_delivery_charge = InternationalDeliveryCharge.where(:weight => product_weight, :location => shopping_mall.location).first

			# delivery_charge = 0

			# if product.get_price(price_option) < shopping_mall.free_condition
			# 	delivery_charge = shopping_mall.delivery_charge
			# end

			if used_saved_money > price
				temp_saved_money = price
			else
				temp_saved_money = used_saved_money
			end
			

			orders_item = OrdersItem.create(
				:product_id => product.id,
				:product_title => product.title,
				:options => eval("{#{option_str}}"),
				# :product_option_id => self.virtual_product_option_id,
				# :product_option_title => product_option_title,
				:product_featured_small_image_url => product.featured_small_image_url,
				:order_id => self.id,
				:price => price,
				:user_id => self.current_user_id, 
				:collection_item_id => self.virtual_collection_item_id,
				:exchange_rate => exchange_rate.price,
				:currency => product.currency,
				:original_price => product.get_original_price(price_option),
				:used_saved_money => temp_saved_money,
				:original_exchange_price => price,
				:saved_money_rate => saved_money_rate,
				:saved_money => ApplicationController.helpers.saved_money_calc(price - temp_saved_money)
				# :delivery_charge => delivery_charge,
				# :international_delivery_charge => international_delivery_charge.price
			)

			used_saved_money_create(temp_saved_money, self.current_user_id, orders_item.id)

			if used_saved_money > price
				used_saved_money = used_saved_money - price
			else
				used_saved_money = 0
			end

		end

		
	end

	
	#카트에서 구매
	def create_orders_items_cart
		if self.virtual_order_type == 'select'
			@carts = LineItem.where(:created_by_id => self.created_by_id, :id => self.virtual_cart_ids)
		else
			@carts = LineItem.where(:created_by_id => self.created_by_id)
		end
		@carts.each do |cart|

			option_str = ""

			if cart.options
				cart.options.each do |o|
					option_str = option_str + "'#{o["option_title"]}' => '#{o["option_item_title"]}', "
				end
			end

			product = Product.find(cart.product_id)
			exchange_rate = ExchangeRate.where(:currency => product.currency).order("created_at DESC").first

			exchange_rate_price = nil
			original_price = nil
			currency = nil

			if product.is_crawled == 'true'
				exchange_rate_price = exchange_rate.price
				original_price = product.get_original_price(cart.get_selected_option_item_title)
				currency = product.currency
			end

			used_saved_money = self.get_saved_money_from_order(product.id, self.virtual_saved_money)
			price = cart.product.get_price_final(cart.get_selected_option_item_title)
			saved_money_rate = SavedMoneyRate.order("created_at DESC").first.saved_money_rate

			(1..cart.quantity.to_i).each do 
				
				if used_saved_money > price
					temp_saved_money = price
				else
					temp_saved_money = used_saved_money
				end

				if product.is_crawled == 'true'
					saved_money = ApplicationController.helpers.saved_money_calc(price - temp_saved_money)
				else
					saved_money = product.save_money
				end


				# product_option_title = ProductOption.find(cart.product_option_id).title if cart.product_option_id.present?
				orders_item = OrdersItem.create({
					:product_id => product.id,
					# :product_option_id => cart.product_option_id,
					:options => eval("{#{option_str}}"),
					:product_title => product.title,
					# :product_option_title => product_option_title,
					:product_featured_small_image_url => product.featured_small_image_url,
					:order_id => self.id,
					:price => price - temp_saved_money,
					:user_id => self.current_user_id,
					:collection_item_id => cart.collection_item_id,
					:exchange_rate => exchange_rate_price,
					:currency => currency,
					:original_price => original_price,
					:used_saved_money => temp_saved_money,
					:original_exchange_price => price,
					:saved_money_rate => saved_money_rate,
					:saved_money => saved_money
				})

				used_saved_money_create(temp_saved_money, self.current_user_id, orders_item.id)

				if used_saved_money > price
					used_saved_money = used_saved_money - price
				else
					used_saved_money = 0
				end

				

			end
			cart.destroy
		end
	end

	def get_saved_money_from_order(product_id, virtual_saved_money)
		saved_money = ""
		self.virtual_saved_money.each do |money|

			if money.split("_")[0] == product_id.to_s
				saved_money = money.split("_")[1] 
				break
			end
		end
		saved_money.to_f
	end

	private

	def used_saved_money_create(saved_money, user_id, orders_item_id)
		SavedMoney.create({
			:user_id => user_id,
			:order_id => self.id,
			:orders_item_id => orders_item_id,
			:money => -saved_money
		}) if saved_money > 0
	end

	


	def encryption_key
		"beanz9_quusome_rrn_#{self.id}"
    # does some fancy logic and returns an encryption key
  end
end
