class SelectedProductCategory < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :seller_profile
end
