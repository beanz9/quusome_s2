class SelectedInterestCategory < ActiveRecord::Base

	acts_as_paranoid
  has_paper_trail
  
  belongs_to :product_category
  belongs_to :normal_profile
end
