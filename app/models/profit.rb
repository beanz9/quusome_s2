class Profit < ActiveRecord::Base
  belongs_to :product
  belongs_to :user
  belongs_to :orders_item
  belongs_to :order
  belongs_to :collection_item
  belongs_to :collection

  def collection_with_deleted
  	Collection.with_deleted.find(self.collection_id)
  end
end
