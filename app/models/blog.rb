class Blog < ActiveRecord::Base
  belongs_to :blog_category
  acts_as_paranoid
  

  do_not_validate_attachment_file_type :image

  has_attached_file :image, 
  	:styles => { 
      :body => "825x"
    }

end
