class ProductCategory < ActiveRecord::Base
	default_scope { order("position ASC")}
	# has_many :selected_product_categories
	# has_many :selected_interest_categories

	has_ancestry
	
	has_many :seller_profiles, :through => :selected_product_categories
	has_many :normal_profile, :through => :selected_interest_categories
	has_many :product_charge_ranges, :order => 'from_price'
	

	accepts_nested_attributes_for :product_charge_ranges, :allow_destroy => true

	def self.cached_category(str)
		Rails.cache.fetch([self, str]) { where(:code => str).first }
	end

end
