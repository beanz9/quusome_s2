class LineItem < ActiveRecord::Base
	# serialize :options, ActiveRecord::Coders::NestedHstore

	track_who_does_it
	# acts_as_paranoid

  belongs_to :product
  belongs_to :product_option

  # def product
  # 	Product.with_deleted.find(self.product_id)
  # end

  def get_selected_option_item_title(option="color")
    if self.product.is_crawled == 'true'
    	if option == 'color'
    		color_option = self.options.detect {|l| l["option_title"] == "color"}
    		color_option["option_item_title"] if color_option
    	end
    end
  end

end
