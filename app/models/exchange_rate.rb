class ExchangeRate < ActiveRecord::Base

	def self.cached_rate(currency)
		Rails.cache.fetch([self, currency]) { where(:currency => currency).order("created_at DESC").first }
	end
end
