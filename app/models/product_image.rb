class ProductImage < ActiveRecord::Base
  belongs_to :product
  belongs_to :upload

  def small_image_url
  	self.upload.file.url(:thumb)
  end

  def image_url
  	self.upload.file.url
  end

end
