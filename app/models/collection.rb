class Collection < ActiveRecord::Base

	# include PgSearch
	include PublicActivity::Model
	is_impressionable
  acts_as_commentable
  acts_as_followable
  acts_as_paranoid 

  belongs_to :user
  has_many :collection_items, dependent: :destroy
  has_many :products, :through => :collection_items
  has_many :profits
  has_many :featured_collections

  scope :has_collection_items, -> { where( "1=1" ) }

  after_create :after_collection_create

  do_not_validate_attachment_file_type :file

  has_attached_file :file, 
    :styles => { 
      :medium => "300x300>", 
      :thumb => "100x70#",
      :rootshop_index => "280x" 
    }, 
    :convert_options => {:medium => "-background white -gravity center -extent 300x300"},
    :default_url => "http://placekitten.com/100/70"

  # pg_search_scope :search,
		# 							:against => {
  #                   :name => 'A',
  #                   :info => 'B'
  #                 },
  #                 :associated_against => {
		# 						    :collection_items => :collection_id,
		# 						    :products => [:title, :detail_info]
		# 						  },
  #                 :using => {
  #                   :tsearch => {:prefix => true}
  #                 }

  def featured_small_image_url
  	
  	if is_deleted?
  		return "http://placekitten.com/100/70"
  	end

  	if self.file_file_name.nil?
  		if self.collection_items.count > 0
				self.collection_items.joins(:product)[0].product.product_images[0].upload.file.url(:thumb) 
			else
				"http://placekitten.com/100/70"
			end
		else
			self.file.url(:thumb)
  	end
	end

	def featured_image_url
		if self.file_file_name.nil?
			# collection_items = self.collection_items.joins(:product)
			# if collection_items.count > 0
			# 	collection_items.each do |item|
			# 		unless item.product.deleted?
			# 			return  item.product.product_images[0].upload.file.url
			# 			break
			# 		end
			# 	end
			# end
			collection_items = self.collection_items.joins(:product)
			collection_items[0].product.product_images[0].upload.file.url if collection_items.count > 0
		else
			self.file.url(:medium)
		end
	end

	def is_deleted?
		!self.deleted_at.nil?
	end

	def featured_item
		self.collection_items[0]
	end

	def after_collection_create
		self.create_activity key: 'collection.collection_create', owner: self.user
	end


end
