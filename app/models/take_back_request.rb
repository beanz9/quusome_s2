class TakeBackRequest < ActiveRecord::Base

	belongs_to :orders_item
	
	has_attached_file :image_1
	has_attached_file :image_2
	has_attached_file :image_3

	do_not_validate_attachment_file_type :image_1, :image_2, :image_3

end
