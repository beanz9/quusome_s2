class CollectionItem < ActiveRecord::Base

	include PublicActivity::Model
	acts_as_paranoid 

  belongs_to :collection, :touch => true
  belongs_to :product
  belongs_to :unscoped_product, -> { unscoped }, class_name: "Product", foreign_key: :product_id 
  belongs_to :user
  has_many :profits

  


  after_create :after_collection_create, :collection_items_count_save
  after_destroy :collection_items_count_save

  def small_image_url
		self.product.product_images.order("created_at ASC").limit(1)[0].upload.file.url(:thumb)
	end

	def after_collection_create
		self.create_activity key: 'collection.collection_item_create_product', owner: self.product.user, recipient: self.product
		self.create_activity key: 'collection.collection_item_create_collection', owner: self.user, recipient: self.collection
	end

	def collection_items_count_save
		collection = Collection.find(self.collection_id)
		collection.collection_items_count = CollectionItem.where(:collection_id => collection.id).count
		collection.save
	end



end
