class Follow < ActiveRecord::Base

  extend ActsAsFollower::FollowerLib
  extend ActsAsFollower::FollowScopes
  include PublicActivity::Model
  # acts_as_paranoid default_scope: false

  


  # NOTE: Follows belong to the "followable" interface, and also to followers
  belongs_to :followable, :polymorphic => true
  belongs_to :follower,   :polymorphic => true

  after_create :after_follow_create
  # before_destroy :before_follow_destroy

  def block!
    self.update_attribute(:blocked, true)
  end

  def after_follow_create
  	self.create_activity key: 'collection.follow_create', owner: self.follower, recipient: self.followable
  end

  def before_follow_destroy
  	self.create_activity key: 'collection.follow_destroy', owner: self.follower, recipient: self.followable
  end

end
