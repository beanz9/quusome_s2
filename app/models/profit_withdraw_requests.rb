class ProfitWithdrawRequests < ActiveRecord::Base

	attr_encrypted :bank_name, :key => "beanz9quusomebankname"
	attr_encrypted :account_number, :key => "beanz9quusomeaccountnumber"
	attr_encrypted :account_owner, :key => "beanz9quusomeaccountowner"
	attr_encrypted :tel_no_1, :key => "beanz9quusometelno1"
	attr_encrypted :tel_no_2, :key => "beanz9quusometelno2"
	attr_encrypted :tel_no_3, :key => "beanz9quusometelno3"

end
