class InternationalDeliveryCharge < ActiveRecord::Base

	def self.cached_charge(weight, location)
		Rails.cache.fetch([self, weight, location]) { where(:weight => weight, :location => location).first }
	end
end
