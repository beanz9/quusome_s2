class SellerProfile < ActiveRecord::Base

  acts_as_paranoid
  has_paper_trail
  
  belongs_to :user
  has_many :selected_product_categories
  has_many :product_categories, :through => :selected_product_categories


  validates :gender, :presence => true,  if: :on_basic_step?
  validates :age, :presence => true, if: :on_basic_step? 
  validates :site, :presence => true, if: :on_basic_step?
  validates :country, :presence => true, if: :on_basic_step? 
  validates :business_registration_number, :presence => true, if: :on_basic_step? 
  validates :business_representative, :presence => true, if: :on_basic_step? 
  validates :company_name, :presence => true, if: :on_basic_step? 
  #validate :product_category_validate
  validates :product_category_ids, :presence => true, if: :on_basic_step?  

  attr_accessor :status
  
  do_not_validate_attachment_file_type :avatar

  has_attached_file :avatar, 
    :styles => { :medium => "300x300>", :thumb => "100x100>" }, 
    :convert_options => {
      :medium => "-background white -gravity center -extent 300x300",
      :thumb => "-background white -gravity center -extent 100x100"
    },

    :default_url => 'quusome_profile.png'


  def small_image_url
    self.avatar.url(:thumb) 
  end

  def image_url
    self.avatar.url
  end

private

  

  def on_basic_step?
  	self.status == 'basic'
  end

  # def product_category_validate
  #   errors.add(:base, I18n.t('at_least_message', '1')) if self.product_category_ids.count < 1
  # end

end
