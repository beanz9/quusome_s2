class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable


  include SentientUser
  # acts_as_paranoid
  has_paper_trail
  acts_as_follower
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # validates :email, presence: true, uniqueness: {:scope => :deleted_at}
  validates :user_id, presence: true, length: { maximum: 8 }, uniqueness: true #{ :scope => :deleted_at}
  validates :user_type, presence: true
  # validates_confirmation_of :password
  

  has_one :seller_profile, :dependent => :destroy
  has_one :normal_profile, :dependent => :destroy

  has_many :collections, :dependent => :destroy
  has_many :product, :foreign_key => 'created_by_id', :dependent => :destroy


  def small_image_url
    if seller_user?
      self.seller_profile.avatar.url(:thumb) 
    else
      self.normal_profile.avatar.url(:thumb) 
    end
  end

  def image_url
    if seller_user?
      self.seller_profile.avatar.url
    else
      self.normal_profile.avatar.url 
    end
  end

 

  def normal_user?
    self.user_type == 'normal'
  end

  def seller_user?
    self.user_type == 'seller'
  end

  def profile
    if normal_user?
      self.normal_profile
    else
      self.seller_profile
    end
  end



  # def self.find_for_authentication(warden_conditions)
  #   p "!!!!!!"
  #   where(:email => warden_conditions[:email], :deleted_at => nil).first
  # end

  # def self.find_first_by_auth_conditions(warden_conditions)
  #   p "??????????"
  #   User.where("email = ? and email is null", warden_conditions[:email])
   
  # end


end
