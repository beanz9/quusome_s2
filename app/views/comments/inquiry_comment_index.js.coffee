$('#inquiry_comment').html("<%= j render 'inquiry_comment', :inquiry_comments => @inquiry_comments %>")

$('.timeago').each ->
	date = moment(new Date($(@).attr('data-date')))
	$(@).html(date.fromNow())

$('#comment_submit_btn').on 'click', ->
	if $('#comment').val() != ''
		$('#comment_submit_btn').addClass('loading')
		$('#comment_submit').click()
			
$('#comment_button').on 'click', ->
	$('#comment_button').hide()
	$('#comment_add_form').show()