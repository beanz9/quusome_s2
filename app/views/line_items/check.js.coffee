<% if @disavailable.empty? %>
	window.location.href = "/orders/new"
<% else %>
	
	alert("<%= @disavailable.join(',') %> 상품이 품절입니다.\n확인 후 주문해 주세요.")
<% end %>