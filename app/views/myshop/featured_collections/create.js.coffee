<% if @featured_collection.errors.any? %>
alert("저장 중 에러가 발생하였습니다.")
$('.ui.modal').modal('hide')
<% else %>
alert("저장되었습니다.")
$('.ui.modal').modal('hide')
location.reload(true)
<% end %>