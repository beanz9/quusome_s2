$('#zipcode_tbody').html("<%= j render 'orders/zipcode', :zipcodes => @zipcodes %>")

$('.row').on 'click', ->
	$('#order_zipcode').val($(@).find('.row_zipcode').text())
	$('#order_address').val($(@).find('.row_address').text())
	$('#zipcode_search_modal').modal('hide')