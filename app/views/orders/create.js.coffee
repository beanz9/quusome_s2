<% if Rails.env.production? %>
	<% if @payment_result_data %>
		<% if @payment_result_data.is_success %>
			alert("<%= @payment_msg %>")
			location.href = "/orders"
		<% else %>
			alert("<%= @payment_msg %>")
		<% end %>
	<% else %>
		alert("<%= @payment_msg %>")
	<% end %>
<% else %>
	location.href = "/orders"
<% end %>