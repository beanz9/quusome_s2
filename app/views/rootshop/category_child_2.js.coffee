$('#category_field_2').empty().append("<%= j select_tag 'category_arr[]', options_from_collection_for_select(@categories, 'id', 'name', params[:category]), :id => 'category_depth_2', :prompt =>'선택하세요.', :style => 'width: 200px;' %> <i class='remove sign icon large' style='cursor: pointer' id='delete_category_depth_2'></i>")

$('#delete_category_depth_2').on 'click', ->
	# $('#category_field_2').empty();
	$('#category_depth_2').val('')
	$('#category_field_3').empty();

$('#category_depth_2').on 'change', ->
	if $('#category_depth_2 option:selected').val() == ''
		$('#category_field_3').empty()
	else
		$.ajax(
			url: 'category_child_3'
			type: 'GET'
			dataType: 'script'
			data: { id: $('#category_depth_2 option:selected').val() }
		)

