


$('#collection_modal_partial').html("<%= j render 'rootshop/partial/collection_modal_partial', 
	:product => @product, :collections => @collections %>")

$('.collection_items').mouseenter (e) ->
		$(@).find('.collection_delete_btn').css('display', 'inline')
		$(@).find('.collection_added_btn').css('display', 'none')

$('.collection_items').mouseleave (e) ->
	$(@).find('.collection_delete_btn').css('display', 'none')
	$(@).find('.collection_added_btn').css('display', 'inline')

$('#collection_cancel_btn').on 'click', ->
		$('#collection_name').val('')
		$('#collection_add_form').hide()
		$('#collection_add_segment').show()

$('#collection_add_btn').on 'click', ->
	$('#collection_add_form').show()
	$('#collection_add_segment').hide()

$('#collection_create_btn').on 'click', ->
		if $('#collection_name').val() != ''
			$('#collection_add_submit').click()


$('.ui.modal').modal('refresh')