class ProductSerializer < ActiveModel::Serializer
  attributes :id, :title, :created_at
  # embed :ids, include: true
            
  has_many :product_images, embed: :objects
end