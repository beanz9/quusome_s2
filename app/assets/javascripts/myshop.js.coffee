$ ->
	$('#option_setting_check').on 'click', ->
		if @.checked
			$('#product_options_div').show()
		else
			$('#product_options_div').hide()

	$('.featured_reg_modal_btn').on 'click', ->
		collection_name = $(@).next().val()
		collection_image = $(@).next().next().val()
		collection_owner = $(@).next().next().next().val()
		collection_id  = $(@).next().next().next().next().val()

		$('#collection_name').text(collection_name)
		$('#collection_image').attr('src', collection_image)
		$('#collection_owner').text(collection_owner)
		$('#featured_collection_collection_id').val(collection_id)
		$('.ui.modal').modal('show')

	$('#modal_submit').on 'click', ->
		
		$('#submit_btn').click()
		$('#featured_collection_title').val("")
		$('#featured_collection_description').val("")
		$('#featured_collection_collection_id').val("")

	$('#modal_close').on 'click', ->
		$('#featured_collection_title').val("")
		$('#featured_collection_description').val("")
		$('#featured_collection_collection_id').val("")
		$('.ui.modal').modal('hide')

	$('.featured_product_reg_modal_btn').on 'click', ->
		product_name = $(@).next().val()
		product_image = $(@).next().next().val()
		product_id  = $(@).next().next().next().val()
		product_is_sale = $(@).next().next().next().next().val()

		$('#product_name').text(product_name)
		$('#product_image').attr('src', product_image)
		$('#featured_product_product_id').val(product_id)
		$('#is_sale').text(product_is_sale)
		$('.ui.product_modal').modal('show')

	$('#product_modal_close').on 'click', ->
		$('#featured_product_title').val("")
		$('#featured_product_description').val("")
		$('#featured_product_product_id').val("")
		$('#is_sale').val("")
		$('.ui.modal').modal('hide')

	$('#product_modal_submit').on 'click', ->
		
		$('#submit_btn').click()
		$('#featured_product_title').val("")
		$('#featured_product_description').val("")
		$('#featured_product_product_id').val("")
		('#is_sale').val("")

