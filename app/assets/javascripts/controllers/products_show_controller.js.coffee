App.ProductsShowController = Em.ObjectController.extend

	actions:

		destroy: ->
			@content.deleteRecord()
			@store.commit()
			@transitionToRoute('products.index')
