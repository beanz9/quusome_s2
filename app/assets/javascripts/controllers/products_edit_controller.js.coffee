App.ProductsEditController = Em.ObjectController.extend
	actions:
		save: ->
			@content.save().then =>
				@transitionToRoute('products.show', @content)

		cancel: ->
			if @content.isDirty
				@content.rollback()
			@transitionToRoute('products.show', @content)

		
			