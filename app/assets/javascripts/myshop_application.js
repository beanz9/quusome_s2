// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require handlebars
//= require ember
//= require ember-data
//= require_self
//= require quusome_s2
//= require semantic.min
//= require jquery.remotipart
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require jquery_nested_form
//= require myshop


// for more details see: http://emberjs.com/guides/application/
// App = Ember.Application.create({
// 	LOG_TRANSITIONS: true,
// 	LOG_VIEW_LOOKUPS: true,
// 	LOG_ACTIVE_GENERATION: true
// });

//= require_tree .
