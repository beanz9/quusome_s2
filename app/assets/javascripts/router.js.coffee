# For more information see: http://emberjs.com/guides/routing/
App.Router.reopen
	rootUrl: '/'


App.Router.map ()->

  @resource 'products', ->
  	@route 'new'
  	@route 'show', path: '/:product_id'
  	@route 'edit', path: '/:product_id/edit'

  @resource 'orders'
  @resource 'promotions'


