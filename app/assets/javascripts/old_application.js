// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.unveil
//= require bootstrap.min


//= require uikit/core
//= require uikit/search
//= require uikit/alert
//= require uikit/button
//= require uikit/grid
//= require uikit/dropdown
//= require uikit/tooltip
//= require semantic-2.min

//= require products
//= require rootshop
//= require collectionshop
//= require item
//= require orders
//= require line_items
//= require mypage
//= require profits
//= require myshop

//= require jquery.tagsinput
//= require cocoon
//= require jquery_nested_form
//= require jquery-fileupload/basic
//= require jquery.remotipart
//= require moment
//= require moment/ko.js
//= require imagesloaded.pkgd.min
//= require jquery.wookmark.min
//= require owl.carousel.min.js
//= require legacy.min
//= require modal

//= require picker
//= require picker.date
//= require pickadate_ko_KR
//= require hoverIntent
//= require superfish
//= require ckeditor/override
//= require ckeditor/init






// $(document).on('page:fetch', function() {

//   $(".loading-indicator").show();
// });
// $(document).on('page:change', function() {
//   $(".loading-indicator").hide();
// });

// $(document).on("page:restore", function() {
//   $(document).on("mouseenter.tooltip.uikit focus.tooltip.uikit", "[data-uk-tooltip]", function(e) {
// 	  var ele = $(this);

// 	  if (!ele.data("tooltip")) {
// 	      ele.data("tooltip", new Tooltip(ele, UI.Utils.options(ele.data("uk-tooltip")))).trigger("mouseenter");
// 	  }
//   });
// });

$.fn.rowspan = function(colIdx, isStats) {       
    return this.each(function(){      
        var that;     
        $('tr', this).each(function(row) {      
            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                
                if ($(this).attr('merge_id') == $(that).attr('merge_id')
                    && (!isStats 
                            || isStats && $(this).prev().html() == $(that).prev().html()
                            )
                    ) {            
                    rowspan = $(that).attr("rowspan") || 1;
                    rowspan = Number(rowspan)+1;
 
                    $(that).attr("rowspan",rowspan);
                     
                    // do your action for the colspan cell here            
                    $(this).hide();
                     
                    //$(this).remove(); 
                    // do your action for the old cell here
                     
                } else {            
                    that = this;         
                }          
                 
                // set the that if not already set
                that = (that == null) ? this : that;      
            });     
        });    
    });  
}; 

$.fn.colspan = function(rowIdx) {
  return this.each(function(){
       
      var that;
      $('tr', this).filter(":eq("+rowIdx+")").each(function(row) {
          $(this).find('th').filter(':visible').each(function(col) {
              if ($(this).html() == $(that).html()) {
                  colspan = $(that).attr("colSpan") || 1;
                  colspan = Number(colspan)+1;
                   
                  $(that).attr("colSpan",colspan);
                  $(this).hide(); // .remove();
              } else {
                  that = this;
              }
               
              // set the that if not already set
              that = (that == null) ? this : that;
               
          });
      });
  });
}

function inputsToArray (inputs) {
  var arr = [];
  for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].checked)
          arr.push(inputs[i].value);
  }
  return arr;
}

function serializeArray (array, name) {
  var serialized = '';
  for(var i = 0, j = array.length; i < j; i++) {
      if(i>0) serialized += '&';
      serialized += name + '=' + array[i];
  }
  return serialized;
}

function get_query_data(obj) {
  var check_obj = $(obj).parent().parent().parent().prev().find("input[name='cart_ids[]']");
  var idArray = inputsToArray(check_obj);
  return serializeArray(idArray, 'cart_ids[]');
}

$(function() {
  //선택한 카트 리스트 삭제
  $('.delete_selected_item_btn').on('click', function() {

    if($("input[name='cart_ids[]']:checked").length == 0) {
      alert("삭제할 상품을 선택하세요.");
      return;
    }

    url = $(this).parent().parent().parent().find("form").attr('action');
    url = url + "?" + get_query_data(this);

    $('#delete_selected_item_form').attr('action', url);
    $('#delete_selected_item_submit').click();
  });

  $('#order_selected_item_btn').on('click', function() {

    var is_available = true
    var checked_obj = $(this).parent().parent().prev().find("input[name='cart_ids[]']:checked");
    if(checked_obj.length == 0) {
      alert("주문할 상품을 선택하세요.");
      return;
    }


    for(var i = 0; i < checked_obj.length; i++) {
      if ($(checked_obj[i]).next().val() == 'true') {
        alert("품절된 상품이 있습니다.");
        is_available = false;
        return;
      }
    }

    if (!is_available) {
      return false;
    }


    var check_obj = $(this).parent().parent().prev().find("input[name='cart_ids[]']");

    var idArray = inputsToArray(check_obj);
    var data = serializeArray(idArray, 'cart_ids[]');
    // url = $('#order_selected_item').attr('href');
    // url = url + "?order_type=select&" + data;
    url = "?order_type=select&" + data;

    window.location.href = "/orders/new" + url;
    // $('#order_selected_item').attr('href', url);
    // $('#order_selected_item').click();
  });

  $('#order_item_btn').on('click', function() {
    var is_available = true

    $('.is_soldout').each(function() {
      if ($(this).val() == 'true') {
        alert("품절된 상품이 있습니다.");
        is_available = false
        return;
      }
    })

    if (is_available) {
      $('#order_item_submit').click();
    }
      

  });

  $('ul.sf-menu').superfish();

  $('.ui.checkbox').checkbox();

  
  options = {
    autoResize: true,
    container: $('#main'),
    offset: 10,
    itemWidth: 280
  }

  handler = $('.items li');
  handler.wookmark(options);

  $('.collection_add_btn').on('click', function() {
    if($('#modal_user_id').val() != "") {
      $('.ui.modal.collection_add')
      .modal('setting', {
        onHide    : function(){
          $('#delete_collection_id').val("");
          $('.collection_remove_dimmer').dimmer('hide');
          return false;
        }
      })
      .modal('show')
    }else {
      alert("로그인이 필요합니다.");
      window.location.href = '/users/sign_in'
    } 
  });



  $(document.body).on('submit', '.collection_item_add_form', function() {
    $('.collection_item_add_dimmer')
    .dimmer({
     closable: false
    })
    .dimmer('show')
    return true;
  });

  $(document.body).on('submit', '.collection_item_remove_form', function() {
    $('.collection_item_remove_dimmer')
    .dimmer({
     closable: false
    })
    .dimmer('show')
    return true;
  });

  $(document.body).on('submit', '.collection_add_form', function() {
    $('.collection_add_dimmer')
    .dimmer({
     closable: false
    })
    .dimmer('show')
    return true;
  });

  $(document.body).on('submit', '.collection_remove_form', function() {
    // confirm("컬렉션을 삭제하시겠습니까?\n컬렉션에 포함된 상품도 삭제됩니다.")
    $('.collection_remove_dimmer')
    .dimmer({
     closable: false
    })
    .dimmer('show')
    return true;
  });

  $(document.body).on('click', '.collection_remove_btn', function() {
    $('.collection_remove_dimmer')
    .dimmer({
     closable: false
    })
    .dimmer('show')
    // if(confirm("컬렉션을 삭제하시겠습니까?\n컬렉션에 포함된 상품도 삭제됩니다.")) {
    $('#delete_collection_id').val(($(this).next().val()));
      // $(this).next().submit();
    // }
    $('.collection_remove_div').hide();
    $('.collection_remove_btn_div').show();
  });

  $(document.body).on('click', '#collection_delete_cancel_btn', function() {
    $('#delete_collection_id').val("");
    $('.collection_remove_dimmer').dimmer('hide');
    return true;
  });

  $(document.body).on('click', '#collection_delete_accept_btn', function() {
    
    $('.collection_remove_div').show();
    $('.collection_remove_btn_div').hide();

    var url = '/collection_remove/ '+ $('#delete_collection_id').val() + '?product_id=' + $('#delete_product_id').val();
    $('.collection_remove_form').attr('action', url)

    $('.collection_remove_form').submit();
    return true;

  });

});



// $(window).load(function() {
//   options = {
//     autoResize: true,
//     container: $('#main'),
//     offset: 10,
//     itemWidth: 280
//   }

//   handler = $('.items li');
//   handler.wookmark(options);

// });







