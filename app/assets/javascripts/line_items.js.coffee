$ ->
	$('.cart_check_all').on 'click', ->
		$(@).parent().parent().parent().parent().find($('input:checkbox')).not(@).prop('checked', @.checked);
	
	$('select').on 'change', ->
		if $(@).attr('option') != 'color' 
			return

		size_option = $(@).parent().next().find($('select'))
		selected_color = $(@).children(':selected').text()
		size_option.children().remove()

		$(@).parent().next().find($('.hidden_size_option')).each ->
			if getInventoryState(selected_color, $(@).attr('title'))
				size_option.append("<option value='" + $(this).val() + "'>" + $(this).attr('title') + "</option>")
			else
				size_option.append("<option disabled value='" + $(this).val() + "'>" + $(this).attr('title') + " - 품절</option>")

		

getInventoryState = (color, size) ->

	state = document.getElementById("color*_*" + color + "-_-size*_*" + size).value

	if state == 'available' 
    return true
  else
    return false