$ ->
	#아바타 이미지 변경
	$('#avatar_image_upload').fileupload(
		url: '/avatar_update'
		dataType: 'json'


		done: (e, data) ->
			$('#avatar_image').attr('src', "#{data.result.small_image_url}")

		add: (e, data) -> 
			types = /(\.|\/)(gif|jpe?g|png)$/i
			file = data.files[0]
			if types.test(file.type) || types.test(file.name)
      	#data.context = $('<p/>').text('Uploading...').appendTo(document.body)
      	data.submit()
      else
      	alert("#{file.name} 은 이미지 파일이 아닙니다.")

		progressall: (e, data) ->

      progress = parseInt(data.loaded / data.total * 100, 10)
      console.log(data)
      console.log(progress)
      # $('#progress .bar').css('width', progress + '%')
	)

	#컬렉션 이미지 변경
	$('#collection_image_upload').fileupload(
		url: "/collection_manage/image_update/" + $('#collection_id').val()
		dataType: 'json'


		done: (e, data) ->
			$('#collection_image').attr('src', "#{data.result.small_image_url}")

		add: (e, data) -> 
			types = /(\.|\/)(gif|jpe?g|png)$/i
			file = data.files[0]
			if types.test(file.type) || types.test(file.name)
      	#data.context = $('<p/>').text('Uploading...').appendTo(document.body)
      	data.submit()
      else
      	alert("#{file.name} 은 이미지 파일이 아닙니다.")

		progressall: (e, data) ->

      progress = parseInt(data.loaded / data.total * 100, 10)
      console.log(data)
      console.log(progress)
      # $('#progress .bar').css('width', progress + '%')
	)

	$('.mypage_menu a.item').unbind()

	#컬렉션 이름 변경 버튼

	$('#collection_name_change_btn').on 'click', ->
		$('#collection_name_div').hide()
		$('#collection_name_mod_div').show()

	#컬렉션 이름 변경 취소 버튼
	$('#collection_name_change_cancel_btn').on 'click', ->
		$('#collection_name_div').show()
		$('#collection_name_mod_div').hide()

	#컬렉션 이름 변경 수락 버튼
	$('#collection_name_change_accept_btn').on 'click', ->
		if $("input[name='collection_name']").val() == ''
			return

		$('#collection_name_change_submit').click()

	$('#tab_products').on 'click', ->
		$('#tab_products_div').show()
		$('#collection_comments').hide()
		$('#tab_products').addClass('active')
		$('#tab_comments').removeClass('active')

	$('#tab_comments').on 'click', ->
		$('#tab_products_div').hide()
		$('#collection_comments').show()
		$('#tab_products').removeClass('active')
		$('#tab_comments').addClass('active')

