$ ->
	$('#zipcode_search').on 'click', ->
		$('#zipcode_search_modal').modal('setting', 'transition', '').modal('show')

	$('input:radio[name=address_type]').on 'change', ->
		if @.value == 'new'
			$('#order_recipient').val('')
			$('#order_recipient_en').val('')
			$('#order_email').val('')
			$('#order_zipcode').val('')
			$('#order_address').val('')
			$('#order_detail_address').val('')
			$('#order_mobile_phone_1').val('')
			$('#order_mobile_phone_2').val('')
			$('#order_mobile_phone_3').val('')
			$('#order_phone_1').val('')
			$('#order_phone_2').val('')
			$('#order_phone_3').val('')
		else 
			$('#order_recipient').val($('#hidden_recipient').val())
			$('#order_recipient_en').val($('#hidden_recipient_en').val())
			$('#order_email').val($('#order_email').val())
			$('#order_zipcode').val($('#hidden_zipcode').val())
			$('#order_address').val($('#hidden_address').val())
			$('#order_detail_address').val($('#hidden_detail_address').val())
			$('#order_mobile_phone_1').val($('#hidden_mobile_phone_1').val())
			$('#order_mobile_phone_2').val($('#hidden_mobile_phone_2').val())
			$('#order_mobile_phone_3').val($('#hidden_mobile_phone_3').val())
			$('#order_phone_1').val($('#hidden_phone_1').val())
			$('#order_phone_2').val($('#hidden_phone_2').val())
			$('#order_phone_3').val($('#hidden_phone_3').val())

	$('#order_list').rowspan(0)
	$('#order_list').rowspan(5)

	$('#myshop_order_list').rowspan(0)
	$('#myshop_order_list').rowspan(5)

	$('.datepicker').pickadate(
		format: 'yyyy-mm-dd'
	)

	$('#search_fields_init').on 'click', ->
		$('#state').val('')
		$('input[name=title]').val('')
		$('input[name=start_date]').val('')
		$('input[name=end_date]').val('')

	$('.change_state_btn').on 'click', ->
		if confirm($(@).attr('state_name') + "하시겠습니까?")
			old_link = $("#" + $(@).attr('id') + "_link").attr('href')

			link = $("#" + $(@).attr('id') + "_link")
			

			if $('#take_back_request_id').val() == ""
				link.attr('href', link.attr('href') + "?desc=" + $('#state_desc').val())
			else
				link.attr('href', link.attr('href') + "?desc=" + $('#state_desc').val() + "&take_back_request_id=" + $('#take_back_request_id').val())
			link.click()

			link.attr('href', old_link)

	$('#take_back_request_btn').on 'click', ->
		if $('#note_check').is(":checked") == false
			alert("유의사항을 읽어 보시고 체크해 주세요.")
			return

		if $('#take_back_request_reason').val() == ""
			alert("반품사유를 입력하세요.")
			return

		if $('.reason_type:checked').val() == "2"
			if $('#take_back_request_image_1').val() == ""
				alert("제품 하자일 경우 사진을 첨부하세요.")
				return

		if confirm("반품접수를 하시겠습니까?")
			$('#take_back_request_submit').click()

	$('#checkout_btn').on 'click', ->

		if validate()

			if $('#order_saved_money').val()

		  	$('#order_checkout_price').val(parseInt($('#order_original_price').val()) - parseInt($('#order_saved_money').val()))	
			else
				$('#order_checkout_price').val($('#order_original_price').val())
			
			# $('#pay_method').val($('#order_payment_method').val())
			
			$('#pay_method').val($('#order_payment_method').val())
			$('#buyr_name').val($('#order_recipient').val())
			tel = $('#order_phone_1').val() + "-" + $('#order_phone_2').val() + "-" + $('#order_phone_3').val()
			mobile = $('#order_mobile_phone_1').val() + "-" + $('#order_mobile_phone_2').val() + "-" + $('#order_mobile_phone_3').val()
			$('#buyr_tel1').val(tel)
			$('#buyr_tel2').val(mobile)
			$('#rcvr_name').val($('#order_recipient').val())
			$('#rcvr_tel1').val(tel)
			$('#rcvr_tel2').val(mobile)
			$('#rcvr_mail').val($('#order_zipcode').val())
			$('#rcvr_zipx').val($('#order_zipcode').val().replace('-', ''))
			$('#rcvr_add1').val($('#order_address').val())
			$('#rcvr_add2').val($('#order_detail_address').val())


			if jsf__pay(document.new_order)
				$('#checkout_submit_btn').click()


	$('input.numberinput').bind 'keypress', (e) ->
		return !(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46)

	$('.saved_money_for_order_item').bind 'keyup paste', (e) ->
		saved_money_sum = 0

		$('.saved_money_for_order_item').each ->
			saved_money_sum = saved_money_sum + Number($(@).val())

		if saved_money_sum > Number($('#hidden_current_saved_money').val())
			alert("적립금을 초과했습니다.")
			$(@).val("")
			$(@).keyup()
			return false

		order_item_price = $(@).next().val()

		if $(@).val()
			$(@).parent().next().find('.order_item_price').css("text-decoration", "line-through")
			$(@).parent().next().find('.div_order_item_price_with_saved_money').show()
			$(@).parent().next().find('.order_item_price_with_saved_money').text((Number(order_item_price) - Number($(@).val())).toLocaleString('ko'))
		else
			$(@).parent().next().find('.order_item_price').text(Number(order_item_price).toLocaleString('ko'))
			$(@).parent().next().find('.order_item_price').css("text-decoration", "")
			$(@).parent().next().find('.div_order_item_price_with_saved_money').hide()
			$(@).parent().next().find('.order_item_price_with_saved_money').text("")

		$('#current_saved_money_sum').text(saved_money_sum.toLocaleString('ko'))
		$('#current_saved_money_label').text((Number($('#hidden_current_saved_money').val()) - saved_money_sum).toLocaleString('ko'))
		# $('#price_sum').text((Number($('#hidden_price_sum').val()) - saved_money_sum).toLocaleString('ko'))

		if saved_money_sum > 0
			$('#price_sum').css("text-decoration", "line-through")
			$('.div_order_item_price_sum_with_saved_money').show()
			$('.order_item_price_sum_with_saved_money').text((Number($('#hidden_price_sum').val()) - saved_money_sum).toLocaleString('ko'))
			$('#good_mny').val(Number($('#hidden_price_sum').val()) - saved_money_sum)
		else
			$('#price_sum').css("text-decoration", "")
			$('.div_order_item_price_sum_with_saved_money').hide()
			$('.order_item_price_sum_with_saved_money').text("")
			$('#good_mny').val($('#hidden_price_sum').val())

		$(@).next().next().val(($(@).next().next().next().val()) + "_" + $(@).val())

	$('input[type=radio][name="order[custom_type]"]').on 'change', ->
		if $(@).val() == 'ssn' 
			$('#through_customs_ssn').show()
			$('#through_customs_private').hide()
		else
			$('#through_customs_ssn').hide()
			$('#through_customs_private').show()

	$('#ssn_agree').on 'change', ->
		if $(@).is(':checked')
			$('#ssn_field').removeClass('disabled')
		else
			$('#ssn_field').addClass('disabled')

validate = ->
	if $('#order_recipient').val() == ''
		alert("받는사람(한글)을 입력하세요.")
		$('#order_recipient').focus()
		return false

	if $('#order_recipient_en').val() == ''
		alert("받는사람(영문)을 입력하세요.")
		$('#order_recipient_en').focus()
		return false

	if $('#order_email').val() == ''
		alert("이메일을 입력하세요.")
		$('#order_email').focus()
		return false
	
	if $('#order_zipcode').val() == ''
		alert("우편번호를 입력하세요.")
		$('#order_zipcode').focus()
		return false

	if $('#order_address').val() == ''
		alert("주소를 입력하세요.")
		$('#order_address').focus()
		return false

	if $('#order_mobile_phone_1').val() == ''
		alert("휴대전화 번호를 입력하세요.")
		$('#order_mobile_phone_1').focus()
		return false

	if $('#order_mobile_phone_2').val() == ''
		alert("휴대전화 번호를 입력하세요.")
		$('#order_mobile_phone_2').focus()
		return false

	if $('#order_mobile_phone_3').val() == ''
		alert("휴대전화 번호를 입력하세요.")
		$('#order_mobile_phone_3').focus()
		return false



	if $('input[type=radio][name="order[custom_type]"]:checked').val() == 'ssn'

		if !$('#ssn_agree').is(':checked')
			alert("주민등록번호 제공에 체크해 주세요.")
			return;
		
		if $('#order_rrn_1').val() == ''
			alert("주민등록번호 앞자리를 입력하세요.")
			$('#order_rrn_1').focus()
			return false

		if $('#order_rrn_2').val() == ''
			alert("주민등록번호 뒷자리를 입력하세요.")
			$('#order_rrn_2').focus()
			return false

		if $('#order_rrn_1').val().length != 6
			alert("주민등록번호 앞자리를 정확히 입력하세요.")
			$('#order_rrn_1').focus()
			return false

		if $('#order_rrn_2').val().length != 7
			alert("주민등록번호 뒷자리를 정확히 입력하세요.")
			$('#order_rrn_2').focus()
			return false
	else
		if $('#order_private_custom_number').val() == ''
			alert("개인통관고유부호를 입력주세요.")
			return false


	if parseInt($('#order_saved_money').val()) > parseInt($('#order_original_price').val())
		alert("적립금은 구매액을 초과할 수 없습니다.")
		$('#order_saved_money').focus()
		return false

	return true









