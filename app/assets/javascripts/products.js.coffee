# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	$('#tags').tagsInput(
		defaultText:'키워드'
		width:'400px'
	)

	$('.product_submit').on 'click', ->
		$('#product_submit_btn').click()

	$('#product_image_file').fileupload(
		url: '/upload'
		dataType: 'json'
		type: 'POST'
		formData: {
			_method: 'POST'
		}


		done: (e, data) ->


			

   		$('.image_item').each (item) ->
	   		if $(@).children("img").length == 0 
	   			console.log(data.result)
	   			html = "<img src='#{data.result.upload.small_image_url}' class='uk-thumbnail upload_id_#{data.result.upload.id}'>"
	   			html += "<a class='upload_id_#{data.result.upload.id}' style='position:relative; left: 65px; top: -93px; text-decoration: none;' data-confirm='삭제하시겠습니가?'' data-method='delete' data-remote='true' href='/upload/#{data.result.upload.id}' rel='nofollow'>"
	   			html += "<i class='square inverted teal close icon link'></i></a>"

	   			data.context = $(@).append(html)
	   			$(@).find("#product_product_images_attributes_upload_id").val(data.result.upload.id)
	   			false
   		#console.log(data.result.upload.small_image_url)

		add: (e, data) -> 
			types = /(\.|\/)(gif|jpe?g|png)$/i
			file = data.files[0]
			if types.test(file.type) || types.test(file.name)
      	#data.context = $('<p/>').text('Uploading...').appendTo(document.body)
      	data.submit()
      else
      	alert("#{file.name} 은 이미지 파일이 아닙니다.")

		progressall: (e, data) ->

      progress = parseInt(data.loaded / data.total * 100, 10)
      console.log(data)
      console.log(progress)
      # $('#progress .bar').css('width', progress + '%')
	)
