// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs


//= require uikit/core
//= require uikit/search
//= require uikit/alert
//= require uikit/button
//= require uikit/grid
//= require uikit/dropdown
//= require uikit/tooltip
//= require semantic/library/tablesort
//= require semantic/library/waypoints
//= require semantic.min
//= require semantic/semantic
//= require semantic/menu
//= require semantic/popup
//= require bootstrap-dropdowns
//= require products
//= require rootshop
//= require collectionshop
//= require item
//= require orders
//= require line_items
//= require mypage
//= require profits
//= require myshop

//= require jquery.tagsinput
//= require cocoon
//= require jquery_nested_form
//= require jquery-fileupload/basic
//= require jquery.remotipart
//= require moment
//= require moment/ko.js
//= require imagesloaded.pkgd.min
//= require jquery.wookmark.min
//= require owl.carousel.min.js
//= require legacy.min
//= require modal

//= require picker
//= require picker.date
//= require pickadate_ko_KR
//= require hoverIntent
//= require superfish
//= require ckeditor/override
//= require ckeditor/init






// $(document).on('page:fetch', function() {

//   $(".loading-indicator").show();
// });
// $(document).on('page:change', function() {
//   $(".loading-indicator").hide();
// });

// $(document).on("page:restore", function() {
//   $(document).on("mouseenter.tooltip.uikit focus.tooltip.uikit", "[data-uk-tooltip]", function(e) {
// 	  var ele = $(this);

// 	  if (!ele.data("tooltip")) {
// 	      ele.data("tooltip", new Tooltip(ele, UI.Utils.options(ele.data("uk-tooltip")))).trigger("mouseenter");
// 	  }
//   });
// });

$.fn.rowspan = function(colIdx, isStats) {       
    return this.each(function(){      
        var that;     
        $('tr', this).each(function(row) {      
            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                
                if ($(this).attr('merge_id') == $(that).attr('merge_id')
                    && (!isStats 
                            || isStats && $(this).prev().html() == $(that).prev().html()
                            )
                    ) {            
                    rowspan = $(that).attr("rowspan") || 1;
                    rowspan = Number(rowspan)+1;
 
                    $(that).attr("rowspan",rowspan);
                     
                    // do your action for the colspan cell here            
                    $(this).hide();
                     
                    //$(this).remove(); 
                    // do your action for the old cell here
                     
                } else {            
                    that = this;         
                }          
                 
                // set the that if not already set
                that = (that == null) ? this : that;      
            });     
        });    
    });  
}; 

$.fn.colspan = function(rowIdx) {
  return this.each(function(){
       
      var that;
      $('tr', this).filter(":eq("+rowIdx+")").each(function(row) {
          $(this).find('th').filter(':visible').each(function(col) {
              if ($(this).html() == $(that).html()) {
                  colspan = $(that).attr("colSpan") || 1;
                  colspan = Number(colspan)+1;
                   
                  $(that).attr("colSpan",colspan);
                  $(this).hide(); // .remove();
              } else {
                  that = this;
              }
               
              // set the that if not already set
              that = (that == null) ? this : that;
               
          });
      });
  });
}

function inputsToArray (inputs) {
  var arr = [];
  for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].checked)
          arr.push(inputs[i].value);
  }
  return arr;
}

function serializeArray (array, name) {
  var serialized = '';
  for(var i = 0, j = array.length; i < j; i++) {
      if(i>0) serialized += '&';
      serialized += name + '=' + array[i];
  }
  return serialized;
}

function get_query_data(obj) {
  var check_obj = $(obj).parent().parent().parent().prev().find("input[name='cart_ids[]']");
  var idArray = inputsToArray(check_obj);
  return serializeArray(idArray, 'cart_ids[]');
}

$(function() {
  //선택한 카트 리스트 삭제
  $('.delete_selected_item_btn').on('click', function() {

    if($("input[name='cart_ids[]']:checked").length == 0) {
      alert("삭제할 상품을 선택하세요.");
      return;
    }

    url = $(this).parent().parent().parent().find("form").attr('action');
    url = url + "?" + get_query_data(this);

    $('#delete_selected_item_form').attr('action', url);
    $('#delete_selected_item_submit').click();
  });

  $('#order_selected_item_btn').on('click', function() {

    var checked_obj = $(this).parent().parent().prev().find("input[name='cart_ids[]']:checked");
    if(checked_obj.length == 0) {
      alert("주문할 상품을 선택하세요.");
      return;
    }

    var check_obj = $(this).parent().parent().prev().find("input[name='cart_ids[]']");

    var idArray = inputsToArray(check_obj);
    var data = serializeArray(idArray, 'cart_ids[]');
    // url = $('#order_selected_item').attr('href');
    // url = url + "?order_type=select&" + data;
    url = "?order_type=select&" + data;

    window.location.href = "/orders/new" + url;
    // $('#order_selected_item').attr('href', url);
    // $('#order_selected_item').click();
  });

  $('#order_item_btn').on('click', function() {
    var arr = [];
    var is_available = true

    $('select').each(function() {
      arr.push($(this).children(":selected").attr("title"))
      if(arr.length == 2) {
        if(document.getElementById($(this).attr('title') + '_color*_*' + arr[0] + '-_-size*_*' + arr[1]).value == 'disavailable') {
          alert($(this).attr('title') + " 상품은 품절되었습니다. 확인 후 주문해 주세요.")
          is_available = false
        }
        arr = [];
      }
    });

    if(is_available) {
      alert("!!!")
      window.location.href = "/orders/new"
      // $('#order_item_submit').click();
    }

      

  });

  $('ul.sf-menu').superfish();

  $('.ui.checkbox').checkbox();




});







