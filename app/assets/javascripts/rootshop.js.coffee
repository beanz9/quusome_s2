$ ->

	# options = (
	# 	autoResize: true
	# 	container: $('#main')
	# 	offset: 10
	# 	itemWidth: 280
	# )

	# handler = $('.items li')
	# handler.wookmark(options)


	# $('.items').imagesLoaded ->
	# 	$('.items li').wookmark(
	# 		autoResize: true
	# 		itemWidth: 280
	# 		offset: 10

	# 		container: $('#main')
	# 		align: 'left'
	# 	)



	# $('.btn').on 'mouseenter', ->
 #  	$(@).css('background-color', '')

 #  $('.btn').on 'mouseleave', ->
 #  	$(@).css('background-color', 'rgba(0, 0, 0, 0.35)')

  $('#cart_btn').on 'click', ->
  	# select_option_id = $('#line_item_product_option_id').children(":selected").attr("value")
  	# if $('#product_amount').val() == '0' or $('#hidden_option_id_' + select_option_id).val() == '0'
  	# 	alert("재고가 없습니다.")
  	# else
  	if add_cart_check()
  		$('#cart_submit_btn').click()
  		
  		# if Number($('#product_amount').val()) < Number($('#line_item_quantity').val())
  		# 	alert("재고 수량을 초과했습니다.")

	$('#direct_purchase_btn').on 'click', ->
  	# if $('#rails_env').val() == 'production'
  	# 	alert("준비중입니다.")
  	# 	return
		if not $('#user_id').val() 
			alert("로그인이 필요합니다.")
			window.location.href = '/users/sign_in'
		else
			if add_cart_check()
				$('#order_type').val('direct')
				$('#cart_submit_btn').click()
		

	$('#comment_submit_btn').on 'click', ->
		if $('#comment').val()
			if not $('#user_id').val() 
				alert("로그인이 필요합니다.")
				window.location.href = '/users/sign_in'
			else
				$('#comment_submit_btn').addClass('loading')
				$('#comment_submit').click()
				
		

	$('.timeago').each ->
		date = moment(new Date($(@).attr('data-date')))
		$(@).html(date.fromNow())

	# $('.collection_add_btn').on 'click', ->
	# 	if $('#modal_user_id').val() 
	# 		$('.ui.modal.collection_add').
	# 		modal('setting', 'transition', 'fade').
	# 		# modal('setting', 'detachable', false).
	# 		modal('show')
	# 		# $('.ui.modal.collection_add').modal('setting', 'closable', 'false').modal('show')
	# 	else
	# 		alert("로그인이 필요합니다.")
	# 		window.location.href = '/users/sign_in'

	$('#size_table_btn').on 'click', ->

		$('.ui.modal.price_size_table').
		modal('setting', 'transition', 'fade').
		# modal('setting', 'detachable', false).
		# modal('setting', 'onHidden', () -> 
		# 	$('.ui.modal.price_size_table').modal('setting', 'destroy')
		# ).
		modal('show')
		# $('.ui.modal.price_size_table').modal('setting', 'closable','false').modal('show')

	# $('.ui.modal > .close').on 'click', ->
	# 	$('.ui.modal').modal('hide all')

	$('.collection_items').mouseenter (e) ->
		$(@).find('.collection_delete_btn').css('display', 'inline')
		$(@).find('.collection_added_btn').css('display', 'none')
	
	 

	$('.collection_items').mouseleave (e) ->
		$(@).find('.collection_delete_btn').css('display', 'none')
		$(@).find('.collection_added_btn').css('display', 'inline')

	$('#collection_create_btn').on 'click', ->
		if $('#collection_name').val()
			$('#collection_add_submit').click()

	$('#collection_cancel_btn').on 'click', ->
		$('#collection_name').val('')
		$('#collection_add_form').hide()
		$('#collection_add_segment').show()

	$('#collection_add_btn').on 'click', ->
		$('#collection_add_form').show()
		$('#collection_add_segment').hide()

	$('#tab_warning_order').on 'click', ->
		alert("!!!!")
		$('#tab_warning_order_content').show()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').addClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').removeClass('active')

	$('#tab_detail_info').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').show()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').addClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').removeClass('active')


	$('#tab_reviews').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').show()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').addClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').removeClass('active')




	$('#tab_comments').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').show()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').addClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').removeClass('active')


	$('#tab_delivery').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').show()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').addClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').removeClass('active')


	$('#tab_cancel_order').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').show()
		$('#tab_inquiry_comment_content').hide()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').addClass('active')
		$('#tab_inquiry_comment').removeClass('active')


	$('#tab_inquiry_comment').on 'click', ->
		$('#tab_warning_order_content').hide()
		$('#tab_detail_info_content').hide()
		$('#tab_reviews_content').hide()
		$('#tab_comments_content').hide()
		$('#tab_delivery_content').hide()
		$('#tab_cancel_order_content').hide()
		$('#tab_inquiry_comment_content').show()

		$('#tab_warning_order').removeClass('active')
		$('#tab_detail_info').removeClass('active')
		$('#tab_reviews').removeClass('active')
		$('#tab_comments').removeClass('active')
		$('#tab_delivery').removeClass('active')
		$('#tab_cancel_order').removeClass('active')
		$('#tab_inquiry_comment').addClass('active')

	$('#comment_button').on 'click', ->
		$('#comment_button').hide()
		$('#comment_add_form').show()

	
add_cart_check = ->
	selected = true

	$('select').each ->
		if $(@).children(':selected').val() == ""
			selected = false
			alert($(@).attr('option') + " 옵션을 선택하세요.")
	return selected

	







	





