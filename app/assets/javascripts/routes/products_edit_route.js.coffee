App.ProductsEditRoute = Em.Route.extend
	model: (params) ->
		@store.find('product', params.product_id)

	setController: (controller, model) ->
		controller.set('content', model)

	