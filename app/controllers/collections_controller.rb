class CollectionsController < ApplicationController
	layout 'rootshop'

	def index
		@collection_user = User.find(params[:user_id])
		@collections = Collection.where(:user_id => params[:user_id])
	end
end