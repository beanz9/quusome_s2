class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # include SentientController
  # include PublicActivity::StoreController 

  
  protect_from_forgery with: :exception
  before_filter :set_locale, :seller_validate
  before_filter :configure_permitted_parameters, if: :devise_controller?
  after_filter :store_location


  include SentientController


protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:email, :password, :password_confirmation, :user_id, :user_type, :deleted_at)}
 
  end

private

  def check_browser
    if browser.ie? and browser.version.to_i < 10
      render 'browser/index', :layout => false
    end 
  end



  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    return unless request.get? 
    if (request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation" &&
        request.path != "/users/sign_out" &&
        !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath 
    end
  end


  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end

  # def after_sign_up_path_for(resource)
  #   session[:previous_url] || '/'
  # end


  # set the language
  def set_locale
    if params[:locale].blank?
      I18n.locale = extract_locale_from_accept_language_header
    else
      I18n.locale = params[:locale]
    end
  end

  # pass in language as a default url parameter
  #def default_url_options(options = {})
  #  {locale: I18n.locale}
  #end

  # extract the language from the clients browser
  def extract_locale_from_accept_language_header
    
    browser_locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first


    if I18n.available_locales.include? browser_locale.to_sym
      browser_locale
    else
      I18n.default_locale
    end
  end

  def seller_validate
    if controller_path.start_with?('myshop')
      if user_signed_in?
        unless current_user.seller_user?
          redirect_to require_seller_role_path
        end
      else
        redirect_to require_sign_in_path
      end
    end

  end


end
