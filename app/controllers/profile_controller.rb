class ProfileController < ApplicationController
	include Wicked::Wizard
	steps :basic, :option, :finish

	before_filter :authenticate_user!
	

	def show

		# @profile = current_user.seller_profile if current_user.user_type == 'seller'	
		# @profile = current_user.normal_profile if current_user.user_type == 'normal'

		@profile = eval "current_user.#{current_user.user_type}_profile"
	
		render_wizard
	end

	def update

		@profile = eval "current_user.#{current_user.user_type}_profile"
		@profile.attributes = eval "#{current_user.user_type}_profile_params"
		
		# if current_user.user_type == 'seller'	
		# 	@profile = current_user.seller_profile
		# 	@profile.attributes = seller_profile_params

		# elsif current_user.user_type == 'normal'	
		# 	@profile = current_user.normal_profile
		# 	@profile.attributes = normal_profile_params
		# end

		render_wizard @profile
	end

private
	def seller_profile_params
		if step == :basic
			params[:seller_profile][:status] = 'basic'
			params.require(:seller_profile).permit(:gender, :age, :site, :country, :business_registration_number, 
				:business_representative, :company_name, { :product_category_ids => [] }, :status)	
		else
			option_profile_params
		end	
	end

	def normal_profile_params
		if step == :basic
			params[:normal_profile][:status] = 'basic'
			params.require(:normal_profile).permit(:gender, :age, :site, :country, 
				{ :talent_category_ids => [], :product_category_ids => [] }, :status)	
		else
			option_profile_params
		end	
	end

	def option_profile_params
		params["#{current_user.user_type}_profile"][:status] = 'option'
		params.require("#{current_user.user_type}_profile").permit(:avatar, :facebook, :twitter, :description, :status)	
	end

end
