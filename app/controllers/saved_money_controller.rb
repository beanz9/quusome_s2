class SavedMoneyController < ApplicationController

	layout 'rootshop'
	def index
		@saved_money = SavedMoney.where(:user_id => @current_user.id).order('created_at DESC').page(params[:page]).per(10)
		@save_money_sum = SavedMoney.where(:user_id => @current_user.id).sum(:money)
	end

end