class UploadsController < ApplicationController

	respond_to :json

	def create
		respond_with Upload.create(:file => params[:product_image_file])
	end

	def destroy
		@upload = Upload.find(params[:id])
		#@upload.destroy

	end

	def avatar_update
		profile = eval "current_user.#{current_user.user_type}_profile"

		# profile = current_user.profile
		# respond_with profile.update_attributes(:avatar => params[:avatar_image_upload])
		profile.avatar = params[:avatar_image_upload]
		profile.save

		render :json =>  {:small_image_url => profile.small_image_url}
		# respond_with profile.save


	end

private
	def upload_params
		params.require(:upload).permit(:product_image_file)
	end

end
