class OtherproductsController < ApplicationController

	layout 'rootshop'

	def index
		# @user = User.find_by_user_id(params[:user_id])

		# unless @user
		# 	raise NotFoundUserException
		# end
		
		brand = URI.decode(params[:brand])

		@products = Product.only_public.where(:brand => brand).page(params[:page]).per(20)
		@products_all_count = Product.only_public.where(:brand => brand).count
		@review_count = Review.joins(:product).where(products: { brand: brand, is_public: true }).count
		@collection_items_count = CollectionItem.joins(:product).where(products: { brand: brand, is_public: true }).count
		
		@brand = brand
		@recomand_brands = Product.connection.execute("select products.* from ( select distinct brand from products where deleted_at is null and is_public = 't') products order by RANDOM() limit 5")
		# @collection_items = CollectionItem.where(:product_id => @products.pluck(:id))
		# @collection_users = User.where(:id => @collection_items.pluck(:user_id))

	rescue NotFoundUserException
		render 'not_found'
	end
end

class NotFoundUserException < StandardError; end