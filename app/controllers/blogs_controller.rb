class BlogsController < ApplicationController 

	layout 'rootshop'

	def index
		if params[:category_id]
			@blogs = Blog.where(:is_public => true, :blog_category_id => params[:category_id]).order('created_at DESC').page(params[:page]).per(5)
		else
			@blogs = Blog.where(:is_public => true).order('created_at DESC').page(params[:page]).per(5)
		end
		
	end

	def show
		@blog = Blog.find(params[:id])
	end

end