class RequestUserDestroyController < ApplicationController

	before_filter :authenticate_user!, :only => [:create, :destroy]

	def create
		@request_user_destroy = RequestUserDestroy.new(:user_id => current_user.id)

		if @request_user_destroy.save
			redirect_to '/users/edit'
		end
	end

	def destroy
		@request_user_destroy = RequestUserDestroy.where(:user_id => current_user.id).first
		@request_user_destroy.destroy
		redirect_to '/users/edit'
	end
end