class ItemController < ApplicationController
	layout 'rootshop'
	impressionist :actions => [:show]
	
	def show
		@collection_item = CollectionItem.find(params[:item_id])

		# @product = @collection_item.product
		@product = Product.find(@collection_item.product_id)

		impressionist(@product)
		@other_products = Product.where(:created_by_id => @product.created_by_id).order('created_at DESC').limit(5)
		@other_products_all = Product.where(:created_by_id => @product.created_by_id).order('created_at DESC')

		# @comments = @product.comments.order("created_at DESC").page(1).per(5)
		# @comments_count = @product.comments.count
		# @reviews_count = @product.reviews.count
		@inquiry_comments = Comment.where(:commentable_type => 'Product', :commentable_id => @product.id, :parent_id => nil).order("created_at DESC").page(1).per(5)
		
	
		@collection_items = CollectionItem.joins(:product).where(:user_id => current_user.id, :product_id => @product.id) if user_signed_in?
		@other_collection_items = CollectionItem.joins(:product).where("collection_id = ? and collection_items.id not in (?)", @collection_item.collection_id, @collection_item.id).limit(5)
		@other_collection_items_all = CollectionItem.joins(:product).where(:collection_id => @collection_item.collection_id)
		@collections_include_this_product = Collection.where('id in (?)', CollectionItem.where(:product_id => @product.id).map {|m| m.collection_id}).limit(4)
		@collections = current_user.collections.order('created_at DESC').limit(8) if user_signed_in?


	rescue ActiveRecord::RecordNotFound
		render 'not_found'
	end
end