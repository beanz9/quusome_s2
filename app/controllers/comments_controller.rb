class CommentsController < ApplicationController

	before_filter :authenticate_user!, :except => [:index]

	def create

		@product = Product.find(params[:rootshop_id])
		comment = @product.comments.create(:comment => params[:comment], :user_id => current_user.id)
		# comment.create_activity key: 'comment.product_comment_create', owner: current_user, recipient: @product
		@comments = @product.comments.order("created_at DESC").page(1).per(5)
		@comments_count = @product.comments.count
	end

	def index
		@product = Product.find(params[:rootshop_id])
		@comments = @product.comments.order("created_at DESC").page(params[:page]).per(5)
		@comments_count = @product.comments.count
	end

	def destroy
		@comment = Comment.find(params[:id])
		@comment.destroy
		@product = Product.find(params[:rootshop_id])
		@comments = @product.comments.order("created_at DESC").page(params[:page]).per(5)
		@comments_count = @product.comments.count
	end


	def collectionshop_create
		@collection = Collection.find(params[:collection_id])
		# comment = @collection.comments.create(:comment => params[:comment], :user_id => current_user.id)

		@comment = Comment.build_from( @collection, @current_user.id, params[:comment] )
		@comment.save

		# comment.create_activity key: 'comment.collectionshop_comment_create', owner: current_user, recipient: @collection
		# @comments = @collection.comments.order("created_at DESC").page(1).per(5)
		@comments = Comment.where(:commentable_type => 'Collection', :commentable_id => @collection.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
		@comments_count = @comments.count
	end

	def collectionshop_index
		@collection = Collection.find(params[:collection_id])
		@comments = Comment.where(:commentable_type => 'Collection', :commentable_id => @collection.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
		# @comments = @collection.comments.order("created_at DESC").page(params[:page]).per(5)
		@comments_count = @comments.count
	end

	def collectionshop_destroy
		@comment = Comment.find(params[:comment_id])
		@comment.destroy
		@collection = Collection.find(params[:collection_id])
		@comments = Comment.where(:commentable_type => 'Collection', :commentable_id => @collection.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
		# @comments = @collection.comments.order("created_at DESC").page(params[:page]).per(5)
		@comments_count = @comments.count
	end

	def inquiry_comment_create
		@product = Product.find(params[:id])
		@user_who_commented = @current_user
		@comment = Comment.build_from( @product, @user_who_commented.id, params[:comment] )
		@comment.save

		@inquiry_comments = Comment.where(:commentable_type => 'Product', :commentable_id => @product.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)

		# @comments_count = @collection.comment_threads.count
	end

	def inquiry_comment_destroy
		@comment = Comment.find(params[:comment_id])
		@comment.destroy
		@product = Product.find(params[:id])
		@inquiry_comments = Comment.where(:commentable_type => 'Product', :commentable_id => @product.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
		# @inquiry_comments = @product.comment_threads.order("created_at DESC").page(1).per(5)
	end
	
	def inquiry_comment_index
		@product = Product.find(params[:id])
		@inquiry_comments = Comment.where(:commentable_type => 'Product', :commentable_id => @product.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
	end



end