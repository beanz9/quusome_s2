class FeaturedController < ApplicationController

	layout 'rootshop'

	def index
		@blogs = Blog.where(:is_public => true, :is_featured => true).order('created_at DESC').limit(4)
		@product_comments = Comment.where(:commentable_type => 'Product', :parent_id => nil).order('created_at DESC').limit(5)
		@boards = Board.where(:board_type => 'review').order('created_at DESC').limit(7)
		
		# expire_fragment('category_menu_cache')
	end

	def hot_collections
		@featured_collections = FeaturedCollection.joins(:collection).page(params[:page]).per(20)
	end
end