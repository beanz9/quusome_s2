class LineItemsController < ApplicationController

	before_filter :authenticate_user!
	layout 'rootshop'

	def index
		@line_items = LineItem.joins("INNER JOIN products ON products.id = line_items.product_id and products.deleted_at is null LEFT OUTER JOIN product_options ON product_options.id = line_items.product_option_id and products.deleted_at is null")
		.where("(line_items.created_by_id = ?)", current_user.id)
		.order('line_items.created_at DESC')

		# @line_items = LineItem.joins("INNER JOIN products ON products.id = line_items.product_id LEFT OUTER JOIN product_options ON product_options.id = line_items.product_option_id")
		# .where("(line_items.created_by_id = ?) and (coalesce(products.amount, 0) > 0 or coalesce(product_options.amount, 0) > 0)", current_user.id)
		# .order('line_items.created_at DESC')

		# @line_items_with_deleted_product = LineItem.joins("INNER JOIN products ON products.id = line_items.product_id LEFT OUTER JOIN product_options ON product_options.id = line_items.product_option_id")
		# .where("line_items.created_by_id = ? and (coalesce(products.amount, 0) = 0 and coalesce(product_options.amount, 0) = 0)", current_user.id)
		# .order('line_items.created_at DESC')

	
	end

	def check
		@line_items = LineItem.where(:created_by_id => current_user.id)
		option_temp_arr = []
		@disavailable = []
		
		@line_items.each do |l|
			l.options.each do |o|
				option_temp_arr << o["option_item_title"]
			end

			
			if l.product.inventory["color*_*#{option_temp_arr[0]}-_-size*_*#{option_temp_arr[1]}"] == 'disavailable'
				p l.product.title
				@disavailable << l.product.title
			end

			option_temp_arr = []
		end
	end

	def create
		if params[:order_type] == 'direct'
			redirect_url = "/orders/new"
			redirect_url += "?order_type=#{params[:order_type]}"
			redirect_url += "&product_id=#{params[:line_item][:product_id]}" 
			if params[:line_item][:product_options_id]
				params[:line_item][:product_options_id].each do |option|
					redirect_url += "&product_options_id[]=#{option}" 
				end
			end
			
			redirect_url += "&quantity=#{params[:line_item][:quantity]}" 
			redirect_url += "&collection_item_id=#{params[:line_item][:collection_item_id]}"

			p redirect_url

			redirect_to redirect_url

		elsif !params[:order_type].present?
			current_line_item = LineItem.where(:product_id => params[:line_item][:product_id], :product_option_id => params[:line_item][:product_option_id]).first
	
			if current_line_item.nil?
				line_item = LineItem.new(line_item_params)
				select_option = params[:line_item][:product_options_id]
				option_str = ""

				if select_option
					select_option.each do |o|
						option_id, option_item_id = o.split('_')
						option = ProductOption.where(:id => option_id.to_i).first
						option_item = ProductOptionItem.where(:id => option_item_id).first
						option_item_title = option_item.title.gsub("'", %q(\\\'))

						# option_str = option_str + "'#{option_id}' => '#{option_item_id}', "
						# option_str = option_str + "'#{option_id}' => {'option_title' => '#{option.title}', 'option_item_id' => '#{option_item.id}', 'option_item_title' => '#{option_item.title}'}, "
						option_str = option_str + "{'option_id' => '#{option.id}', 'option_title' => '#{option.title}', 'option_item_id' => '#{option_item.id}', 'option_item_title' => '#{option_item_title}'},"
					end
			

					line_item.options = eval("[#{option_str}]")
				end
				
				line_item.save
				redirect_to action: 'index'
			else
				current_line_item.update_attributes(:quantity => current_line_item.quantity + params[:line_item][:quantity].to_i)
				redirect_to action: 'index'
			end

		end
	end

	def destroy
		line_item = LineItem.find(params[:line_item_id])
		line_item.destroy
		redirect_to action: 'index'
	end

	def destroy_all

		if params[:cart_ids].present?
			@line_items = LineItem.where(:created_by_id => current_user.id, :id => params[:cart_ids])
		else
			if params[:deleted] == 'true'
				@line_items = (LineItem.joins("INNER JOIN products ON products.id = line_items.product_id LEFT OUTER JOIN product_options ON product_options.id = line_items.product_option_id")
				.where("line_items.created_by_id = ? and (coalesce(products.amount, 0) = 0 and coalesce(product_options.amount, 0) = 0)", current_user.id)).readonly(false)
			else
				@line_items = LineItem.joins("INNER JOIN products ON products.id = line_items.product_id LEFT OUTER JOIN product_options ON product_options.id = line_items.product_option_id")
				.where("(line_items.created_by_id = ?) and (coalesce(products.amount, 0) > 0 or coalesce(product_options.amount, 0) > 0)", current_user.id).readonly(false)
			end
		end
		@line_items.destroy_all
		redirect_to action: 'index'

	end

	def update_quantity
		line_item = LineItem.find(params[:line_item_id])
		line_item.update_attributes(:quantity => params[:quantity])
		redirect_to action: 'index'
	end

	def update_option
		line_item = LineItem.find(params[:line_item_id])
		select_option = params[:line_item][:product_options_id]

		option_str = ""

		select_option.each do |o|
			option_id, option_item_id = o.split('_')
			option = ProductOption.where(:id => option_id.to_i).first
			option_item = ProductOptionItem.where(:id => option_item_id.to_i).first
			option_item_title = option_item.title.gsub("'", %q(\\\'))
			
			line_item.options.each do |lo|
				if lo["option_id"] == option_id
					lo["option_item_id"] = option_item.id.to_s
					lo["option_item_title"] = option_item_title
				end
			end
		end

		line_item.options_will_change!
		line_item.save

		# line_item.update_attributes(:product_option_id => params[:product_option_id])
		redirect_to action: 'index'
	end

	def line_item_params
		params.require(:line_item).permit(:product_id, :product_option_id, :quantity, :collection_item_id)
	end

end