class BoardController < ApplicationController

	layout 'rootshop'
	# before_filter :authenticate_user!, :only => [:new, :create]

	def index 

		# if params[:type] == 'review'
		# 	@boards = Board.where(:board_type => 'review').order('created_at DESC')
		# 	render 'review'
		# end
		if params[:type] == 'product_faq'
			@boards = Comment.where(:commentable_type => 'Product', :parent_id => nil).order('created_at DESC').page(params[:page]).per(10)
			render 'product_faq'
		else
			@boards = Board.where(:board_type => params[:type]).order('created_at DESC').page(params[:page]).per(10)
		end
	end

	def new
		@board = Board.new
		

	end

	def show
		if params[:type] == 'product_faq'
			@board = Comment.find(params[:id])
			@product = Product.find(@board.commentable_id)
			@comments = Comment.where(:parent_id => @board.id).order('created_at DESC') 
			render 'product_faq_show'
		else
			@board = Board.find(params[:id])
			@comments = Comment.where(:commentable_id => @board.id).order('created_at DESC') 
		end
	end

	def edit
		@board = Board.find(params[:id])
	end

	def create
		@board = Board.new(board_params)
		@board.user_id = current_user.id
		@board.board_type = params[:type]

		if @board.save
			redirect_to :action => 'index'  
		else
			redirect_to :action => 'new'  
		end
	end

	def update
		@board = Board.find(params[:id])
		if @board.update(board_params)
			redirect_to :action => 'index'  
		else
			redirect_to :action => 'edit'  
		end
	end

	def destroy
		@board = Board.find(params[:id])
		@board.destroy
		redirect_to :action => 'index'
	end

	def comment_create
		@board = Board.find(params[:id])
		@comment = Comment.build_from(@board, current_user.id, params[:comment])		
		@comment.save
		redirect_to :action => 'show'  
	end

	def comment_destroy
		@comment = Comment.find(params[:comment_id])
		@comment.destroy
		redirect_to :action => 'show'  
	end

private
	def board_params
		params.require(:board).permit(:title, :body)
	end

end