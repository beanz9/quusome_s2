class InquiryCommentController < ApplicationController

	layout 'rootshop'

	def index
		@inquiry_comments = Comment.where(:commentable_type => 'Product', :user_id => @current_user.id, :parent_id => nil).order("created_at DESC").page(params[:page]).per(5)
	end

	def destroy
		@inquiry_comment = Comment.find(params[:id])
		@inquiry_comment.destroy

		redirect_to action: 'index', :page => params[:page]
	end

end