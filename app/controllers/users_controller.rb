class UsersController < Devise::RegistrationsController

  layout 'rootshop'

  # after_create :event_sign_up

  def event_sign_up
    if Date.parse('2014-08-30') >= Date.today
      p "!!!"
    end
  end



  def new
    build_resource({})

    if request.original_fullpath == '/sign_up/seller'
    	self.resource.user_type = 'seller'
    elsif request.original_fullpath == '/sign_up/normal'
    	self.resource.user_type = 'normal'
    end
	  
	  respond_with self.resource
  end

  def create
    build_resource(sign_up_params)
    ActiveRecord::Base.transaction do
      if resource.save
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_navigational_format?
          sign_up(resource_name, resource)
          respond_with resource, :location => after_sign_up_path_for(resource)
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
          expire_session_data_after_sign_in!
          respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        end

        resource.build_seller_profile.save
        resource.build_normal_profile.save
       

      else
        clean_up_passwords resource
        respond_with resource
      end
    end
  end

  def edit
    @profile = eval "current_user.#{current_user.user_type}_profile"
    @request_user_destroy = RequestUserDestroy.where(:user_id => current_user.id).first if current_user.seller_user?
    @order_state = OrdersItem.where(
      "created_by_id = ? 
      and (
        state = 'order_completed' 
        or state = 'order_confirmed' 
        or state = 'ship_prepared'
        or state = 'shipping'
        or state = 'take_back_requested'
        or state = 'take_back_received'
        )", current_user.id)

    @profits_sum = Profit.where(:collection_owner_id => current_user.id).select('coalesce(sum(cast(collection_profit as float)), 0) as collection_profit')[0]
  end

protected

  def after_sign_up_path_for(resource)

    # if Date.parse('2014-08-30') >= Date.today
    s = SavedMoney.new
    s.user_id = resource.id
    s.money = 5000
    s.save


    # end

    root_path
  end

  def after_update_path_for(resource)
    '/users/edit'
  end

  # def after_sign_in_path_for(resource)
  #   '/profile'
  # end





end
