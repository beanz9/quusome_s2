
	class MypageController < ApplicationController

		layout 'rootshop'
		before_filter :authenticate_user!, :only => [:index]

		
		def user_info
		end
		
		def index

			@activities = PublicActivity::Activity.where("
				(recipient_type='Collection' and recipient_id in (:collection_ids)) or
				(recipient_type='Product' and recipient_id in (:product_ids)) or
				(trackable_type='Follow' and trackable_id in (:follows_ids)) or
				(recipient_type='Collection' and recipient_id in (:followed_collection_ids)) or
				(owner_type='User' and owner_id=:user_id)
			", 
			:collection_ids => current_user.collection_ids, 
			:product_ids => current_user.product_ids, 
			:follows_ids => current_user.follows_by_type('Collection').map(&:id),
			:followed_collection_ids => current_user.follows_by_type('Collection').map(&:followable_id),
			:user_id => current_user.id).
			order("created_at DESC").
			page(params[:page]).per(5)
		end
	end
