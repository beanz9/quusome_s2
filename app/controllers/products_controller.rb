class ProductsController < ApplicationController
	#include Wicked::Wizard
	#steps :basic, :image, :movie, :story

	before_filter :authenticate_user!
	# respond_to :json

	def index


		# respond_with Product.where("title is not null")
		respond_to do |format|
			format.html
			format.json
		end
	end

	def show
		respond_with Product.find(params[:id])
	end

	def new
		@product = Product.new
		@product.product_images.build
		@product.product_images.build
		@product.product_images.build

	end

	def create
    respond_with Product.create(product_params)
  end

  def update
    respond_with Product.update(params[:id], product_params)
  end

  def destroy
    respond_with Product.destroy(params[:id])
  end






	# def show

	# 	p "!!!!!!!!!!!!!!"
	# 	p params
	# 	if params[:product_id].present?
	# 		@product = Product.find(params[:product_id])
	# 	else
	# 		@product = Product.new
	# 	end

		

	
	# 	render_wizard
	# end

	# def create
	# 	@product = Product.new(product_params)

	# 	render_wizard @product, :product_id => @product.id

	# end

	# def update

	# 	@product = Product.new(product_params)


	# 	p @product
	# 	render_wizard @product

	# end



private
	def product_params
		params.require(:product).permit(:title, { :product_category_ids => [] }, :keyword_list)
	end



end
