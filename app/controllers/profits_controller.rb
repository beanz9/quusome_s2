class ProfitsController < ApplicationController

	layout 'rootshop'

	def index
		@profits = Profit.includes({ product: :product_images}, :collection_item).
		where(:collection_owner_id => current_user.id).order('created_at DESC').
		page(params[:page]).per(10)
		@profits_sum = Profit.where(:collection_owner_id => current_user.id).select('coalesce(sum(cast(collection_profit as float)), 0) as collection_profit')[0]
		@profit_withdraw_request = ProfitWithdrawRequests.new

	end
	

	def withdraw

		profits_sum = Profit.where(:collection_owner_id => current_user.id).select('coalesce(sum(cast(collection_profit as float)), 0) as collection_profit')[0]

		if params[:profits_withdraw].to_i <= profits_sum.collection_profit
			@profit = Profit.new

			@profit.collection_owner_id = current_user.id
			@profit.collection_profit = "-" + params[:profits_withdraw]
			@profit.is_cancel = false

			if @profit.save
				redirect_to :action => 'index'
			end

		else
			redirect_to :action => 'index'
		end

	end

	def withdrawal_request
		@profit_withdraw_request = ProfitWithdrawRequests.new(withdraw_request_params)
		@profit_withdraw_request.user_id = current_user.id
		@profit_withdraw_request.is_done = false

		if @profit_withdraw_request.save
			@profit = Profit.new
			@profit.collection_owner_id = current_user.id
			@profit.collection_profit = -@profit_withdraw_request.withdraw_amount.to_i
			@profit.profit_withdraw_request_id = @profit_withdraw_request.id

			@profit.save
		end

		redirect_to :action => 'index'
	end

private 
	def withdraw_request_params

		params.require(:profit_withdraw_requests).permit(:bank_name, :account_owner, :account_number, :withdraw_amount, :tel_no_1, :tel_no_2, :tel_no_3)
	end

end