#encoding: utf-8

class OrdersController < ApplicationController

	require 'order_state'
	require 'payment_config'
	require 'open3'

	before_filter :saved_money_check, :only => [:create]
	before_filter :authenticate_user!

	layout 'rootshop'
	def index
		@orders = Order.where(:created_by_id => current_user.id)
		@orders_items = OrdersItem.
			# joins(:product).
			group("product_id, product_title, product_option_id, product_option_title, order_id, state, product_featured_small_image_url, options ").
			select("max(orders_items.id) as id, product_id, product_title, product_option_id, product_option_title, 
				 order_id, sum(orders_items.price) as price, product_featured_small_image_url, 
				 state, count(1) as quantity, max(orders_items.created_at) as created_at, count(order_id) as order_id_count, options").
			where(
				:created_by_id => current_user.id
				
			).
			search_product_title(params[:title]).
			search_state(params[:state]).
			search_date(params[:start_date], params[:end_date]).
			order("max(orders_items.created_at) DESC").
			group("order_id").
			page(params[:page]).per(10)


		# @orders_items = OrdersItem.find_by_sql(
		# 	"SELECT max(id) as id, product_id, product_option_id, order_id, sum(price) as price,
		# 		 state, count(1) as quantity, max(created_at) as created_at, count(order_id) as order_id_count 
		# 		 FROM orders_items WHERE orders_items.created_by_id = 2 AND (orders_items.deleted_at IS NULL) 
		# 		 GROUP BY product_id, product_option_id, order_id, state ORDER BY max(created_at) DESC"
		# )


		# @orders_items = Kaminari.paginate_array(@orders_items).page(params[:page]).per(5)
	end

	def new


		if params[:order_type] == 'direct'
			product = Product.find(params[:product_id])

			select_option = params[:product_options_id]
			option_str = ""
			if select_option
				select_option.each do |o|
					option_id, option_item_id = o.split('_')
					option = ProductOption.where(:id => option_id.to_i).first
					option_item = ProductOptionItem.where(:id => option_item_id).first
					option_item_title = option_item.title.gsub("'", %q(\\\'))

					option_str = option_str + "{'option_id' => '#{option.id}', 'option_title' => '#{option.title}', 'option_item_id' => '#{option_item.id}', 'option_item_title' => '#{option_item_title}'},"
				end
			end

			@line_items = []

			@line_items << LineItem.new(
				:product_id => product.id,
				:quantity => params[:quantity],
				:options => eval("[#{option_str}]")
			)
		elsif params[:order_type] == 'select'
			@line_items = LineItem.where(:created_by_id => current_user.id, :id => params[:cart_ids]).order("created_at DESC")
		else
			# @line_items = LineItem.joins(:product, :product_option)
			# .where("(line_items.created_by_id = ? and products.deleted_at is null) and (products.amount > 0 or product_options.amount > 0)", current_user.id)
			@line_items = LineItem.joins(:product)
			.where("(line_items.created_by_id = ? and products.deleted_at is null)", current_user.id).order("created_at DESC")
		end

		@order = Order.new
		@exist_address = Order.where(:created_by_id => current_user.id).last
		@current_saved_money = SavedMoney.where(:user_id => current_user.id).sum(:money)

		@payment_config = PaymentConfig.new
		@pay_uuid = SecureRandom.hex

		good_info_arr = []

		@line_items.each_with_index do |line_item, i|
			good_info_arr << "seq=#{i+1}#{31.chr}ordr_numb=#{@pay_uuid}-#{i+1}#{31.chr}good_name=#{line_item.product.title.slice(0, 30)}#{31.chr}good_cntx=#{line_item.quantity}#{31.chr}good_amtx=#{line_item.product.get_price_final(line_item.get_selected_option_item_title)}"
		end

		@good_info = good_info_arr.join(30.chr)
		@good_count = @line_items.count

		@good_name = @line_items.map { |l| l.product.title }.join(',')

	end

	def create
		order = Order.new(order_params)
		
		order.virtual_order_type = params[:order_type]
		order.virtual_product_id = params[:product_id]
		order.virtual_product_option_id = params[:product_options_id]
		order.virtual_quantity = params[:quantity]
		order.virtual_cart_ids = params[:cart_ids]
		order.virtual_collection_item_id = params[:collection_item_id]
		order.virtual_saved_money = params[:hidden_saved_money_for_order_item]
		order.saved_money = get_saved_money_from_order 
		order.checkout_price = params[:order][:original_price].to_f - get_saved_money_from_order
		order.current_user_id = current_user.id

		# if order.checkout_price.to_i == params[:good_mny].to_i 

			if order.save
				if Rails.env.production?

					result = pay_exec(params)

					if result["res_cd"] == "0000"
						save_payment_result_data(result, order, true)
						@payment_msg = "결제에 성공했습니다."
						order.ordr_idxx = result["ordr_idxx"]
						order.save
					else
						save_payment_result_data(result, order, false)
						order.destroy
						@payment_msg = "결제에 실패했습니다."
					end
				else
					@payment_msg = "결제에 성공했습니다."
					@payment_result_data = PaymentResultData.new
				end

				# redirect_to action: 'index'
			else
				@payment_msg = "결제중 오류가 발생했습니다."
				# order.destoy
				# redirect_to action: 'failure_order'
			end
		# else
		# 	@payment_msg = "주문가격과 상품가격이 다릅니다."
		# end
	end

	def save_payment_result_data(result, order, is_success)
		@payment_result_data = PaymentResultData.new
		@payment_result_data.site_cd = result["site_cd"]
		@payment_result_data.req_tx = result["req_tx"]
		@payment_result_data.use_pay_method = result["use_pay_method"]
		@payment_result_data.bSucc = result["bSucc"]
		@payment_result_data.panc_mod_mny = result["panc_mod_mny"]
		@payment_result_data.panc_rem_mny = result["panc_rem_mny"]
		@payment_result_data.mod_type = result["mod_type"]
		@payment_result_data.amount = result["amount"]
		@payment_result_data.res_cd = result["res_cd"]
		@payment_result_data.res_msg = result["res_msg"]
		@payment_result_data.ordr_idxx = result["ordr_idxx"]
		@payment_result_data.tno = result["tno"]
		@payment_result_data.good_mny = result["good_mny"]
		@payment_result_data.good_name = result["good_name"]
		@payment_result_data.buyr_name = result["buyr_name"]
		@payment_result_data.buyr_tel1 = result["buyr_tel1"]
		@payment_result_data.buyr_tel2 = result["buyr_tel2"]
		@payment_result_data.buyr_mail = result["buyr_mail"]
		@payment_result_data.app_time = result["app_time"]
		@payment_result_data.card_cd = result["card_cd"]
		@payment_result_data.card_name = result["card_name"]
		@payment_result_data.noinf = result["noinf"]
		@payment_result_data.app_no = result["app_no"]
		@payment_result_data.quota = result["quota"]
		@payment_result_data.partcanc_yn = result["partcanc_yn"]
		@payment_result_data.card_bin_type_01 = result["card_bin_type_01"]
		@payment_result_data.card_bin_type_02 = result["card_bin_type_02"]
		@payment_result_data.bank_name = result["bank_name"]
		@payment_result_data.bank_code = result["bank_code"]
		@payment_result_data.bankname = result["bankname"]
		@payment_result_data.depositor = result["depositor"]
		@payment_result_data.account = result["account"]
		@payment_result_data.va_date = result["va_date"]
		@payment_result_data.pnt_issue = result["pnt_issue"]
		@payment_result_data.pnt_app_time = result["pnt_app_time"]
		@payment_result_data.pnt_app_no = result["pnt_app_no"]
		@payment_result_data.pnt_amount = result["pnt_amount"]
		@payment_result_data.add_pnt = result["add_pnt"]
		@payment_result_data.use_pnt = result["use_pnt"]
		@payment_result_data.rsv_pnt = result["rsv_pnt"]
		@payment_result_data.commid = result["commid"]
		@payment_result_data.mobile_no = result["mobile_no"]
		@payment_result_data.tk_van_code = result["tk_van_code"]
		@payment_result_data.tk_app_no = result["tk_app_no"]
		@payment_result_data.cash_yn = result["cash_yn"]
		@payment_result_data.cash_authno = result["cash_authno"]
		@payment_result_data.cash_tr_code = result["cash_tr_code"]
		@payment_result_data.cash_id_info = result["cash_id_info"]
		@payment_result_data.escw_yn = result["escw_yn"]
		@payment_result_data.deli_term = result["deli_term"]
		@payment_result_data.bask_cntx = result["bask_cntx"]
		@payment_result_data.good_info = result["good_info"]
		@payment_result_data.rcvr_name = result["rcvr_name"]
		@payment_result_data.rcvr_tel1 = result["rcvr_tel1"]
		@payment_result_data.rcvr_tel2 = result["rcvr_tel2"]
		@payment_result_data.rcvr_mail = result["rcvr_mail"]
		@payment_result_data.rcvr_zipx = result["rcvr_zipx"]
		@payment_result_data.rcvr_add1 = result["rcvr_add1"]
		@payment_result_data.rcvr_add2 = result["rcvr_add2"]

		@payment_result_data.order_id = order.id
		@payment_result_data.is_success = is_success

		@payment_result_data.save

	end

	def pay_exec(params)

		pay_info_json = {
			:pay_method => params[:pay_method],
			:ordr_idxx => params[:ordr_idxx],
			:good_name => params[:good_name],
			:good_mny => params[:good_mny],
			:buyr_name => params[:buyr_name],
			:buyr_mail => params[:buyr_mail],
			:buyr_tel1 => params[:buyr_tel1],
			:buyr_tel2 => params[:buyr_tel2],
			:rcvr_name => params[:rcvr_name],
			:rcvr_tel1 => params[:rcvr_tel1],
			:rcvr_tel2 => params[:rcvr_tel2],
			:rcvr_mail => params[:rcvr_mail],
			:rcvr_zipx => params[:rcvr_zipx],
			:rcvr_add1 => params[:rcvr_add1],
			:rcvr_add2 => params[:rcvr_add2],
			:req_tx => params[:req_tx],
			:site_cd => params[:site_cd],
			:site_name => params[:site_name],
			:quotaopt => params[:quotaopt],
			:currency => params[:currency],
			:module_type => params[:module_type],
			:escw_used => params[:escw_used],
			:pay_mod => params[:pay_mod],
			:deli_term => params[:deli_term],
			:bask_cntx => params[:bask_cntx],
			:good_info => params[:good_info],
			:res_cd => params[:res_cd],
			:res_msg => params[:res_msg],
			:tno => params[:tno],
			:trace_no => params[:trace_no],
			:enc_info => params[:enc_info],
			:enc_data => params[:enc_data],
			:ret_pay_method => params[:ret_pay_method],
			:tran_cd => params[:tran_cd],
			:bank_name => params[:bank_name],
			:bank_issu => params[:bank_issu],
			:use_pay_method => params[:use_pay_method],
			:cash_tsdtime => params[:cash_tsdtime],
			:cash_yn => params[:cash_yn],
			:cash_authno => params[:cash_authno],
			:cash_tr_code => params[:cash_tr_code],
			:cash_id_info => params[:cash_id_info],
			:good_expr => params[:good_expr],
			:shop_user_id => params[:shop_user_id],
			:pt_memcorp_cd => params[:pt_memcorp_cd]
		}

		json = JSON.generate(pay_info_json)
		stdout_str, stderr_str, status = Open3.capture3("java -cp /home/quusome/quusome_s2/java KcpWrapper '#{json}'")

		return JSON.parse(stdout_str)
	end

	# def cancel
	# 	p params
	# 	order_item = OrdersItem.find(params[:id])
	# 	order_item.state = "order_canceled"
	# 	order_item.save

	# 	redirect_to action: 'index'
	# end

	def cancel
		os = OrderState.new
		p os.cancel(params)
		redirect_to action: 'index'
	end

	def ship_complete
		os = OrderState.new
		os.ship_complete(params)
		redirect_to action: 'index'
	end

	# def take_back_request
	# 	os = OrderState.new
	# 	os.take_back_request(params)
	# 	redirect_to action: 'index'
	# end

	def take_back_request
		@orders_item = OrdersItem.find(params[:id])
		@take_back_request = TakeBackRequest.new
	end

	def create_take_back_request
		@take_back_request = TakeBackRequest.new(take_back_request_params)

	
		if @take_back_request.save
			os = OrderState.new
			os.take_back_receive(params)
			redirect_to action: 'index'
		else
			redirect_to action: 'take_back_request'
		end

		

	end


	def complete
	end

	def failure_order
	end

private

	def get_saved_money_from_order
		saved_money = 0
		params[:hidden_saved_money_for_order_item].each do |money|
			saved_money = saved_money +  money.split("_")[1].to_f 
		end
		saved_money.to_f
	end

	def take_back_request_params
		params.require(:take_back_request).permit(:reason_type, :reason, :image_1, :image_2, :image_3).merge(orders_item_id: params[:id])
	end
	
	def order_params
		params.require(:order).permit(:recipient, :recipient_en, :email, :custom_type, :private_custom_number, :zipcode, :address, :detail_address, :mobile_phone_1, :mobile_phone_2,
			:mobile_phone_3, :phone_1, :phone_2, :phone_3, :shipping_note, :payment_method, :rrn_1, :rrn_2, :saved_money, :checkout_price, :original_price)
	end

	def saved_money_check
		current_saved_money = SavedMoney.where(:user_id => current_user.id).sum(:money)

		saved_money_use = params[:hidden_saved_money_for_order_item]
		money = 0

		saved_money_use.each do |saved_money|
			money = money + saved_money.split("_")[1].to_i
		end

		if money > current_saved_money
			redirect_to :back, notice: 'saved_money'
		end
	end

	def check_product_amount

		line_items = []

		if !params[:order_type].present?
			line_items = LineItem.where(:created_by_id => current_user.id)
		elsif params[:order_type] == 'select'
			line_items = LineItem.where(:created_by_id => current_user.id, :id => params[:cart_ids])
		elsif params[:order_type] == 'direct'
			product = Product.find(params[:product_id])

			line_items << LineItem.new(
				:product_id => product.id,
				:quantity => params[:quantity],
				:price => product.price,
				:product_option_id => params[:product_option_id]
			)
		end

		flag = false
		items = []

		line_items.each do |item|
		
			if !item.product_option_id.present?
				p item.product.amount
				p item.quantity
				if item.product.amount < item.quantity
					items << { "product_id" => item.product_id }
					flag = true
				end
			else
				product_option = ProductOption.find(item.product_option_id)
				if product_option.amount < item.quantity
					items << { "product_id" => item.product_id, "product_option_id" => item.product_option_id }
					flag = true
				end
			end
		end

		if flag
			flash[:items] = items
			flash[:notice] = "check_amount"

			if !params[:order_type].present?
				redirect_to action: "new"
			elsif params[:order_type] == 'direct'
				redirect_to new_orders_path(
					:order_type => params[:order_type], 
					:product_id => params[:product_id], 
					:product_option_id => params[:product_option_id],
					:quantity => params[:quantity]
				)	
			elsif params[:order_type] == 'select'
				url = '/orders/new?order_type=select'
				params[:cart_ids].each do |id|
					url = url + "&cart_ids[]=#{id}"
				end
				redirect_to url
			end
		end
	end

end