# require 'better_ftp'

class TestController < ApplicationController

	# around_filter :transactional, :only => [:index]

	# CONTENT_SERVER_DOMAIN_NAME = "115.68.110.205"
	# CONTENT_SERVER_FTP_LOGIN = "quusome"
	# CONTENT_SERVER_FTP_PASSWORD = "van5150@gmail"

	def calculate
		order_items = OrdersItem.where(:state => 'international_deliverd', :is_calculate => false)
		order_items.each do |item|
			
		end
	end

	def product_delete
		products = Product.where("id > 6487")

		products.each do |p|
			s = ScrapyItem.where(:url => p.url).first
			s.is_done = 'false'
			s.save

			p.delete
		end
	end

	def not_available_product
		@image_check = ImageCheck.all

		@image_check.each do |ic|
			product = Product.where(:id => ic.product_id).first
			if product
				p product.delete

				ScrapyItem.where(:url => ic.url).each do |s|
					p s.destroy
				end
			end
		end
	end

	def not_available_image
		@products = Product.order("created_at DESC")
		p "......."



		@products.each_with_index do |product, i|

			is_delete = false

			product.product_images.each do |image|
				

				# p File.exist?((Rails.root.to_s + image.upload.file.url(:thumb)).split('?')[0])
				unless File.file?((Rails.root.to_s + "/public" + image.upload.file.url(:thumb)).split('?')[0])
					is_delete = true
					break
				end

				unless File.file?((Rails.root.to_s + "/public" + image.upload.file.url(:medium)).split('?')[0])
					is_delete = true
					break
				end

				unless File.file?((Rails.root.to_s + "/public" + image.upload.file.url(:rootshop_index)).split('?')[0])
					is_delete = true
					break
				end

				unless File.file?((Rails.root.to_s + "/public" + image.upload.file.url(:original)).split('?')[0])
					is_delete = true
					break
				end
			end

			
			if is_delete
				image_check = ImageCheck.new
				image_check.product_id = product.id
				image_check.url = product.url
				image_check.save

			end

		end
	end


	def index
		# @ftp = BetterFTP.new(CONTENT_SERVER_DOMAIN_NAME, CONTENT_SERVER_FTP_LOGIN, CONTENT_SERVER_FTP_PASSWORD)
	
		# @items = ScrapyItem.where(:is_done => 'false').skip(rand(ScrapyItem.count)).limit(1)
		# @items = ScrapyItem.where(:is_done => 'false', :category.ne => "").no_timeout.order("created_at ASC").limit(500)
		@items = ScrapyItem.where(:is_done => 'false').no_timeout.order("created_at ASC").limit(500)
		@item = nil

		# @items = ScrapyItem.skip(rand(ScrapyItem.count)).limit(1)


		@items.each_with_index do |item, i|
			# begin

			item = ScrapyItem.find(item.id)
			@item = item

			if item.is_done == "doing" or item.is_done == 'true'
				item.is_done = "true"
				item.save
				next
			end

			item.is_done = "doing"
			item.save

			if item.status == 'create'

					@product = Product.new

					@product.title = item.title
					@product.sub_title = item.sub_title
					@product.url = item.url
					@product.detail_info = item.desc
					@product.currency = item.currency
					@product.multi_price = item.price
					@product.is_public = true
					@product.category_str = item.category
					@product.created_by_id = item.created_by
					@product.updated_by_id = item.created_by
					@product.domain = item.domain
					@product.brand = item.brand
					@product.is_sale = item.is_sale

					min = []
					min << @product.multi_price_final["original"].to_f
					@product.multi_price_final["sale_color"].values.each do |v|
						min << v.to_f
					end

					@product.init_price = min.min




					inventory_str =  item.inventory.to_s.gsub '||', '.'
					@product.inventory = eval(inventory_str)

					@product.size_table_html = item.size_table_html
					@product.created_at = Time.now
					@product.updated_at = Time.now

					if Product.where(:url => @product.url).count > 0
						next
					end

					if @product.save

						item.options.keys.each do |option_key|
							product_option = ProductOption.new

							product_option.product_id = @product.id
							product_option.title = option_key
							product_option.amount = 100000
							product_option.created_at = Time.now
							product_option.updated_at = Time.now

							if product_option.save
								
								option_items = item.options["#{option_key}"]

								option_items.each do |option_item|
									product_option_item = ProductOptionItem.new
									product_option_item.product_option_id = product_option.id
									product_option_item.title = option_item
									product_option_item.amount = 100000
									product_option.created_at = Time.now
									product_option.updated_at = Time.now
									
									product_option_item.save

								end
							end
						end

						item.options['color'].each do |color_option|
							images = item.images[color_option]
							images.each_with_index do |image, i|
								image_create(color_option, @product,image, i)
								# sleep(rand)
							end
						end

					end

					item.is_done = "true"
					item.save

			
			else
				p "update"
				@product = Product.where(:url => item.url).first

				if @product

					@product.multi_price = item.price

					inventory_str =  item.inventory.to_s.gsub '||', '.'
					@product.inventory = eval(inventory_str)

					if @product.save
						item.is_done = "true"
						item.save
					end

				

				end

				
			end

			# rescue 
			# 	p "rescue!!!!!!!!"
			# 	@item.is_done = "false"
			# 	@item.save
			# 	@product.destroy if @item.status == 'create'
			# 	next
				
			# end
		
		end

		# @ftp.close

	

	end



	

private
	
	def image_process(image_key, product, file_temp_path, file_name, i)
		file_temp = Magick::Image.read(file_temp_path).first
		file_ext = file_temp.format.downcase
		# p file_temp.filesize

		upload = Upload.new
		upload.file_file_name = file_name + ".#{file_ext}"
		upload.file_content_type = "image/#{file_ext}"
		# upload.file_file_size = file
		upload.created_at = Time.now
		upload.updated_at = Time.now
		upload.file_updated_at = Time.now

		upload.save

		#product_image save
		product_image = ProductImage.new
		product_image.product_id = product.id
		product_image.upload_id = upload.id
		product_image.color_option = image_key
		product_image.created_at = Time.now
		product_image.updated_at = Time.now
		product_image.sort = i

		product_image.save

		#original image

		original_file_path = "#{File.dirname(upload.file.path)}/"
		original_file_path_remote_temp = "#{File.dirname(upload.file.path)}/".split('/public/')[1]
		original_file_path_remote = "/home/quusome/quusome_s2/public/#{original_file_path_remote_temp}/"

		FileUtils::mkdir_p original_file_path

		original_image = file_temp
		
		original_image_target = Magick::Image.new(600, 600) do
			self.background_color = 'white'
		end

		original_image.resize_to_fit!(600, 600)
		original_image_target.composite(
			original_image, 
			Magick::CenterGravity, 
			Magick::CopyCompositeOp).write(original_file_path + upload.file_file_name)

		# @ftp.mkdir_p(original_file_path_remote)
		# @ftp.putbinaryfile(File.new("#{original_file_path + upload.file_file_name}"), original_file_path_remote + upload.file_file_name)

		#medium image
		medium_file_path = "#{File.dirname(upload.file.path(:medium))}/"
		medium_file_path_remote_temp = "#{File.dirname(upload.file.path(:medium))}/".split('/public/')[1]
		medium_file_path_remote = "/home/quusome/quusome_s2/public/#{medium_file_path_remote_temp}"

		FileUtils::mkdir_p medium_file_path

		medium_image = Magick::Image.read(file_temp_path).first
		medium_image_target = Magick::Image.new(300, 300) do
			self.background_color = 'white'
		end
		medium_image.resize_to_fit!(300, 300)
		medium_image_target.composite(
			medium_image, 
			Magick::CenterGravity, 
			Magick::CopyCompositeOp).write(medium_file_path + upload.file_file_name)

		# @ftp.mkdir_p(medium_file_path_remote)
		# @ftp.putbinaryfile(File.new("#{medium_file_path + upload.file_file_name}"), medium_file_path_remote + upload.file_file_name)

		#thumb image
		thumb_file_path = "#{File.dirname(upload.file.path(:thumb))}/"
		thumb_file_path_remote_temp = "#{File.dirname(upload.file.path(:thumb))}/".split('/public/')[1]
		thumb_file_path_remote = "/home/quusome/quusome_s2/public/#{thumb_file_path_remote_temp}"
		FileUtils::mkdir_p thumb_file_path

		thumb_image = Magick::Image.read(file_temp_path).first
		thumb_image_target = Magick::Image.new(100, 100) do
			self.background_color = 'white'
		end
		thumb_image.resize_to_fit!(100, 100)
		thumb_image_target.composite(
			thumb_image, 
			Magick::CenterGravity, 
			Magick::CopyCompositeOp).write(thumb_file_path + upload.file_file_name)

		# @ftp.mkdir_p(thumb_file_path_remote)
		# @ftp.putbinaryfile(File.new("#{thumb_file_path + upload.file_file_name}"), thumb_file_path_remote + upload.file_file_name)

		#rootshop_index image
		rootshop_index_file_path = "#{File.dirname(upload.file.path(:rootshop_index))}/"
		rootshop_index_file_path_remote_temp = "#{File.dirname(upload.file.path(:rootshop_index))}/".split('/public/')[1]
		rootshop_index_file_path_remote = "/home/quusome/quusome_s2/public/#{rootshop_index_file_path_remote_temp}"
		FileUtils::mkdir_p rootshop_index_file_path

		rootshop_index_image = Magick::Image.read(file_temp_path).first
		# rootshop_index_image.resize_to_fit!(260)
		rootshop_index_image.change_geometry("260") {|cols, rows, img| img.resize!(cols, rows)}
		rootshop_index_image.write(rootshop_index_file_path + upload.file_file_name)

		# @ftp.mkdir_p(rootshop_index_file_path_remote)
		# @ftp.putbinaryfile(File.new("#{rootshop_index_file_path + upload.file_file_name}"), rootshop_index_file_path_remote + upload.file_file_name)

		# File.delete(file_temp_path)
	end

	def image_create(image_key, product, image, i)
		file_name = "#{Digest::MD5.hexdigest(image)}"
		file_temp_path = "public/system/crawl_images/#{file_name}"
		
		file = File.open(file_temp_path, "wb") do |f|
			response = HTTParty.get(image)
			p response.code
			if response.code == 200
				f.write response.parsed_response 
				image_process(image_key, product, file_temp_path, file_name, i)
			end
		end

		

		
	end


	def transactional
	  ActiveRecord::Base.transaction do
	    yield
	  end
	end
end



