class CollectionshopController < ApplicationController 


	# layout Proc.new {
	# 	|controller| controller.action_name == 'collection_manage' ? 'rootshop' : 'collection_shop'
	# }

	layout 'rootshop'

	impressionist :actions => [:show]
	before_filter :authenticate_user!, :only => [:follow]

	

	def index
		# @collections = Collection.find_by_sql(
		# 	"select * from collections where (
		# 		select count(1) 
		# 		from collection_items, products
		# 		where (collection_items.collection_id = collections.id and collection_items.deleted_at is null) 
		# 		and (products.id = collection_items.product_id and products.deleted_at is null)
		# 	) > 0"
		# )
		if params[:q].present?

			@collections = Collection.search(params[:q]).where("collection_items_count > 0").page(params[:page]).per(20)
			# @collections = Collection.search(params[:q])
		else
			@collections = Collection.where("collection_items_count > 0").order('updated_at DESC').page(params[:page]).per(20)


		# 	@collections = Collection.find_by_sql(
		# 	"select * from collections where (
		# 		select count(1) 
		# 		from collection_items, products
		# 		where (collection_items.collection_id = collections.id and collection_items.deleted_at is null) 
		# 		and (products.id = collection_items.product_id and products.deleted_at is null)
		# 	) > 0"
		# )
		end

	end

	def show
		user = User.find_by_user_id(params[:user_id])

		unless user
			raise NotFoundUserException
		end

		@collections = Collection.where(:user_id => user.id)
		# @collections =  Collection.select('collections.*, count(collection_items.id) as item_count').
		# 		joins('LEFT JOIN collection_items on collection_items.collection_id = collections.id').
		# 		where(:user_id => user.id).group('collections.id')

		if params[:collection_id].nil?
			@collection = @collections[0]
		else
			@collection = Collection.find(params[:collection_id])
		end

		impressionist(@collection)
		# @collection_items = CollectionItem.where(:collection_id => @collection.id)


		@collection_items = CollectionItem.joins(:product).
			where(
				"collection_items.collection_id = ? 
				and collection_items.deleted_at is null 
				and products.deleted_at is null",
				@collection.id).page(params[:page]).per(20)
			
		# @collection_items = CollectionItem.joins(:product).
		# 	where(
		# 		"collection_items.collection_id = ? ",
		# 		@collection.id).page(params[:page]).per(20)

		# @collection_items = CollectionItem.includes({product: :product_images}).
		# 	where(
		# 		"collection_items.collection_id = ? ",
		# 		@collection.id).page(params[:page]).per(20)


		@comments = @collection.comment_threads.order("created_at DESC").page(1).per(20)
		@comments_count = @collection.comment_threads.count

		# @other_collections = Collection.where('user_id = :user_id and id not in (:id)', 
		# 	:user_id => @collection.user_id, :id => @collection.id).limit(5)
		# @other_collections_all = Collection.where('user_id = :user_id and id not in (:id)', 
		# 	:user_id => @collection.user_id, :id => @collection.id)
		
		# impressionist(@collection)

	rescue ActiveRecord::RecordNotFound
		render 'not_found'

	rescue NotFoundUserException
		render 'not_found'

		
	end

	def collection_manage
		@collections = current_user.collections.includes( :collection_items )
	end

	def collection_manage_show
		@collection = Collection.find(params[:collection_id])
		@collection_items = @collection.collection_items.joins(:product).page(params[:page]).per(10)
		@comments = @collection.comment_threads.order("created_at DESC").page(params[:page]).per(5)
	end

	def collection_name_change
		@collection = Collection.find(params[:collection_id])
		@collection.name = params[:collection_name]
		@collection.save

	end

	def remove_collection_item
		collection_item = CollectionItem.find(params[:item_id])
		# collection = collection_item.collection
		collection_item.destroy

		redirect_to :action => 'collection_manage_show', :collection_id => collection_item.collection_id
	end

	def remove_collection
		collection = Collection.find(params[:collection_id])
		collection.destroy

		redirect_to :action => 'collection_manage'
	end

	def collection_image_update
		collection = Collection.find(params[:collection_id])
		collection.file = params[:collection_image_upload]
		collection.save

		render :json =>  {:small_image_url => collection.featured_small_image_url}
	end
	
	

	def follow
		@collection = Collection.find(params[:collection_id])
		if current_user.following?(@collection)
			# follow = current_user.stop_following(@collection)
			follow = Follow.where(:followable_id => @collection.id, :followable_type => 'Collection', :follower_id => current_user.id, :follower_type => 'User').first
			follow.create_activity key: 'collection.follow_destroy', owner: follow.follower, recipient: follow.followable
			follow.destroy
		
		else
			current_user.follow(@collection)
		end
	end

	def unfollow
		collection = Collection.find(params[:collection_id])
		follow = current_user.stop_following(collection)
	
		# create_activity key: 'collection.follow_destroy', owner: self.follower, recipient: self.followable
	end
end

class NotFoundUserException < StandardError; end

