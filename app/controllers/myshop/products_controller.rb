module Myshop
	class ProductsController < ApplicationController
		layout 'myshop_application'

		def index

			@q = Product.select(:id, :title, :multi_price, :domain, :category_str, :image_width, :image_height, :is_crawled, :created_at).only_public.ransack(params[:q])
			category_arr = []

			@q.sorts = 'created_at desc' if @q.sorts.empty?

			# p params[:category_arr]

			if params[:category_arr]
				params[:category_arr].reject! { |c| c.empty? } 
			else
				if params[:category]
					params[:category_arr] = ProductCategory.find(params[:category]).path.map { |m| m.id }
				end
			end

			if params[:category_arr] and !params[:category_arr].empty?
				subtree_ids = ProductCategory.find(params[:category_arr].last).subtree_ids
				categories = ProductCategory.select(:code).where("id in (?)", subtree_ids)
				@products = @q.result.where("products.category_str in (?)", categories).page(params[:page]).per(20)
			else
				if params[:q]
					@products = @q.result.page(params[:page]).per(20)
				else
					if params[:category]
						subtree_ids = ProductCategory.find(params[:category]).subtree_ids
						categorys = ProductCategory.select(:code).where("id in (?)", subtree_ids)

						@products = Product.only_public.
							where("products.category_str in (?)", categorys).
							order('random_id DESC').page(params[:page]).per(20)
					else
						@products = Product.only_public.order('random_id DESC').page(params[:page]).per(20)
					end
				end
			end


			# @products = Product.only_public.order("created_at DESC").page(params[:page]).per(10)
		end

		def show
			@product = Product.find(params[:id])
		end

		def edit
			@product = Product.find(params[:id])
			# (1..(5 - @product.product_images.count)).each do
			# 	@product.product_images.build
			# end
		end

		def new
			@product = Product.new
			# @product.product_options.build
			# @product.product_options.options_items.build
			
			# (1..5).each do
			# 	@product.product_images.build
			# end
	
		end

		def create

			@product = Product.new(product_params)
			@product.is_crawled = 'false'

			category = ProductCategory.find(params[:product][:product_category_id].to_i)

			@product.category_str = category.code

			if params[:sale_price]
				@product.is_sale = 'true'
			else
				@product.is_sale = 'false'
			end

			if @product.save

				params[:product][:product_images_attributes].each do |image|
	     			
		      		ProductImage.create!({
		      			:product_id => @product.id,
		      			:upload_id => image[1][:upload_id],
		      			:sort => image[0]
		      		}) unless image[1][:upload_id].blank?

	      	end

	      	product_images = @product.product_images
					if product_images[0]
						root = Rails.root.to_s
						rootshop_index = root + "/public" + product_images[0].upload.file.url(:rootshop_index).split('?')[0]
						image_size = FastImage.size(rootshop_index) 

						# if image_size
						# 	@product.image_width = image_size[0].to_s
						# 	@product.image_height = image_size[1].to_s
						# 	# ActiveRecord::Base.connection.execute("update products set image_width=#{image_size[0].to_s} , image_height=#{image_size[1].to_s} where id=#{product.id}")
						
						# 	@product.save
						# end
					end
					# option_inventory = {}

					# @product.product_options.order('created_at ASC').each do |option|
					# 	str = ""
					# 	option.product_option_items.each_with_index do |item, i|
					# 		if i == 0
					# 			str += "#{option.title}*_*#{item.title}"
					# 		else
					# 			str += "-_-#{option.title}*_*#{item.title}"
					# 		end
					# 	end
					# 	option_inventory[str] = "available"
					# end
	      	
				redirect_to myshop_products_path
			else
				render action: "new"
			end
		end


		def update

			@product = Product.find(params[:id])
			@product.product_images.delete_all if @product.is_crawled == 'false'
			# @product.product_options.destroy_all

			respond_to do |format|

				p @product

				p ">>>>>>>>>>>>>"
				p params[:product][:sale_price].empty?
				
				if params[:product][:sale_price].empty?
					@product.is_sale = 'false'
				else
					@product.is_sale = 'true'
				end


	      if @product.update(update_product_params)
					
					if @product.is_crawled == 'false'
		      	params[:product][:product_images_attributes].each do |image|
		     			
			      		ProductImage.create!({
			      			:product_id => @product.id,
			      			:upload_id => image[1][:upload_id],
			      			:sort => image[0]
			      		}) unless image[1][:upload_id].blank?

		      	end

		    #   	inventory = []

						# @product.product_options.order('created_at ASC').each do |option|
						# 	temp_arr = []
						# 	option.product_option_items.order('created_at ASC').each_with_index do |item, i|
						# 		temp_arr << option.title + ":" + item.title
						# 	end
						# 	inventory << temp_arr
						# 	# inventory[option.title] = temp_arr
						# end

						# p "!!!!!!!!"
						# p inventory
						# inventory.each do |items|
						# 	items.each do |item|
						# 		p  item
						# 	end
						# end



		      end


	        format.html { redirect_to edit_myshop_product_path(@product) }
	        # format.json { head :no_content }
	      else
	        # format.html { render action: 'edit' }
	        # format.json { render json: @post.errors, status: :unprocessable_entity }
	      end
	    end
	  end
		

		def destroy
			@product = Product.find(params[:id])
			@product.destroy

			redirect_to myshop_products_path
		end

	private
		def product_params
			params.require(:product).permit(
				:product_category_id, 
				:title,
				:country_of_origin,
				:date_of_manufacture, 
				:detail_info,
				:price,
				:amount,
				:delivery_charge,
				:is_cash_on_arrival,
				:take_back_address,
				{:product_options_attributes => [:title, :amount, :id, :_destroy, product_option_items_attributes: [:id, :title, :_destroy]]},
				# {:product_images_attributes => [:upload_id, :sort]},
				:is_public,
				:supplier_id,
				:product_type,
				:is_soldout,
				:sale_price,
				:save_money,
				:disable_collection,
				:brand,
				:sub_title
			)
		end

		def update_product_params
			params.require(:product).permit(
				:product_category_id, 
				:title,
				:country_of_origin,
				:date_of_manufacture, 
				:detail_info,
				:price,
				:amount,
				:delivery_charge,
				:is_cash_on_arrival,
				:take_back_address,
				{:product_options_attributes => [:title, :amount, :id, :_destroy, product_option_items_attributes: [:id, :title, :_destroy]]},
				:is_public,
				:supplier_id,
				:product_type,
				:is_soldout,
				:sale_price,
				:save_money,
				:disable_collection,
				:brand,
				:sub_title
			)
		end

	end
end