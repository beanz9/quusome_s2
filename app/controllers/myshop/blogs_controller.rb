module Myshop
	class BlogsController < ApplicationController
		
		layout 'myshop_application'

		def index
			@blogs = Blog.order('is_featured DESC, created_at DESC').page(params[:page]).per(10)
		end

		def new
			@blog = Blog.new
			
		end

		def edit
			@blog = Blog.find(params[:id])
		end

		def create
			@blog = Blog.new(blog_params)
	  
      if @blog.save
        redirect_to :action => 'index'  
      else
        render action: 'new' 
      end

		end

		def update
			@blog = Blog.find(params[:id])

			if @blog.update(blog_params)
				redirect_to :action => 'index' 
			else
				render action: 'edit' 
			end
		end

		def destroy
			@blog = Blog.find(params[:id])
			@blog.destroy

			redirect_to :action => 'index' 
		end


	private
		def blog_params
			params.require(:blog).permit(:title, :body, :is_public, :is_featured, :description, :blog_category_id, :image)
		end

	end
end