module Myshop
	class SavedMoneyRatesController < ApplicationController

		layout 'myshop_application'

		def index
			@saved_money_rates = SavedMoneyRate.order("created_at DESC")
			@saved_money_rate = SavedMoneyRate.new
		end

		def create
			@saved_money_rate = SavedMoneyRate.new(saved_money_rate_params)

			respond_to do |format|
	      if @saved_money_rate.save
	        format.html { redirect_to :action => 'index'  }
	        # format.json { render action: 'show', status: :created, location: @supplier }
	      else
	        format.html { render action: 'new' }
	        # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	      end
	    end
	    
		end


	private
		def saved_money_rate_params
			params.require(:saved_money_rate).permit(:saved_money_rate)
		end
	end
end