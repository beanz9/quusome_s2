class Myshop::ProfitWithdrawRequestsController < ApplicationController
	
	layout 'myshop_application'

	def index
		@profit_withdraw_requests = ProfitWithdrawRequests.order('created_at DESC')
	end

	def update_state
		profit_withdraw_request = ProfitWithdrawRequests.find(params[:id])
		profit_withdraw_request.is_done = true
		profit_withdraw_request.save

		redirect_to action: 'index'
	end
end