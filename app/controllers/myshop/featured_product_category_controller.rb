module Myshop
	class FeaturedProductCategoryController < ApplicationController

		layout 'myshop_application'

		def index
			@featured_product_category = FeaturedProductCategory.order('sort_num ASC')
		end

		def new
			@featured_product_category = FeaturedProductCategory.new
		end

		def create
			@featured_product_category = FeaturedProductCategory.new(featured_product_category_params)
			@featured_product_category.save
			redirect_to :action => 'index'   
		end

		def edit
			@featured_product_category = FeaturedProductCategory.find(params[:id])
		end

		def update
			@featured_product_category = FeaturedProductCategory.find(params[:id])
			@featured_product_category.update(featured_product_category_params)
			redirect_to :action => 'index'   
		end

		def destroy
			@featured_product_category = FeaturedProductCategory.find(params[:id])
			@featured_product_category.destroy
			redirect_to :action => 'index'
		end

	private
		def featured_product_category_params
			params.require(:featured_product_category).permit(:name, :sort_num, :is_public)
		end

	end

end