module Myshop
	class InternationalDeliveryChargesController < ApplicationController
		layout 'myshop_application'

		def index
			@international_delivery_charges = InternationalDeliveryCharge.order("weight DESC").page(params[:page]).per(20)
		end

		def new
			@international_delivery_charge = InternationalDeliveryCharge.new
		end

		def edit
			@international_delivery_charge = InternationalDeliveryCharge.find(params[:id])
		end

		def create
			@international_delivery_charge = InternationalDeliveryCharge.new(international_delivery_charge_params)

	    respond_to do |format|
	      if @international_delivery_charge.save
	        format.html { redirect_to :action => 'index'  }
	        # format.json { render action: 'show', status: :created, location: @supplier }
	      else
	        format.html { render action: 'new' }
	        # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	      end
	    end
		end

		def update
			@international_delivery_charge = InternationalDeliveryCharge.find(params[:id])
			@international_delivery_charge.update(international_delivery_charge_params)
			redirect_to :action => 'index'
		end

		def destroy
			@international_delivery_charge = InternationalDeliveryCharge.find(params[:id])
			@international_delivery_charge.destroy

			redirect_to action: 'index'
		end
	private
		def international_delivery_charge_params
			params.require(:international_delivery_charge).permit(:weight, :location, :price)

		end
	end
end