module Myshop
	class Myshop::DashboardController < ApplicationController

		layout 'myshop_application'

		before_filter :authenticate_user!
		before_filter :request_user_destroy

		def index
		end

		#판매자 회원탈퇴 요청시 
		def request_user_destroy
			request_user_destroy = RequestUserDestroy.where(:user_id => current_user.id)

			unless request_user_destroy.empty?
				redirect_to root_path
			end
		end
	end
end