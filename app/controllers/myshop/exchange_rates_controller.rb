module Myshop
	class ExchangeRatesController < ApplicationController
		
		layout 'myshop_application'

		def index
			@exchange_rates = ExchangeRate.find_by_sql("select distinct on (currency) * from exchange_rates order by currency, created_at DESC")
			@exchange_rate  = ExchangeRate.new
		end

		def create
			@exchange_rate = ExchangeRate.new(exchange_rate_params)

	    respond_to do |format|
	      if @exchange_rate.save
	        format.html { redirect_to :action => 'index'  }
	        # format.json { render action: 'show', status: :created, location: @supplier }
	      else
	        format.html { render action: 'new' }
	        # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	      end
	    end
		end

	private
		def exchange_rate_params
			params.require(:exchange_rate).permit(:currency, :price)
		end
	end
end