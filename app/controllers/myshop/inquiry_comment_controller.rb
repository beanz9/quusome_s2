class Myshop::InquiryCommentController < ApplicationController

	layout 'myshop_application'
	def index
		@inquiry_comments = Comment.where(:commentable_type => 'Product', :parent_id => nil).order("created_at DESC").page(params[:page]).per(20)
	end

	def show
		@inquiry_comment = Comment.find(params[:id])
		@child_comment = Comment.where(:parent_id => @inquiry_comment.id).order("created_at DESC")
	end

	def create
		user_who_commented = @current_user
		parent_comment = Comment.find(params[:id])
		product = Product.find(parent_comment.commentable_id)
		
		@comment = Comment.build_from( product, user_who_commented.id, params[:comment] )
		if @comment.save
			@comment.move_to_child_of(parent_comment)
		end
		
		redirect_to action: 'show'
	end

	def destroy
		comment = Comment.find(params[:id])
		comment.destroy
		p comment.parent.id
		
		redirect_to action: 'show', :id => comment.parent.id
	end


end