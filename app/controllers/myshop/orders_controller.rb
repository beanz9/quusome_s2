module Myshop
	class OrdersController < ApplicationController

		layout 'myshop_application'
		around_filter :transactional

		require 'order_state'

		def index
			@orders_items = OrdersItem.
			group("product_id, product_title, product_option_id, product_option_title, order_id, state, product_featured_small_image_url , options").
			select("max(orders_items.id) as id, product_id, product_title, product_option_id, product_option_title, 
				 order_id, sum(orders_items.price) as price, product_featured_small_image_url, 
				 state, count(1) as quantity, max(orders_items.created_at) as created_at, count(order_id) as order_id_count, options").
			
			search_product_title(params[:title]).
			search_state(params[:state]).
			search_date(params[:start_date], params[:end_date]).
			order("max(orders_items.created_at) DESC").
			group("order_id").
			page(params[:page]).per(10)

				
		end

		def change_state
			# orders_item = OrdersItem.find(params[:id])

			# @orders_item = OrdersItem.
			# group("product_id, product_title, product_option_id, product_option_title, order_id, state, product_featured_small_image_url , options").
			# select("max(orders_items.id) as id, product_id, product_title, product_option_id, product_option_title, 
			# 	 order_id, sum(orders_items.price) as price, product_featured_small_image_url, 
			# 	 state, count(1) as quantity, max(orders_items.created_at) as created_at, count(order_id) as order_id_count, options").
			# find_by_order_id(orders_item.order_id)

			@orders_item = OrdersItem.find(params[:id])
		
			options = @orders_item.options.to_s.gsub('{', '').gsub('}', '') if @orders_item.options

			@order_quantity = OrdersItem.where(:order_id => @orders_item.order_id, :product_id => @orders_item.product_id, :options => options).count
			
			if @order_quantity > 1
				@order_item_ids = OrdersItem.order('created_at ASC').where(:order_id => @orders_item.order_id, :product_id => @orders_item.product_id, :options => options).pluck(:id)
			end
		end

		def give_saved_money
			orders_item = OrdersItem.find(params[:id])

			saved_money = SavedMoney.new
			saved_money.user_id = orders_item.user_id
			saved_money.order_id = orders_item.order_id

			if orders_item.options
				options = orders_item.options.to_s.gsub('{', '').gsub('}', '')
			else
				options = ""
			end

			saved_money.money = OrdersItem.where(:order_id => orders_item.order_id, :product_id => orders_item.product_id, :options => options).sum {|o| o.saved_money }.to_i
			saved_money.orders_item_id = orders_item.id

			saved_money.save

			orders_item.saved_money_state = 'gived'
			orders_item.save

			collection_item = CollectionItem.where(:id => orders_item.collection_item_id).first
			p ">>>>>>>"



			if orders_item.collection_item_id and collection_item.collection
				collection_share_rate = 0.1
				profit_rate = 0.1

				profit = Profit.new
				profit.collection_owner_id = collection_item.user_id
				profit.collection_item_id = collection_item.id
				profit.collection_profit = OrdersItem.where(:order_id => orders_item.order_id, :product_id => orders_item.product_id, :options => options).sum {|o| (o.original_price * profit_rate * collection_share_rate) * orders_item.exchange_rate  }.to_i
				profit.collection_charge_rate = collection_share_rate
				profit.charge_rate = profit_rate
				profit.product_id = orders_item.product_id
				profit.user_id = current_user.id
				profit.orders_item_id = orders_item.id
				profit.order_id = orders_item.order_id
				profit.collection_id = collection_item.collection_id

				profit.save
			end
			



			redirect_to action: 'change_state'

		end

		def take_back_change_state
			@take_back_request = TakeBackRequest.find(params[:id])
			@orders_item = OrdersItem.find(@take_back_request.orders_item_id)
			options = @orders_item.options.to_s.gsub('{', '').gsub('}', '') if @orders_item.options

			@order_quantity = OrdersItem.where(:order_id => @orders_item.order_id, :product_title => @orders_item.product_title, :options => options).count
		end

		# def confirm
		# 	os = OrderState.new
		# 	os.confirm(params)
		# 	redirect_to action: 'index'
		# end

		def cancel
			os = OrderState.new
			os.cancel(params)
			redirect_to action: 'change_state'
		end

		def local_order
			os = OrderState.new
			os.local_order(params)
			redirect_to action: 'change_state'
		end

		def local_delivery
			os = OrderState.new
			os.local_delivery(params)
			redirect_to action: 'change_state'
		end

		def international_delivery
			os = OrderState.new
			os.international_delivery(params)
			redirect_to action: 'change_state'
		end

		def take_back_complete
			os = OrderState.new
			os.take_back_complete(params)
			redirect_to action: 'take_back_change_state', :id => params[:take_back_request_id]
		end

		def take_back_cancel
			os = OrderState.new
			os.take_back_cancel(params)
			redirect_to action: 'take_back_change_state', :id => params[:take_back_request_id]
		end

		def take_back_confirm
			os = OrderState.new
			os.take_back_confirm(params)
			redirect_to action: 'take_back_change_state', :id => params[:take_back_request_id]
		end

		def refund
			os = OrderState.new
			os.refund(params)
			redirect_to action: 'take_back_change_state', :id => params[:take_back_request_id]
		end

		def refund_hold
			os = OrderState.new
			os.refund_hold(params)
			redirect_to action: 'take_back_change_state', :id => params[:take_back_request_id]
		end

		def take_back_receives
			@take_back_request = TakeBackRequest.order("created_at DESC").page(params[:page]).per(10)

		end

		
	private
		def transactional
		  ActiveRecord::Base.transaction do
		    yield
		  end
		end
	end
end