module Myshop
	class FeaturedProductsController < ApplicationController

		layout 'myshop_application'

		def index
			@featured_products = FeaturedProduct.page(params[:page]).per(20)

		end

		def new
			
			@q = Product.only_public.ransack(params[:q])
			category_arr = []

			@q.sorts = 'created_at desc' if @q.sorts.empty?

			# p params[:category_arr]

			if params[:category_arr]
				params[:category_arr].reject! { |c| c.empty? } 
			else
				if params[:category]
					params[:category_arr] = ProductCategory.find(params[:category]).path.map { |m| m.id }
				end
			end

			if params[:category_arr] and !params[:category_arr].empty?
				subtree_ids = ProductCategory.find(params[:category_arr].last).subtree_ids
				categories = ProductCategory.select(:code).where("id in (?)", subtree_ids)
				@products = @q.result.where("products.category_str in (?)", categories).page(params[:page]).per(20)
			else
				if params[:q]
					@products = @q.result.page(params[:page]).per(20)
				else
					if params[:category]
						subtree_ids = ProductCategory.find(params[:category]).subtree_ids
						categorys = ProductCategory.select(:code).where("id in (?)", subtree_ids)

						@products = Product.only_public.
							where("products.category_str in (?)", categorys).
							order('random_id DESC').page(params[:page]).per(20)
					else
						@products = Product.only_public.order('random_id DESC').page(params[:page]).per(20)
					end
				end
			end
			@featured_product = FeaturedProduct.new
		end

		def create
			@featured_product = FeaturedProduct.new(product_params)
			@featured_product.save
		end

		def destroy
			@featured_product = FeaturedProduct.find(params[:id])
			@featured_product.destroy

			respond_to do |format|
	      format.html { redirect_to :action => 'index' }
	    end
		end



		private
		def product_params
			params.require(:featured_product).permit(:product_id, :title, :description, :featured_type, :featured_product_category_id)
		end

	end
end