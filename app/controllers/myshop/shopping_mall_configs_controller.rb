module Myshop
	class ShoppingMallConfigsController < ApplicationController
		layout 'myshop_application'
		before_action :set_shopping_mall_config, only: [:edit, :update, :destroy]

		def index
			@shopping_mall_configs = ShoppingMallConfig.page(params[:page]).per(10)
		end

		def new
			@shopping_mall_config = ShoppingMallConfig.new
		end

		def create
			@shopping_mall_config = ShoppingMallConfig.new(shopping_mall_config_params)

	    respond_to do |format|
	      if @shopping_mall_config.save
	        format.html { redirect_to :action => 'index'  }
	        # format.json { render action: 'show', status: :created, location: @supplier }
	      else
	        format.html { render action: 'new' }
	        # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	      end
	    end
		end

		def edit
		end

		def update
			respond_to do |format|
	      if @shopping_mall_config.update(shopping_mall_config_params)
	        format.html { redirect_to :action => 'index'  }
	        format.json { head :no_content }
	      else
	        format.html { render action: 'edit' }
	        format.html { render action: 'new' }
	      end
	    end

		end

		def destroy
			@shopping_mall_config.destroy
	    respond_to do |format|
	      format.html { redirect_to :action => 'index'  }
	      format.json { head :no_content }
	    end
		end

	private
		def shopping_mall_config_params
			params.require(:shopping_mall_config).permit(:domain, :currency, :location, :delivery_charge, :free_condition)
		end

		def set_shopping_mall_config
			@shopping_mall_config = ShoppingMallConfig.find(params[:id])
		end
	end
end