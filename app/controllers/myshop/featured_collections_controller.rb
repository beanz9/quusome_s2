module Myshop
	class FeaturedCollectionsController < ApplicationController

		layout 'myshop_application'

		def index
			@featured_collections = FeaturedCollection.joins(:collection)
		end

		def new
			@collections = Collection.order("created_at DESC").page(params[:page]).per(20)
			@featured_collection = FeaturedCollection.new
		end

		def create
			@featured_collection = FeaturedCollection.new(collction_params)
			@featured_collection.save

			# respond_to do |format|
	  #     if @featured_collection.save
	  #       format.html { redirect_to :action => 'index'  }
	  #       # format.json { render action: 'show', status: :created, location: @supplier }
	  #     else
	  #       format.html { render action: 'new' }
	  #       # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	  #     end
	  #   end
		end

		def destroy
			@featured_collection = FeaturedCollection.find(params[:id])
			@featured_collection.destroy

			respond_to do |format|
	      format.html { redirect_to :action => 'index' }
	    end
		end

		

	private
		def collction_params
			params.require(:featured_collection).permit(:collection_id, :title, :description)
		end
	end
end 