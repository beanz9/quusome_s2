module Myshop
	class BlogCategoriesController < ApplicationController

		layout 'myshop_application'

		def index
			@blog_categories = BlogCategory.order("sort_num ASC")
		end

		def new
			@blog_category = BlogCategory.new
		end

		def create
			@blog_category = BlogCategory.new(blog_category_params)
	  
      if @blog_category.save
        redirect_to :action => 'index'  
      else
        render action: 'new' 
      end
	  
		end

		def edit
			@blog_category = BlogCategory.find(params[:id])
		end

		def update

			@blog_category = BlogCategory.find(params[:id])

			if @blog_category.update(blog_category_params)
				redirect_to :action => 'index' 
			else
				render action: 'edit' 
			end

		end

		def destroy
			@blog_category = BlogCategory.find(params[:id])

			@blog_category.destroy
			redirect_to :action => 'index' 
		end

	private
		def blog_category_params
			params.require(:blog_category).permit(:name, :sort_num)
		end
	end
end