module Myshop
	class SuppliersController < ApplicationController
		layout 'myshop_application'

		def index
			@suppliers = Supplier.where(:created_by_id => current_user.id).page(params[:page]).per(10)
		end

		def new
			@supplier = Supplier.new
		end

		def edit
			@supplier = Supplier.find(params[:id])

		end

		def create
			@supplier = Supplier.new(supplier_params)

	    respond_to do |format|
	      if @supplier.save
	        format.html { redirect_to :action => 'index'  }
	        # format.json { render action: 'show', status: :created, location: @supplier }
	      else
	        format.html { render action: 'new' }
	        # format.json { render json: @supplier.errors, status: :unprocessable_entity }
	      end
	    end
		end

		def update

			@supplier = Supplier.find(params[:id])
			
			respond_to do |format|
      if @supplier.update(supplier_params)
        format.html { redirect_to :action => 'index'  }
        # format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        # format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end

		end

		def destroy
			@supplier = Supplier.find(params[:id])
			@supplier.destroy

			redirect_to :action => 'index'
		end

	private
		def supplier_params
			params.require(:supplier).permit(:name, :description, :address, :take_back_address)
		end
	end
end