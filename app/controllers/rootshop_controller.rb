class RootshopController < ApplicationController

	layout 'rootshop'
	impressionist :actions => [:show]

	def index



		# if params["q"]
		# 	unless params["q"]["s"] 
		# 		params["q"]["s"] = "created_at desc"
		# 	end
		# else
		# 	params["q"] = {}
		# 	params["q"]["s"] = "created_at desc"
		# end

		@q = Product.only_public.ransack(params[:q])
		category_arr = []

		@q.sorts = 'created_at desc' if @q.sorts.empty?

		# p params[:category_arr]

		if params[:category_arr]
			params[:category_arr].reject! { |c| c.empty? } 
		else
			if params[:category]
				params[:category_arr] = ProductCategory.find(params[:category]).path.map { |m| m.id }
			end
		end



		if params[:category_arr] and !params[:category_arr].empty?
			subtree_ids = ProductCategory.find(params[:category_arr].last).subtree_ids
			categories = ProductCategory.select(:code).where("id in (?)", subtree_ids)
			@products = @q.result.where("products.category_str in (?)", categories).page(params[:page]).per(20)
		else
			if params[:q]
				@products = @q.result.page(params[:page]).per(20)
			else
				if params[:category]
					subtree_ids = ProductCategory.find(params[:category]).subtree_ids
					categorys = ProductCategory.select(:code).where("id in (?)", subtree_ids)

					@products = Product.only_public.
						where("products.category_str in (?)", categorys).
						order('random_id DESC').page(params[:page]).per(20)
				else
					@products = Product.only_public.order('random_id DESC').page(params[:page]).per(20)
				end
			end
		end

		
		# if params[:category].present?
		# 	subtree_ids = ProductCategory.find(params[:category]).subtree_ids
		# 	categorys = ProductCategory.select(:code).where("id in (?)", subtree_ids)

		# 	@products = Product.only_public.
		# 		where("products.category_str in (?)", categorys).
		# 		order('random_id DESC').page(params[:page]).per(20)
		# else
		# 	if params[:q].present?
		# 		@products = Product.only_public.search(params[:q]).page(params[:page]).per(20)
		# 	else
		# 		if params[:sort] == 'new'
		# 			@products = Product.only_public.order('created_at DESC').page(params[:page]).per(20)
		# 		else
		# 			@products = Product.only_public.order('random_id DESC').page(params[:page]).per(20)
		# 		end
		# 	end
			
		# end
		
	end

	def category_child_2
		category = ProductCategory.find(params[:id])
		@categories = category.children
	end

	def category_child_3
		category = ProductCategory.find(params[:id])
		@categories = category.children
	end

	def show

		@product = Product.only_public.find(params[:id])

	

		@category = ProductCategory.where("code = ?", @product.category_str).first
		
		impressionist(@product)

		@other_products = Product.only_public.where(:brand => @product.brand).limit(5).order('RANDOM()')
		@other_products_all = Product.only_public.where(:brand => @product.brand).order('created_at DESC')

		# @inquiry_comments = @product.comment_threads.order("created_at DESC").page(1).per(10)

		@inquiry_comments = Comment.where(:commentable_type => 'Product', :commentable_id => @product.id, :parent_id => nil).order("created_at DESC").page(1).per(5)
		# @inquiry_comments_count = @product.comment_threads.count
		
		# @reviews_count = @product.reviews.count
		@collection_items = CollectionItem.joins(:product).where(:user_id => current_user.id, :product_id => @product.id) if current_user
		@collections_include_this_product = Collection.where('id in (?)', CollectionItem.where(:product_id => @product.id).map {|m| m.collection_id})
		@collections = current_user.collections.order('created_at DESC').limit(8) if current_user

	rescue ActiveRecord::RecordNotFound
		render 'not_found'
	

	end

	def collection_item_add

		sleep 1

		if params[:collection_id].nil?
			@collection = Collection.new({ :name => "기본 컬렉션", :user_id => current_user.id })
			if @collection.save
				CollectionItem.create({ :collection_id => @collection.id, :product_id => params[:product_id], :user_id => current_user.id })
			end

		else
			CollectionItem.create({ :collection_id => params[:collection_id], :product_id => params[:product_id], :user_id => current_user.id })
		end

		@product = Product.find(params[:product_id])
		@collection_items = CollectionItem.where(:user_id => current_user.id, :product_id => @product.id)
		@collections = current_user.collections.order('created_at DESC').limit(8)
	end

	def collection_item_remove

		sleep 1
		
		@collection_item = CollectionItem.find_by(:collection_id => params[:collection_id], :product_id => params[:product_id], :user_id => current_user.id )
		@collection_item.destroy
		@product = Product.find(params[:product_id])
		@collection_items = CollectionItem.where(:user_id => current_user.id, :product_id => @product.id)
		@collections = current_user.collections.order('created_at DESC').limit(8)
	end

	def collection_add

		sleep 1

		Collection.create({:name => params[:collection_name], :user_id => current_user.id})
		@product = Product.find(params[:product_id])
		@collections = current_user.collections.order('created_at DESC').limit(8)
	end

	def collection_remove

		sleep 1
		
		@collection = Collection.find(params[:collection_id])
		@collection.destroy
		@product = Product.find(params[:product_id])
		@collection_items = CollectionItem.where(:user_id => current_user.id, :product_id => @product.id)
		@collections = current_user.collections.order('created_at DESC').limit(8)
	end




end