ActiveAdmin.register TalentCategory do

	filter :name
	
	controller do
		def permitted_params
      params.permit talent_category: [:name, :description]
    end
	end
	
end
