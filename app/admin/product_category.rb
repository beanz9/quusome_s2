#encode: utf-8
ActiveAdmin.register ProductCategory do

	filter :name
	sortable tree: true

	index :as => :sortable do
    label do |a|
      a.name + " - " + a.weight.to_s + 'kg'
    end

    	actions
        
  end

  show do |pc|
		attributes_table do
			row "이름" do
        pc.name
      end
      row "코드" do
        pc.code
      end
      row "수수료" do
        pc.charge
      end
      row "설명" do
        pc.description
      end
      row "무게" do
        pc.weight
      end
      row "생성일" do
        pc.created_at.strftime("%F %T")
      end
      row "수정일" do
        pc.updated_at.strftime("%F %T")
      end
		end

  	panel "자식카테고리" do
      table_for pc.children do

        column "이름" do |children|
          simple_format "<a href='#{admin_product_category_path(children)}'>" + children.name + "</a>"
        end
        column "코드" do |children|
          if children.code
            children.code
          else
            "비었음"
          end
        end
        column "수수료" do |children|
          if children.charge
            children.charge
          else
            "비었음"
          end
          
        end
        column "생성일" do |children|
          children.created_at.strftime("%F %T")
        end
        column "수정일" do |children|
          children.updated_at.strftime("%F %T")
        end
      end
  		# render 'show'
  	end

    panel "가격대별 수수료" do
      table_for pc.product_charge_ranges do
        column "시작가격" do |product_charge_ranges|
          product_charge_ranges.from_price_currency_comma
        end
        column "끝가격" do |product_charge_ranges|
          product_charge_ranges.to_price_currency_comma
        end
        column "수수료" do |product_charge_ranges|
          product_charge_ranges.charge + " %"
        end
      end
    end

    active_admin_comments
  end

  # form do |f|
  #   f.form_buffers.last << Arbre::Context.new({}, f.template) do
  #     panel "Panel Above Form" do
  #       ul do
  #         li "item 1"
  #         li "item 2"
  #       end

  #       attributes_table_for(f.object) do
  #         row :name
  #       end
  #     end
  #   end

  #   f.inputs
  # end


  form do |f|
    f.inputs "Product Category 수정" do
      f.input :name
      f.input :code
      f.input :charge
      f.input :weight
      f.input :description
    end

    unless f.object.id.nil?
      f.form_buffers.last << Arbre::Context.new({}, f.template) do
        panel "Panel Above Form" do
          render partial: 'child_category_form'
          
            table_for(f.object.children) do
              column "이름" do |children|
                children.name
              end

              column "코드" do |children|
                children.code
              end

              column "수수료" do |children|
                if !children.charge.nil?
                  children.charge.to_s + " %"
                else
                  "비었음"
                end
              end

              column "생성일" do |children|
                children.created_at.strftime("%F %T")
              end

              column "수정일" do |children|
                children.updated_at.strftime("%F %T")
              end
            end
        end
      end
    end


    f.inputs "가격대별 수수료" do
      f.has_many :product_charge_ranges, :allow_destroy => true, 
        :heading => false, :new_record => true do |pcr|

        pcr.input :from_price, :label => '시작가격'
        pcr.input :to_price, :label => '끝가격' 
        pcr.input :charge, :label => '수수료'
      end
    end

    f.actions
  end

  # form :partial => "form"

  member_action :add_categories, :method => :post do
    parent_product_category = ProductCategory.find(params[:id])
    parent_product_category.children.create :name => params[:name], :code => params[:code], :charge => params[:charge], :weight => params[:weight], :description => params[:description]
    redirect_to :action => :edit
  end

	controller do

    def update
      update! 
      expire_fragment "category_menu_cache"
    end

    def create
      create! 
      expire_fragment "category_menu_cache"
    end

		def permitted_params
      params.permit product_category: [:name, :code, :weight, :description, product_charge_ranges_attributes: [:id, :from_price, :to_price, :charge, :_destroy]]
    end
	end
	
end
