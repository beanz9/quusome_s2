ActiveAdmin.register CreditCardCompany do

	filter :name
	
	controller do
		def permitted_params
      params.permit credit_card_company: [:name]
    end
	end
	
end
