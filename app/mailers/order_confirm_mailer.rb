class OrderConfirmMailer < ActionMailer::Base
	layout 'email'

  default from: "admin@quusome.com"

  def confirm_email(url)
  	email = url

  	mail(to: email, subject: "test")
  end
end
