module ApplicationHelper
	def price_currency_comma(price)
		price.to_s.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
	end

	def render_menu(category)
		s = '<ul>'
		category.each do |item|

			if item.has_children?
				s << "<li><a href='/rootshop?category=#{item.id}'>#{item.name}</a>"
				s << render_menu(item.children)
				s << "</li>"
			else
				s << "<li><a href='/rootshop?category=#{item.id}'>#{item.name}</a></li>"
			end
		end
		s << '</ul>'
		
		s
	end

	def saved_money_calc(price)
		(price * SavedMoneyRate.order("created_at DESC").first.saved_money_rate * 0.01).to_i
	end

end
