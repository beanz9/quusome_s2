--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    namespace character varying(255),
    body text,
    resource_id character varying(255) NOT NULL,
    resource_type character varying(255) NOT NULL,
    author_id integer,
    author_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: activities; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE activities (
    id integer NOT NULL,
    trackable_id integer,
    trackable_type character varying(255),
    owner_id integer,
    owner_type character varying(255),
    key character varying(255),
    parameters text,
    recipient_id integer,
    recipient_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: activities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE activities_id_seq OWNED BY activities.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE admin_users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admin_users_id_seq OWNED BY admin_users.id;


--
-- Name: blog_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE blog_categories (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    sort_num integer
);


--
-- Name: blog_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE blog_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: blog_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE blog_categories_id_seq OWNED BY blog_categories.id;


--
-- Name: blogs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE blogs (
    id integer NOT NULL,
    title character varying(255),
    body text,
    description text,
    is_public boolean,
    blog_category_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    image_file_name character varying(255),
    image_content_type character varying(255),
    image_file_size integer,
    image_updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    is_featured boolean
);


--
-- Name: blogs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE blogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: blogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE blogs_id_seq OWNED BY blogs.id;


--
-- Name: boards; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE boards (
    id integer NOT NULL,
    title character varying(255),
    body text,
    user_id integer,
    board_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: boards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE boards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: boards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE boards_id_seq OWNED BY boards.id;


--
-- Name: change_states; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE change_states (
    id integer NOT NULL,
    state character varying(255),
    state_name character varying(255),
    "desc" text,
    orders_item_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: change_states_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE change_states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: change_states_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE change_states_id_seq OWNED BY change_states.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ckeditor_assets (
    id integer NOT NULL,
    data_file_name character varying(255) NOT NULL,
    data_content_type character varying(255),
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: collection_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE collection_items (
    id integer NOT NULL,
    collection_id integer,
    product_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id integer,
    deleted_at timestamp without time zone
);


--
-- Name: collection_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE collection_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: collection_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE collection_items_id_seq OWNED BY collection_items.id;


--
-- Name: collections; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE collections (
    id integer NOT NULL,
    user_id integer,
    name character varying(255),
    deleted_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    info text,
    file_file_name character varying(255),
    file_content_type character varying(255),
    file_file_size integer,
    file_updated_at timestamp without time zone,
    collection_items_count integer DEFAULT 0
);


--
-- Name: collections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: collections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE collections_id_seq OWNED BY collections.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE comments (
    id integer NOT NULL,
    title character varying(50) DEFAULT ''::character varying,
    body text,
    commentable_id integer,
    commentable_type character varying(255),
    user_id integer,
    role character varying(255) DEFAULT 'comments'::character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    subject character varying(255),
    parent_id integer,
    lft integer,
    rgt integer
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: credit_card_companies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE credit_card_companies (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: credit_card_companies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credit_card_companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credit_card_companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credit_card_companies_id_seq OWNED BY credit_card_companies.id;


--
-- Name: exchange_rates; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE exchange_rates (
    id integer NOT NULL,
    currency character varying(255),
    price integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: exchange_rates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE exchange_rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: exchange_rates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE exchange_rates_id_seq OWNED BY exchange_rates.id;


--
-- Name: featured_collections; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE featured_collections (
    id integer NOT NULL,
    collection_id integer,
    title text,
    description text,
    is_public boolean,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    sort integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: featured_collections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE featured_collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: featured_collections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE featured_collections_id_seq OWNED BY featured_collections.id;


--
-- Name: featured_product_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE featured_product_categories (
    id integer NOT NULL,
    name character varying(255),
    sort_num integer,
    is_public boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: featured_product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE featured_product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: featured_product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE featured_product_categories_id_seq OWNED BY featured_product_categories.id;


--
-- Name: featured_products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE featured_products (
    id integer NOT NULL,
    product_id integer,
    is_public boolean,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    sort integer,
    title character varying(255),
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    featured_type character varying(255),
    featured_product_category_id integer
);


--
-- Name: featured_products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE featured_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: featured_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE featured_products_id_seq OWNED BY featured_products.id;


--
-- Name: follows; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE follows (
    id integer NOT NULL,
    followable_id integer NOT NULL,
    followable_type character varying(255) NOT NULL,
    follower_id integer NOT NULL,
    follower_type character varying(255) NOT NULL,
    blocked boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: follows_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE follows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: follows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE follows_id_seq OWNED BY follows.id;


--
-- Name: image_checks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE image_checks (
    id integer NOT NULL,
    product_id integer,
    url character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: image_checks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE image_checks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: image_checks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE image_checks_id_seq OWNED BY image_checks.id;


--
-- Name: impressions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE impressions (
    id integer NOT NULL,
    impressionable_type character varying(255),
    impressionable_id integer,
    user_id integer,
    controller_name character varying(255),
    action_name character varying(255),
    view_name character varying(255),
    request_hash character varying(255),
    ip_address character varying(255),
    session_hash character varying(255),
    message text,
    referrer text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: impressions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE impressions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: impressions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE impressions_id_seq OWNED BY impressions.id;


--
-- Name: international_delivery_charges; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE international_delivery_charges (
    id integer NOT NULL,
    location character varying(255),
    weight integer,
    price integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: international_delivery_charges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE international_delivery_charges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: international_delivery_charges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE international_delivery_charges_id_seq OWNED BY international_delivery_charges.id;


--
-- Name: line_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE line_items (
    id integer NOT NULL,
    product_id integer,
    quantity integer,
    price numeric,
    created_by_id integer,
    updated_by_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    product_option_id integer,
    deleted_at timestamp without time zone,
    collection_item_id integer,
    collection_id integer,
    options hstore[]
);


--
-- Name: line_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE line_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: line_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE line_items_id_seq OWNED BY line_items.id;


--
-- Name: normal_profiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE normal_profiles (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    gender character varying(255),
    age character varying(255),
    country character varying(255),
    facebook character varying(255),
    twitter character varying(255),
    description text,
    avatar_file_name character varying(255),
    avatar_content_type character varying(255),
    avatar_file_size integer,
    avatar_updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: normal_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE normal_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: normal_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE normal_profiles_id_seq OWNED BY normal_profiles.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE orders (
    id integer NOT NULL,
    recipient character varying(255),
    zipcode character varying(255),
    address text,
    detail_address text,
    mobile_phone_1 character varying(255),
    mobile_phone_2 character varying(255),
    mobile_phone_3 character varying(255),
    phone_1 character varying(255),
    phone_2 character varying(255),
    phone_3 character varying(255),
    shipping_note text,
    payment_method character varying(255),
    created_by_id integer,
    updated_by_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    encrypted_rrn_1 character varying(255),
    encrypted_rrn_2 character varying(255),
    saved_money numeric,
    checkout_price numeric,
    original_price numeric,
    exchange_rate numeric,
    recipient_en character varying(255),
    email character varying(255),
    private_custom_number character varying(255),
    custom_type character varying(255),
    ordr_idxx character varying(255)
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: orders_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE orders_items (
    id integer NOT NULL,
    product_id integer,
    product_option_id integer,
    created_by_id integer,
    updated_by_id integer,
    deleted_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    order_id integer,
    quantity integer,
    price numeric,
    user_id integer,
    state character varying(255),
    collection_id integer,
    collection_item_id integer,
    product_title character varying(255),
    product_option_title character varying(255),
    product_featured_small_image_url character varying(255),
    options hstore,
    exchange_rate numeric,
    location character varying(255),
    currency character varying(255),
    delivery_charge numeric,
    international_delivery_charge numeric,
    charge numeric,
    original_price numeric,
    is_calculate boolean DEFAULT false,
    used_saved_money numeric,
    original_exchange_price numeric,
    saved_money numeric,
    saved_money_rate numeric,
    saved_money_state character varying(255),
    collection_share_money numeric
);


--
-- Name: orders_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_items_id_seq OWNED BY orders_items.id;


--
-- Name: payment_result_data; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE payment_result_data (
    id integer NOT NULL,
    site_cd character varying(255),
    req_tx character varying(255),
    use_pay_method character varying(255),
    "bSucc" character varying(255),
    panc_mod_mny character varying(255),
    panc_rem_mny character varying(255),
    mod_type character varying(255),
    amount character varying(255),
    res_cd character varying(255),
    res_msg text,
    ordr_idxx character varying(255),
    tno character varying(255),
    good_mny character varying(255),
    good_name text,
    buyr_name character varying(255),
    buyr_tel1 character varying(255),
    buyr_tel2 character varying(255),
    buyr_mail character varying(255),
    app_time character varying(255),
    card_cd character varying(255),
    card_name character varying(255),
    noinf character varying(255),
    app_no character varying(255),
    quota character varying(255),
    partcanc_yn character varying(255),
    card_bin_type_01 character varying(255),
    card_bin_type_02 character varying(255),
    bank_name character varying(255),
    bank_code character varying(255),
    bankname character varying(255),
    depositor character varying(255),
    account character varying(255),
    va_date character varying(255),
    pnt_issue character varying(255),
    pnt_app_time character varying(255),
    pnt_app_no character varying(255),
    pnt_amount character varying(255),
    add_pnt character varying(255),
    use_pnt character varying(255),
    rsv_pnt character varying(255),
    commid character varying(255),
    mobile_no character varying(255),
    tk_van_code character varying(255),
    tk_app_no character varying(255),
    cash_yn character varying(255),
    cash_authno character varying(255),
    cash_tr_code character varying(255),
    cash_id_info character varying(255),
    escw_yn character varying(255),
    deli_term character varying(255),
    bask_cntx character varying(255),
    good_info text,
    rcvr_name character varying(255),
    rcvr_tel1 character varying(255),
    rcvr_tel2 character varying(255),
    rcvr_mail character varying(255),
    rcvr_zipx character varying(255),
    rcvr_add1 character varying(255),
    rcvr_add2 character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    order_id integer,
    is_success boolean
);


--
-- Name: payment_result_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_result_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_result_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_result_data_id_seq OWNED BY payment_result_data.id;


--
-- Name: product_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    name character varying(255),
    description character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    ancestry character varying(255),
    "position" integer,
    charge character varying(255),
    code character varying(255),
    weight integer
);


--
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- Name: product_charge_ranges; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_charge_ranges (
    id integer NOT NULL,
    product_category_id integer,
    from_price character varying(255),
    to_price character varying(255),
    charge character varying(255),
    deleted_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: product_charge_ranges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_charge_ranges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_charge_ranges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_charge_ranges_id_seq OWNED BY product_charge_ranges.id;


--
-- Name: product_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_images (
    id integer NOT NULL,
    product_id integer,
    upload_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    sort integer,
    color_option character varying(255)
);


--
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- Name: product_option_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_option_items (
    id integer NOT NULL,
    product_option_id integer,
    title character varying(255),
    amount integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: product_option_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_option_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_option_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_option_items_id_seq OWNED BY product_option_items.id;


--
-- Name: product_options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_options (
    id integer NOT NULL,
    product_id integer,
    title character varying(255),
    amount integer,
    price character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: product_options_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_options_id_seq OWNED BY product_options.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    title character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    product_category_id integer,
    product_type character varying(255),
    country_of_origin character varying(255),
    date_of_manufacture date,
    detail_info text,
    price character varying(255),
    amount integer,
    delivery_charge character varying(255),
    is_cash_on_arrival boolean,
    take_back_address text,
    is_public boolean,
    created_by_id integer,
    updated_by_id integer,
    deleted_at timestamp without time zone,
    slug character varying(255),
    supplier_id integer,
    sub_title character varying(255),
    is_crawled character varying(255),
    url character varying(255),
    currency character varying(255),
    brand character varying(255),
    domain character varying(255),
    options hstore,
    multi_price hstore,
    inventory hstore,
    size_table hstore,
    size_table_html text,
    categories character varying(255)[] DEFAULT '{}'::character varying[],
    category_str character varying(255),
    init_price numeric,
    is_sale character varying(255),
    breadcrumb character varying(255)[] DEFAULT '{}'::character varying[],
    measurements_table_html text,
    random_id character varying(255),
    keyword_arr character varying(255)[] DEFAULT '{}'::character varying[],
    sale_rate integer,
    delete_flag boolean DEFAULT false,
    image_width character varying(255),
    image_height character varying(255),
    store_location character varying(255),
    is_soldout boolean DEFAULT false,
    sale_price character varying(255),
    save_money character varying(255),
    disable_collection boolean DEFAULT false
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: profit_withdraw_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profit_withdraw_requests (
    id integer NOT NULL,
    encrypted_bank_name character varying(255),
    encrypted_account_number character varying(255),
    user_id integer,
    is_done boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    encrypted_account_owner character varying(255),
    withdraw_amount numeric,
    encrypted_tel_no_1 character varying(255),
    encrypted_tel_no_2 character varying(255),
    encrypted_tel_no_3 character varying(255)
);


--
-- Name: profit_withdraw_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE profit_withdraw_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profit_withdraw_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE profit_withdraw_requests_id_seq OWNED BY profit_withdraw_requests.id;


--
-- Name: profits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profits (
    id integer NOT NULL,
    product_id integer,
    user_id integer,
    orders_item_id integer,
    charge_rate character varying(255),
    price character varying(255),
    profit character varying(255),
    order_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    seller_profit character varying(255),
    collection_owner_id integer,
    collection_item_id integer,
    collection_profit character varying(255),
    collection_charge_rate character varying(255),
    final_profit character varying(255),
    is_cancel boolean,
    collection_id integer,
    profit_withdraw_request_id integer
);


--
-- Name: profits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE profits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE profits_id_seq OWNED BY profits.id;


--
-- Name: que_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE que_jobs (
    priority integer DEFAULT 100 NOT NULL,
    run_at timestamp with time zone DEFAULT now() NOT NULL,
    job_id bigint NOT NULL,
    job_class text NOT NULL,
    args json DEFAULT '[]'::json NOT NULL,
    error_count integer DEFAULT 0 NOT NULL,
    last_error text
);


--
-- Name: TABLE que_jobs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE que_jobs IS '2';


--
-- Name: que_jobs_job_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE que_jobs_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: que_jobs_job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE que_jobs_job_id_seq OWNED BY que_jobs.job_id;


--
-- Name: request_user_destroys; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE request_user_destroys (
    id integer NOT NULL,
    user_id integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: request_user_destroys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE request_user_destroys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: request_user_destroys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE request_user_destroys_id_seq OWNED BY request_user_destroys.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE reviews (
    id integer NOT NULL,
    title character varying(255),
    content character varying(255),
    product_id integer,
    user_id integer,
    deleted_at timestamp without time zone,
    rating integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE reviews_id_seq OWNED BY reviews.id;


--
-- Name: saved_money_rates; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE saved_money_rates (
    id integer NOT NULL,
    saved_money_rate numeric,
    "desc" text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: saved_money_rates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE saved_money_rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: saved_money_rates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE saved_money_rates_id_seq OWNED BY saved_money_rates.id;


--
-- Name: saved_moneys; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE saved_moneys (
    id integer NOT NULL,
    user_id integer,
    order_id integer,
    money integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    orders_item_id integer
);


--
-- Name: saved_moneys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE saved_moneys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: saved_moneys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE saved_moneys_id_seq OWNED BY saved_moneys.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: selected_interest_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE selected_interest_categories (
    id integer NOT NULL,
    product_category_id integer,
    normal_profile_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: selected_interest_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE selected_interest_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: selected_interest_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE selected_interest_categories_id_seq OWNED BY selected_interest_categories.id;


--
-- Name: selected_product_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE selected_product_categories (
    id integer NOT NULL,
    product_category_id integer,
    seller_profile_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: selected_product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE selected_product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: selected_product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE selected_product_categories_id_seq OWNED BY selected_product_categories.id;


--
-- Name: selected_talent_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE selected_talent_categories (
    id integer NOT NULL,
    normal_profile_id integer,
    talent_category_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: selected_talent_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE selected_talent_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: selected_talent_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE selected_talent_categories_id_seq OWNED BY selected_talent_categories.id;


--
-- Name: seletect_product_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE seletect_product_categories (
    id integer NOT NULL,
    product_category_id integer,
    seller_profile_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: seletect_product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seletect_product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seletect_product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE seletect_product_categories_id_seq OWNED BY seletect_product_categories.id;


--
-- Name: seller_profiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE seller_profiles (
    id integer NOT NULL,
    user_id integer,
    gender character varying(255),
    age character varying(255),
    site character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    facebook character varying(255),
    country character varying(255),
    business_registration_number character varying(255),
    business_representative character varying(255),
    company_name character varying(255),
    twitter character varying(255),
    description text,
    avatar_file_name character varying(255),
    avatar_content_type character varying(255),
    avatar_file_size integer,
    avatar_updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- Name: seller_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seller_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seller_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE seller_profiles_id_seq OWNED BY seller_profiles.id;


--
-- Name: shopping_mall_configs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE shopping_mall_configs (
    id integer NOT NULL,
    domain character varying(255),
    delivery_charge numeric,
    free_condition numeric,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    currency character varying(255),
    location character varying(255)
);


--
-- Name: shopping_mall_configs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shopping_mall_configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shopping_mall_configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shopping_mall_configs_id_seq OWNED BY shopping_mall_configs.id;


--
-- Name: suppliers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE suppliers (
    id integer NOT NULL,
    name character varying(255),
    description text,
    address text,
    take_back_address text,
    created_by_id integer,
    updated_by_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: suppliers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE suppliers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: suppliers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE suppliers_id_seq OWNED BY suppliers.id;


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying(255),
    tagger_id integer,
    tagger_type character varying(255),
    context character varying(128),
    created_at timestamp without time zone
);


--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying(255)
);


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: take_back_requests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE take_back_requests (
    id integer NOT NULL,
    orders_item_id integer,
    reason_type character varying(255),
    reason text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    image_1_file_name character varying(255),
    image_1_content_type character varying(255),
    image_1_file_size integer,
    image_1_updated_at timestamp without time zone,
    image_2_file_name character varying(255),
    image_2_content_type character varying(255),
    image_2_file_size integer,
    image_2_updated_at timestamp without time zone,
    image_3_file_name character varying(255),
    image_3_content_type character varying(255),
    image_3_file_size integer,
    image_3_updated_at timestamp without time zone
);


--
-- Name: take_back_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE take_back_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: take_back_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE take_back_requests_id_seq OWNED BY take_back_requests.id;


--
-- Name: talent_categories; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE talent_categories (
    id integer NOT NULL,
    name character varying(255),
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: talent_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE talent_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: talent_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE talent_categories_id_seq OWNED BY talent_categories.id;


--
-- Name: uploads; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE uploads (
    id integer NOT NULL,
    file_file_name character varying(255),
    file_content_type character varying(255),
    file_file_size integer,
    file_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE uploads_id_seq OWNED BY uploads.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying(255),
    failed_attempts integer DEFAULT 0,
    unlock_token character varying(255),
    locked_at timestamp without time zone,
    authentication_token character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id character varying(255),
    type character varying(255),
    user_type character varying(255),
    deleted_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE versions (
    id integer NOT NULL,
    item_type character varying(255) NOT NULL,
    item_id integer NOT NULL,
    event character varying(255) NOT NULL,
    whodunnit character varying(255),
    object text,
    created_at timestamp without time zone
);


--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE versions_id_seq OWNED BY versions.id;


--
-- Name: zipcodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE zipcodes (
    id integer NOT NULL,
    zipcode character varying(255),
    sido character varying(255),
    gugun character varying(255),
    dong character varying(255),
    ri character varying(255),
    bldg text,
    seq character varying(255),
    bunji character varying(255),
    address text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: zipcodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE zipcodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zipcodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE zipcodes_id_seq OWNED BY zipcodes.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY activities ALTER COLUMN id SET DEFAULT nextval('activities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_users ALTER COLUMN id SET DEFAULT nextval('admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY blog_categories ALTER COLUMN id SET DEFAULT nextval('blog_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY blogs ALTER COLUMN id SET DEFAULT nextval('blogs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY boards ALTER COLUMN id SET DEFAULT nextval('boards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY change_states ALTER COLUMN id SET DEFAULT nextval('change_states_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY collection_items ALTER COLUMN id SET DEFAULT nextval('collection_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY collections ALTER COLUMN id SET DEFAULT nextval('collections_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_card_companies ALTER COLUMN id SET DEFAULT nextval('credit_card_companies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY exchange_rates ALTER COLUMN id SET DEFAULT nextval('exchange_rates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY featured_collections ALTER COLUMN id SET DEFAULT nextval('featured_collections_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY featured_product_categories ALTER COLUMN id SET DEFAULT nextval('featured_product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY featured_products ALTER COLUMN id SET DEFAULT nextval('featured_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY follows ALTER COLUMN id SET DEFAULT nextval('follows_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY image_checks ALTER COLUMN id SET DEFAULT nextval('image_checks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY impressions ALTER COLUMN id SET DEFAULT nextval('impressions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY international_delivery_charges ALTER COLUMN id SET DEFAULT nextval('international_delivery_charges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY line_items ALTER COLUMN id SET DEFAULT nextval('line_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY normal_profiles ALTER COLUMN id SET DEFAULT nextval('normal_profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders_items ALTER COLUMN id SET DEFAULT nextval('orders_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_result_data ALTER COLUMN id SET DEFAULT nextval('payment_result_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_charge_ranges ALTER COLUMN id SET DEFAULT nextval('product_charge_ranges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_option_items ALTER COLUMN id SET DEFAULT nextval('product_option_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_options ALTER COLUMN id SET DEFAULT nextval('product_options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY profit_withdraw_requests ALTER COLUMN id SET DEFAULT nextval('profit_withdraw_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY profits ALTER COLUMN id SET DEFAULT nextval('profits_id_seq'::regclass);


--
-- Name: job_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY que_jobs ALTER COLUMN job_id SET DEFAULT nextval('que_jobs_job_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY request_user_destroys ALTER COLUMN id SET DEFAULT nextval('request_user_destroys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY reviews ALTER COLUMN id SET DEFAULT nextval('reviews_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY saved_money_rates ALTER COLUMN id SET DEFAULT nextval('saved_money_rates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY saved_moneys ALTER COLUMN id SET DEFAULT nextval('saved_moneys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY selected_interest_categories ALTER COLUMN id SET DEFAULT nextval('selected_interest_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY selected_product_categories ALTER COLUMN id SET DEFAULT nextval('selected_product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY selected_talent_categories ALTER COLUMN id SET DEFAULT nextval('selected_talent_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY seletect_product_categories ALTER COLUMN id SET DEFAULT nextval('seletect_product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY seller_profiles ALTER COLUMN id SET DEFAULT nextval('seller_profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shopping_mall_configs ALTER COLUMN id SET DEFAULT nextval('shopping_mall_configs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY suppliers ALTER COLUMN id SET DEFAULT nextval('suppliers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY take_back_requests ALTER COLUMN id SET DEFAULT nextval('take_back_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY talent_categories ALTER COLUMN id SET DEFAULT nextval('talent_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY uploads ALTER COLUMN id SET DEFAULT nextval('uploads_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY versions ALTER COLUMN id SET DEFAULT nextval('versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY zipcodes ALTER COLUMN id SET DEFAULT nextval('zipcodes_id_seq'::regclass);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: activities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY activities
    ADD CONSTRAINT activities_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: blog_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY blog_categories
    ADD CONSTRAINT blog_categories_pkey PRIMARY KEY (id);


--
-- Name: blogs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY blogs
    ADD CONSTRAINT blogs_pkey PRIMARY KEY (id);


--
-- Name: boards_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY boards
    ADD CONSTRAINT boards_pkey PRIMARY KEY (id);


--
-- Name: change_states_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY change_states
    ADD CONSTRAINT change_states_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: collection_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY collection_items
    ADD CONSTRAINT collection_items_pkey PRIMARY KEY (id);


--
-- Name: collections_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY collections
    ADD CONSTRAINT collections_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: credit_card_companies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY credit_card_companies
    ADD CONSTRAINT credit_card_companies_pkey PRIMARY KEY (id);


--
-- Name: exchange_rates_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY exchange_rates
    ADD CONSTRAINT exchange_rates_pkey PRIMARY KEY (id);


--
-- Name: featured_collections_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY featured_collections
    ADD CONSTRAINT featured_collections_pkey PRIMARY KEY (id);


--
-- Name: featured_product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY featured_product_categories
    ADD CONSTRAINT featured_product_categories_pkey PRIMARY KEY (id);


--
-- Name: featured_products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY featured_products
    ADD CONSTRAINT featured_products_pkey PRIMARY KEY (id);


--
-- Name: follows_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY follows
    ADD CONSTRAINT follows_pkey PRIMARY KEY (id);


--
-- Name: image_checks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY image_checks
    ADD CONSTRAINT image_checks_pkey PRIMARY KEY (id);


--
-- Name: impressions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY impressions
    ADD CONSTRAINT impressions_pkey PRIMARY KEY (id);


--
-- Name: international_delivery_charges_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY international_delivery_charges
    ADD CONSTRAINT international_delivery_charges_pkey PRIMARY KEY (id);


--
-- Name: line_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY line_items
    ADD CONSTRAINT line_items_pkey PRIMARY KEY (id);


--
-- Name: normal_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY normal_profiles
    ADD CONSTRAINT normal_profiles_pkey PRIMARY KEY (id);


--
-- Name: orders_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY orders_items
    ADD CONSTRAINT orders_items_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: payment_result_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY payment_result_data
    ADD CONSTRAINT payment_result_data_pkey PRIMARY KEY (id);


--
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- Name: product_charge_ranges_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_charge_ranges
    ADD CONSTRAINT product_charge_ranges_pkey PRIMARY KEY (id);


--
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- Name: product_option_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_option_items
    ADD CONSTRAINT product_option_items_pkey PRIMARY KEY (id);


--
-- Name: product_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_options
    ADD CONSTRAINT product_options_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: profit_withdraw_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY profit_withdraw_requests
    ADD CONSTRAINT profit_withdraw_requests_pkey PRIMARY KEY (id);


--
-- Name: profits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY profits
    ADD CONSTRAINT profits_pkey PRIMARY KEY (id);


--
-- Name: que_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY que_jobs
    ADD CONSTRAINT que_jobs_pkey PRIMARY KEY (priority, run_at, job_id);


--
-- Name: request_user_destroys_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY request_user_destroys
    ADD CONSTRAINT request_user_destroys_pkey PRIMARY KEY (id);


--
-- Name: reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: saved_money_rates_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY saved_money_rates
    ADD CONSTRAINT saved_money_rates_pkey PRIMARY KEY (id);


--
-- Name: saved_moneys_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY saved_moneys
    ADD CONSTRAINT saved_moneys_pkey PRIMARY KEY (id);


--
-- Name: selected_interest_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY selected_interest_categories
    ADD CONSTRAINT selected_interest_categories_pkey PRIMARY KEY (id);


--
-- Name: selected_product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY selected_product_categories
    ADD CONSTRAINT selected_product_categories_pkey PRIMARY KEY (id);


--
-- Name: selected_talent_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY selected_talent_categories
    ADD CONSTRAINT selected_talent_categories_pkey PRIMARY KEY (id);


--
-- Name: seletect_product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY seletect_product_categories
    ADD CONSTRAINT seletect_product_categories_pkey PRIMARY KEY (id);


--
-- Name: seller_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY seller_profiles
    ADD CONSTRAINT seller_profiles_pkey PRIMARY KEY (id);


--
-- Name: shopping_mall_configs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY shopping_mall_configs
    ADD CONSTRAINT shopping_mall_configs_pkey PRIMARY KEY (id);


--
-- Name: suppliers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY suppliers
    ADD CONSTRAINT suppliers_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: take_back_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY take_back_requests
    ADD CONSTRAINT take_back_requests_pkey PRIMARY KEY (id);


--
-- Name: talent_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY talent_categories
    ADD CONSTRAINT talent_categories_pkey PRIMARY KEY (id);


--
-- Name: uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY uploads
    ADD CONSTRAINT uploads_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: zipcodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY zipcodes
    ADD CONSTRAINT zipcodes_pkey PRIMARY KEY (id);


--
-- Name: controlleraction_ip_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX controlleraction_ip_index ON impressions USING btree (controller_name, action_name, ip_address);


--
-- Name: controlleraction_request_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX controlleraction_request_index ON impressions USING btree (controller_name, action_name, request_hash);


--
-- Name: controlleraction_session_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX controlleraction_session_index ON impressions USING btree (controller_name, action_name, session_hash);


--
-- Name: fk_followables; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fk_followables ON follows USING btree (followable_id, followable_type);


--
-- Name: fk_follows; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fk_follows ON follows USING btree (follower_id, follower_type);


--
-- Name: idx_ckeditor_assetable; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable ON ckeditor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_ckeditor_assetable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable_type ON ckeditor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: impressionable_type_message_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX impressionable_type_message_index ON impressions USING btree (impressionable_type, message, impressionable_id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_activities_on_owner_id_and_owner_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_activities_on_owner_id_and_owner_type ON activities USING btree (owner_id, owner_type);


--
-- Name: index_activities_on_recipient_id_and_recipient_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_activities_on_recipient_id_and_recipient_type ON activities USING btree (recipient_id, recipient_type);


--
-- Name: index_activities_on_trackable_id_and_trackable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_activities_on_trackable_id_and_trackable_type ON activities USING btree (trackable_id, trackable_type);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_email ON admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON admin_users USING btree (reset_password_token);


--
-- Name: index_blogs_on_blog_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_blogs_on_blog_category_id ON blogs USING btree (blog_category_id);


--
-- Name: index_boards_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_boards_on_user_id ON boards USING btree (user_id);


--
-- Name: index_change_states_on_orders_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_change_states_on_orders_item_id ON change_states USING btree (orders_item_id);


--
-- Name: index_collection_items_on_collection_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_collection_items_on_collection_id ON collection_items USING btree (collection_id);


--
-- Name: index_collection_items_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_collection_items_on_product_id ON collection_items USING btree (product_id);


--
-- Name: index_collection_items_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_collection_items_on_user_id ON collection_items USING btree (user_id);


--
-- Name: index_collections_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_collections_on_user_id ON collections USING btree (user_id);


--
-- Name: index_comments_on_commentable_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_comments_on_commentable_id ON comments USING btree (commentable_id);


--
-- Name: index_comments_on_commentable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_comments_on_commentable_type ON comments USING btree (commentable_type);


--
-- Name: index_comments_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_comments_on_user_id ON comments USING btree (user_id);


--
-- Name: index_exchange_rates_on_currency; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_exchange_rates_on_currency ON exchange_rates USING btree (currency);


--
-- Name: index_featured_products_on_featured_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_featured_products_on_featured_product_category_id ON featured_products USING btree (featured_product_category_id);


--
-- Name: index_impressions_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_impressions_on_user_id ON impressions USING btree (user_id);


--
-- Name: index_international_delivery_charges_on_location_and_weight; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_international_delivery_charges_on_location_and_weight ON international_delivery_charges USING btree (location, weight);


--
-- Name: index_line_items_on_collection_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_line_items_on_collection_id ON line_items USING btree (collection_id);


--
-- Name: index_line_items_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_line_items_on_product_id ON line_items USING btree (product_id);


--
-- Name: index_normal_profiles_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_normal_profiles_on_user_id ON normal_profiles USING btree (user_id);


--
-- Name: index_orders_items_on_collection_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_collection_id ON orders_items USING btree (collection_id);


--
-- Name: index_orders_items_on_collection_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_collection_item_id ON orders_items USING btree (collection_item_id);


--
-- Name: index_orders_items_on_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_order_id ON orders_items USING btree (order_id);


--
-- Name: index_orders_items_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_product_id ON orders_items USING btree (product_id);


--
-- Name: index_orders_items_on_product_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_product_option_id ON orders_items USING btree (product_option_id);


--
-- Name: index_orders_items_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_orders_items_on_user_id ON orders_items USING btree (user_id);


--
-- Name: index_product_categories_on_ancestry; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_categories_on_ancestry ON product_categories USING btree (ancestry);


--
-- Name: index_product_charge_ranges_on_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_charge_ranges_on_product_category_id ON product_charge_ranges USING btree (product_category_id);


--
-- Name: index_product_images_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_images_on_product_id ON product_images USING btree (product_id);


--
-- Name: index_product_images_on_upload_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_images_on_upload_id ON product_images USING btree (upload_id);


--
-- Name: index_product_option_items_on_product_option_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_option_items_on_product_option_id ON product_option_items USING btree (product_option_id);


--
-- Name: index_product_options_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_product_options_on_product_id ON product_options USING btree (product_id);


--
-- Name: index_products_on_brand; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_brand ON products USING btree (brand);


--
-- Name: index_products_on_breadcrumb; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_breadcrumb ON products USING gin (breadcrumb);


--
-- Name: index_products_on_category_str; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_category_str ON products USING btree (category_str);


--
-- Name: index_products_on_category_str_and_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_category_str_and_created_at ON products USING btree (category_str, created_at);


--
-- Name: index_products_on_category_str_and_init_price; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_category_str_and_init_price ON products USING btree (category_str, init_price);


--
-- Name: index_products_on_category_str_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_category_str_and_is_public ON products USING btree (category_str, is_public);


--
-- Name: index_products_on_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_created_at ON products USING btree (created_at);


--
-- Name: index_products_on_created_at_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_created_at_and_is_public ON products USING btree (created_at, is_public);


--
-- Name: index_products_on_created_at_and_is_public_and_category_str; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_created_at_and_is_public_and_category_str ON products USING btree (created_at, is_public, category_str);


--
-- Name: index_products_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_products_on_id ON products USING btree (id);


--
-- Name: index_products_on_id_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_id_and_is_public ON products USING btree (id, is_public);


--
-- Name: index_products_on_init_price; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_init_price ON products USING btree (init_price);


--
-- Name: index_products_on_init_price_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_init_price_and_is_public ON products USING btree (init_price, is_public);


--
-- Name: index_products_on_init_price_and_is_public_and_category_str; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_init_price_and_is_public_and_category_str ON products USING btree (init_price, is_public, category_str);


--
-- Name: index_products_on_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_category_id ON products USING btree (product_category_id);


--
-- Name: index_products_on_product_category_id_and_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_category_id_and_created_at ON products USING btree (product_category_id, created_at);


--
-- Name: index_products_on_product_category_id_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_product_category_id_and_is_public ON products USING btree (product_category_id, is_public);


--
-- Name: index_products_on_random_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_random_id ON products USING btree (random_id);


--
-- Name: index_products_on_slug; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_slug ON products USING btree (slug);


--
-- Name: index_products_on_supplier_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_supplier_id ON products USING btree (supplier_id);


--
-- Name: index_products_on_url_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_url_and_is_public ON products USING btree (url, is_public);


--
-- Name: index_products_on_url_and_is_public_and_deleted_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_on_url_and_is_public_and_deleted_at ON products USING btree (url, is_public, deleted_at);


--
-- Name: index_profits_on_collection_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_collection_id ON profits USING btree (collection_id);


--
-- Name: index_profits_on_collection_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_collection_item_id ON profits USING btree (collection_item_id);


--
-- Name: index_profits_on_collection_owner_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_collection_owner_id ON profits USING btree (collection_owner_id);


--
-- Name: index_profits_on_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_order_id ON profits USING btree (order_id);


--
-- Name: index_profits_on_orders_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_orders_item_id ON profits USING btree (orders_item_id);


--
-- Name: index_profits_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_product_id ON profits USING btree (product_id);


--
-- Name: index_profits_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_profits_on_user_id ON profits USING btree (user_id);


--
-- Name: index_request_user_destroys_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_request_user_destroys_on_user_id ON request_user_destroys USING btree (user_id);


--
-- Name: index_reviews_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_reviews_on_product_id ON reviews USING btree (product_id);


--
-- Name: index_reviews_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_reviews_on_user_id ON reviews USING btree (user_id);


--
-- Name: index_selected_interest_categories_on_normal_profile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_interest_categories_on_normal_profile_id ON selected_interest_categories USING btree (normal_profile_id);


--
-- Name: index_selected_interest_categories_on_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_interest_categories_on_product_category_id ON selected_interest_categories USING btree (product_category_id);


--
-- Name: index_selected_product_categories_on_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_product_categories_on_product_category_id ON selected_product_categories USING btree (product_category_id);


--
-- Name: index_selected_product_categories_on_seller_profile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_product_categories_on_seller_profile_id ON selected_product_categories USING btree (seller_profile_id);


--
-- Name: index_selected_talent_categories_on_normal_profile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_talent_categories_on_normal_profile_id ON selected_talent_categories USING btree (normal_profile_id);


--
-- Name: index_selected_talent_categories_on_talent_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_selected_talent_categories_on_talent_category_id ON selected_talent_categories USING btree (talent_category_id);


--
-- Name: index_seletect_product_categories_on_product_category_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_seletect_product_categories_on_product_category_id ON seletect_product_categories USING btree (product_category_id);


--
-- Name: index_seletect_product_categories_on_seller_profile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_seletect_product_categories_on_seller_profile_id ON seletect_product_categories USING btree (seller_profile_id);


--
-- Name: index_seller_profiles_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_seller_profiles_on_user_id ON seller_profiles USING btree (user_id);


--
-- Name: index_shopping_mall_configs_on_domain; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_shopping_mall_configs_on_domain ON shopping_mall_configs USING btree (domain);


--
-- Name: index_suppliers_on_created_by_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_suppliers_on_created_by_id ON suppliers USING btree (created_by_id);


--
-- Name: index_taggings_on_tag_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_taggings_on_tag_id ON taggings USING btree (tag_id);


--
-- Name: index_taggings_on_taggable_id_and_taggable_type_and_context; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_taggings_on_taggable_id_and_taggable_type_and_context ON taggings USING btree (taggable_id, taggable_type, context);


--
-- Name: index_users_on_authentication_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_authentication_token ON users USING btree (authentication_token);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_unlock_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_unlock_token ON users USING btree (unlock_token);


--
-- Name: index_versions_on_item_type_and_item_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_versions_on_item_type_and_item_id ON versions USING btree (item_type, item_id);


--
-- Name: index_zipcodes_on_dong; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_zipcodes_on_dong ON zipcodes USING btree (dong);


--
-- Name: index_zipcodes_on_dong_and_ri; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_zipcodes_on_dong_and_ri ON zipcodes USING btree (dong, ri);


--
-- Name: index_zipcodes_on_ri; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_zipcodes_on_ri ON zipcodes USING btree (ri);


--
-- Name: index_zipcodes_on_seq; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_zipcodes_on_seq ON zipcodes USING btree (seq);


--
-- Name: poly_ip_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX poly_ip_index ON impressions USING btree (impressionable_type, impressionable_id, ip_address);


--
-- Name: poly_request_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX poly_request_index ON impressions USING btree (impressionable_type, impressionable_id, request_hash);


--
-- Name: poly_session_index; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX poly_session_index ON impressions USING btree (impressionable_type, impressionable_id, session_hash);


--
-- Name: product_category_id_and_created_at_and_is_public; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX product_category_id_and_created_at_and_is_public ON products USING btree (product_category_id, created_at, is_public);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20130901043207');

INSERT INTO schema_migrations (version) VALUES ('20130901053218');

INSERT INTO schema_migrations (version) VALUES ('20130901053220');

INSERT INTO schema_migrations (version) VALUES ('20130906071808');

INSERT INTO schema_migrations (version) VALUES ('20130906074647');

INSERT INTO schema_migrations (version) VALUES ('20130906082244');

INSERT INTO schema_migrations (version) VALUES ('20130908042821');

INSERT INTO schema_migrations (version) VALUES ('20130909021801');

INSERT INTO schema_migrations (version) VALUES ('20130909064952');

INSERT INTO schema_migrations (version) VALUES ('20130909090307');

INSERT INTO schema_migrations (version) VALUES ('20130909110159');

INSERT INTO schema_migrations (version) VALUES ('20130909111933');

INSERT INTO schema_migrations (version) VALUES ('20130909113532');

INSERT INTO schema_migrations (version) VALUES ('20130910070135');

INSERT INTO schema_migrations (version) VALUES ('20130911022233');

INSERT INTO schema_migrations (version) VALUES ('20130911022809');

INSERT INTO schema_migrations (version) VALUES ('20130911022853');

INSERT INTO schema_migrations (version) VALUES ('20130911023147');

INSERT INTO schema_migrations (version) VALUES ('20130911035051');

INSERT INTO schema_migrations (version) VALUES ('20130911050410');

INSERT INTO schema_migrations (version) VALUES ('20130911052119');

INSERT INTO schema_migrations (version) VALUES ('20130911061539');

INSERT INTO schema_migrations (version) VALUES ('20130911062557');

INSERT INTO schema_migrations (version) VALUES ('20130911064306');

INSERT INTO schema_migrations (version) VALUES ('20130911065238');

INSERT INTO schema_migrations (version) VALUES ('20130911090807');

INSERT INTO schema_migrations (version) VALUES ('20130911090916');

INSERT INTO schema_migrations (version) VALUES ('20130912035120');

INSERT INTO schema_migrations (version) VALUES ('20130912071620');

INSERT INTO schema_migrations (version) VALUES ('20130930054739');

INSERT INTO schema_migrations (version) VALUES ('20131001061847');

INSERT INTO schema_migrations (version) VALUES ('20131007055353');

INSERT INTO schema_migrations (version) VALUES ('20131007061223');

INSERT INTO schema_migrations (version) VALUES ('20131007062800');

INSERT INTO schema_migrations (version) VALUES ('20131007071724');

INSERT INTO schema_migrations (version) VALUES ('20131007080516');

INSERT INTO schema_migrations (version) VALUES ('20131007085004');

INSERT INTO schema_migrations (version) VALUES ('20131007090152');

INSERT INTO schema_migrations (version) VALUES ('20131007090232');

INSERT INTO schema_migrations (version) VALUES ('20131007090413');

INSERT INTO schema_migrations (version) VALUES ('20131007090654');

INSERT INTO schema_migrations (version) VALUES ('20131008051616');

INSERT INTO schema_migrations (version) VALUES ('20131008065723');

INSERT INTO schema_migrations (version) VALUES ('20131014064505');

INSERT INTO schema_migrations (version) VALUES ('20131015070754');

INSERT INTO schema_migrations (version) VALUES ('20131015071044');

INSERT INTO schema_migrations (version) VALUES ('20131016052520');

INSERT INTO schema_migrations (version) VALUES ('20131019060708');

INSERT INTO schema_migrations (version) VALUES ('20131022094555');

INSERT INTO schema_migrations (version) VALUES ('20131023032141');

INSERT INTO schema_migrations (version) VALUES ('20131024034601');

INSERT INTO schema_migrations (version) VALUES ('20131025075433');

INSERT INTO schema_migrations (version) VALUES ('20131025093635');

INSERT INTO schema_migrations (version) VALUES ('20131026050005');

INSERT INTO schema_migrations (version) VALUES ('20131026055050');

INSERT INTO schema_migrations (version) VALUES ('20131026060956');

INSERT INTO schema_migrations (version) VALUES ('20131026062419');

INSERT INTO schema_migrations (version) VALUES ('20131026064242');

INSERT INTO schema_migrations (version) VALUES ('20131027045816');

INSERT INTO schema_migrations (version) VALUES ('20131027050656');

INSERT INTO schema_migrations (version) VALUES ('20131027050853');

INSERT INTO schema_migrations (version) VALUES ('20131028091105');

INSERT INTO schema_migrations (version) VALUES ('20131029055000');

INSERT INTO schema_migrations (version) VALUES ('20131029063029');

INSERT INTO schema_migrations (version) VALUES ('20131029063156');

INSERT INTO schema_migrations (version) VALUES ('20131113034257');

INSERT INTO schema_migrations (version) VALUES ('20131115032401');

INSERT INTO schema_migrations (version) VALUES ('20131116033023');

INSERT INTO schema_migrations (version) VALUES ('20131202140315');

INSERT INTO schema_migrations (version) VALUES ('20131204060219');

INSERT INTO schema_migrations (version) VALUES ('20131204071409');

INSERT INTO schema_migrations (version) VALUES ('20131205035733');

INSERT INTO schema_migrations (version) VALUES ('20131205045153');

INSERT INTO schema_migrations (version) VALUES ('20131205045310');

INSERT INTO schema_migrations (version) VALUES ('20131205052450');

INSERT INTO schema_migrations (version) VALUES ('20131205063807');

INSERT INTO schema_migrations (version) VALUES ('20131206082651');

INSERT INTO schema_migrations (version) VALUES ('20131206132028');

INSERT INTO schema_migrations (version) VALUES ('20131207073057');

INSERT INTO schema_migrations (version) VALUES ('20131209065119');

INSERT INTO schema_migrations (version) VALUES ('20131209072855');

INSERT INTO schema_migrations (version) VALUES ('20131209073429');

INSERT INTO schema_migrations (version) VALUES ('20131209074203');

INSERT INTO schema_migrations (version) VALUES ('20131209081417');

INSERT INTO schema_migrations (version) VALUES ('20131209081609');

INSERT INTO schema_migrations (version) VALUES ('20131209091512');

INSERT INTO schema_migrations (version) VALUES ('20131209091612');

INSERT INTO schema_migrations (version) VALUES ('20131209091930');

INSERT INTO schema_migrations (version) VALUES ('20131209093001');

INSERT INTO schema_migrations (version) VALUES ('20131209093219');

INSERT INTO schema_migrations (version) VALUES ('20131209095546');

INSERT INTO schema_migrations (version) VALUES ('20131210053522');

INSERT INTO schema_migrations (version) VALUES ('20131211043056');

INSERT INTO schema_migrations (version) VALUES ('20131211043920');

INSERT INTO schema_migrations (version) VALUES ('20131211044030');

INSERT INTO schema_migrations (version) VALUES ('20131213050912');

INSERT INTO schema_migrations (version) VALUES ('20131213051046');

INSERT INTO schema_migrations (version) VALUES ('20131213054557');

INSERT INTO schema_migrations (version) VALUES ('20131222052838');

INSERT INTO schema_migrations (version) VALUES ('20131223045900');

INSERT INTO schema_migrations (version) VALUES ('20131224081620');

INSERT INTO schema_migrations (version) VALUES ('20140102230835');

INSERT INTO schema_migrations (version) VALUES ('20140104052625');

INSERT INTO schema_migrations (version) VALUES ('20140105071549');

INSERT INTO schema_migrations (version) VALUES ('20140105072011');

INSERT INTO schema_migrations (version) VALUES ('20140105072304');

INSERT INTO schema_migrations (version) VALUES ('20140108033018');

INSERT INTO schema_migrations (version) VALUES ('20140108082723');

INSERT INTO schema_migrations (version) VALUES ('20140117033027');

INSERT INTO schema_migrations (version) VALUES ('20140212033109');

INSERT INTO schema_migrations (version) VALUES ('20140224045325');

INSERT INTO schema_migrations (version) VALUES ('20140226060249');

INSERT INTO schema_migrations (version) VALUES ('20140304053953');

INSERT INTO schema_migrations (version) VALUES ('20140305024304');

INSERT INTO schema_migrations (version) VALUES ('20140305030016');

INSERT INTO schema_migrations (version) VALUES ('20140305031405');

INSERT INTO schema_migrations (version) VALUES ('20140305031734');

INSERT INTO schema_migrations (version) VALUES ('20140305071937');

INSERT INTO schema_migrations (version) VALUES ('20140307064439');

INSERT INTO schema_migrations (version) VALUES ('20140307081249');

INSERT INTO schema_migrations (version) VALUES ('20140308075218');

INSERT INTO schema_migrations (version) VALUES ('20140320012725');

INSERT INTO schema_migrations (version) VALUES ('20140320063007');

INSERT INTO schema_migrations (version) VALUES ('20140322044319');

INSERT INTO schema_migrations (version) VALUES ('20140325053213');

INSERT INTO schema_migrations (version) VALUES ('20140325064048');

INSERT INTO schema_migrations (version) VALUES ('20140325093133');

INSERT INTO schema_migrations (version) VALUES ('20140327031243');

INSERT INTO schema_migrations (version) VALUES ('20140408051758');

INSERT INTO schema_migrations (version) VALUES ('20140408101047');

INSERT INTO schema_migrations (version) VALUES ('20140409041745');

INSERT INTO schema_migrations (version) VALUES ('20140414020332');

INSERT INTO schema_migrations (version) VALUES ('20140414020342');

INSERT INTO schema_migrations (version) VALUES ('20140414020354');

INSERT INTO schema_migrations (version) VALUES ('20140416030456');

INSERT INTO schema_migrations (version) VALUES ('20140416030753');

INSERT INTO schema_migrations (version) VALUES ('20140416032713');

INSERT INTO schema_migrations (version) VALUES ('20140416060530');

INSERT INTO schema_migrations (version) VALUES ('20140416060643');

INSERT INTO schema_migrations (version) VALUES ('20140416092800');

INSERT INTO schema_migrations (version) VALUES ('20140416092903');

INSERT INTO schema_migrations (version) VALUES ('20140417022720');

INSERT INTO schema_migrations (version) VALUES ('20140417031831');

INSERT INTO schema_migrations (version) VALUES ('20140417032738');

INSERT INTO schema_migrations (version) VALUES ('20140417063210');

INSERT INTO schema_migrations (version) VALUES ('20140417063721');

INSERT INTO schema_migrations (version) VALUES ('20140417063841');

INSERT INTO schema_migrations (version) VALUES ('20140418075307');

INSERT INTO schema_migrations (version) VALUES ('20140420072858');

INSERT INTO schema_migrations (version) VALUES ('20140420072921');

INSERT INTO schema_migrations (version) VALUES ('20140420075521');

INSERT INTO schema_migrations (version) VALUES ('20140421021014');

INSERT INTO schema_migrations (version) VALUES ('20140421032509');

INSERT INTO schema_migrations (version) VALUES ('20140421033838');

INSERT INTO schema_migrations (version) VALUES ('20140421054944');

INSERT INTO schema_migrations (version) VALUES ('20140421072151');

INSERT INTO schema_migrations (version) VALUES ('20140421075929');

INSERT INTO schema_migrations (version) VALUES ('20140422030628');

INSERT INTO schema_migrations (version) VALUES ('20140422032645');

INSERT INTO schema_migrations (version) VALUES ('20140422055515');

INSERT INTO schema_migrations (version) VALUES ('20140423022656');

INSERT INTO schema_migrations (version) VALUES ('20140423065621');

INSERT INTO schema_migrations (version) VALUES ('20140423073550');

INSERT INTO schema_migrations (version) VALUES ('20140424022513');

INSERT INTO schema_migrations (version) VALUES ('20140424030205');

INSERT INTO schema_migrations (version) VALUES ('20140505050505');

INSERT INTO schema_migrations (version) VALUES ('20140505052158');

INSERT INTO schema_migrations (version) VALUES ('20140510060539');

INSERT INTO schema_migrations (version) VALUES ('20140510060822');

INSERT INTO schema_migrations (version) VALUES ('20140510060850');

INSERT INTO schema_migrations (version) VALUES ('20140511144328');

INSERT INTO schema_migrations (version) VALUES ('20140512033420');

INSERT INTO schema_migrations (version) VALUES ('20140512043332');

INSERT INTO schema_migrations (version) VALUES ('20140513070732');

INSERT INTO schema_migrations (version) VALUES ('20140514142035');

INSERT INTO schema_migrations (version) VALUES ('20140527064941');

INSERT INTO schema_migrations (version) VALUES ('20140527074522');

INSERT INTO schema_migrations (version) VALUES ('20140602063855');

INSERT INTO schema_migrations (version) VALUES ('20140604032642');

INSERT INTO schema_migrations (version) VALUES ('20140610055027');

INSERT INTO schema_migrations (version) VALUES ('20140612031456');

INSERT INTO schema_migrations (version) VALUES ('20140612032615');

INSERT INTO schema_migrations (version) VALUES ('20140612032827');

INSERT INTO schema_migrations (version) VALUES ('20140612051841');

INSERT INTO schema_migrations (version) VALUES ('20140612061224');

INSERT INTO schema_migrations (version) VALUES ('20140620044957');

INSERT INTO schema_migrations (version) VALUES ('20140708071759');

INSERT INTO schema_migrations (version) VALUES ('20140708080354');

INSERT INTO schema_migrations (version) VALUES ('20140711080331');

INSERT INTO schema_migrations (version) VALUES ('20140711081006');

INSERT INTO schema_migrations (version) VALUES ('20140712061504');

INSERT INTO schema_migrations (version) VALUES ('20140712061537');

INSERT INTO schema_migrations (version) VALUES ('20140725023243');

INSERT INTO schema_migrations (version) VALUES ('20140725025027');

INSERT INTO schema_migrations (version) VALUES ('20140725030852');

INSERT INTO schema_migrations (version) VALUES ('20140725031633');

INSERT INTO schema_migrations (version) VALUES ('20140725051539');

INSERT INTO schema_migrations (version) VALUES ('20140727111317');

INSERT INTO schema_migrations (version) VALUES ('20140727111728');

INSERT INTO schema_migrations (version) VALUES ('20140727111849');

INSERT INTO schema_migrations (version) VALUES ('20140727113431');

INSERT INTO schema_migrations (version) VALUES ('20140727114048');

INSERT INTO schema_migrations (version) VALUES ('20140727114421');

INSERT INTO schema_migrations (version) VALUES ('20140728025106');

INSERT INTO schema_migrations (version) VALUES ('20140729132651');

INSERT INTO schema_migrations (version) VALUES ('20140730133300');

INSERT INTO schema_migrations (version) VALUES ('20140730134629');

INSERT INTO schema_migrations (version) VALUES ('20140803051453');

INSERT INTO schema_migrations (version) VALUES ('20140805024536');

INSERT INTO schema_migrations (version) VALUES ('20140807053100');

INSERT INTO schema_migrations (version) VALUES ('20140819044531');

INSERT INTO schema_migrations (version) VALUES ('20140819044708');

INSERT INTO schema_migrations (version) VALUES ('20140820092522');

INSERT INTO schema_migrations (version) VALUES ('20140820093830');

INSERT INTO schema_migrations (version) VALUES ('20140821025758');

INSERT INTO schema_migrations (version) VALUES ('20140902065822');

INSERT INTO schema_migrations (version) VALUES ('20140902065957');

INSERT INTO schema_migrations (version) VALUES ('20140902070222');

INSERT INTO schema_migrations (version) VALUES ('20140902082048');

INSERT INTO schema_migrations (version) VALUES ('20140903030823');

INSERT INTO schema_migrations (version) VALUES ('20140903085717');

INSERT INTO schema_migrations (version) VALUES ('20140909044118');

INSERT INTO schema_migrations (version) VALUES ('20140911071024');

INSERT INTO schema_migrations (version) VALUES ('20140911082210');

INSERT INTO schema_migrations (version) VALUES ('20140912090546');

INSERT INTO schema_migrations (version) VALUES ('20140912090622');

INSERT INTO schema_migrations (version) VALUES ('20141007061841');
