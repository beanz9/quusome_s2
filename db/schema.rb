# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140226060249) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "collection_items", force: true do |t|
    t.integer  "collection_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.datetime "deleted_at"
  end

  add_index "collection_items", ["collection_id"], name: "index_collection_items_on_collection_id", using: :btree
  add_index "collection_items", ["product_id"], name: "index_collection_items_on_product_id", using: :btree
  add_index "collection_items", ["user_id"], name: "index_collection_items_on_user_id", using: :btree

  create_table "collections", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "info"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "collection_items_count", default: 0
  end

  add_index "collections", ["user_id"], name: "index_collections_on_user_id", using: :btree

  create_table "comments", force: true do |t|
    t.string   "title",            limit: 50, default: ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.string   "role",                        default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "credit_card_companies", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "follows", force: true do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "impressions", force: true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "line_items", force: true do |t|
    t.integer  "product_id"
    t.integer  "quantity"
    t.decimal  "price"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_option_id"
    t.datetime "deleted_at"
    t.integer  "collection_item_id"
    t.integer  "collection_id"
  end

  add_index "line_items", ["collection_id"], name: "index_line_items_on_collection_id", using: :btree
  add_index "line_items", ["product_id"], name: "index_line_items_on_product_id", using: :btree

  create_table "normal_profiles", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender"
    t.string   "age"
    t.string   "country"
    t.string   "facebook"
    t.string   "twitter"
    t.text     "description"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "deleted_at"
  end

  add_index "normal_profiles", ["user_id"], name: "index_normal_profiles_on_user_id", using: :btree

  create_table "orders", force: true do |t|
    t.string   "recipient"
    t.string   "zipcode"
    t.text     "address"
    t.text     "detail_address"
    t.string   "mobile_phone_1"
    t.string   "mobile_phone_2"
    t.string   "mobile_phone_3"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "phone_3"
    t.text     "shipping_note"
    t.string   "payment_method"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "orders_items", force: true do |t|
    t.integer  "product_id"
    t.integer  "product_option_id"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
    t.integer  "quantity"
    t.decimal  "price"
    t.integer  "user_id"
    t.string   "state"
    t.integer  "collection_id"
    t.integer  "collection_item_id"
    t.string   "product_title"
    t.string   "product_option_title"
    t.string   "product_featured_small_image_url"
  end

  add_index "orders_items", ["collection_id"], name: "index_orders_items_on_collection_id", using: :btree
  add_index "orders_items", ["collection_item_id"], name: "index_orders_items_on_collection_item_id", using: :btree
  add_index "orders_items", ["order_id"], name: "index_orders_items_on_order_id", using: :btree
  add_index "orders_items", ["product_id"], name: "index_orders_items_on_product_id", using: :btree
  add_index "orders_items", ["product_option_id"], name: "index_orders_items_on_product_option_id", using: :btree
  add_index "orders_items", ["user_id"], name: "index_orders_items_on_user_id", using: :btree

  create_table "product_categories", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
    t.integer  "position"
    t.string   "charge"
  end

  add_index "product_categories", ["ancestry"], name: "index_product_categories_on_ancestry", using: :btree

  create_table "product_charge_ranges", force: true do |t|
    t.integer  "product_category_id"
    t.string   "from_price"
    t.string   "to_price"
    t.string   "charge"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_charge_ranges", ["product_category_id"], name: "index_product_charge_ranges_on_product_category_id", using: :btree

  create_table "product_images", force: true do |t|
    t.integer  "product_id"
    t.integer  "upload_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sort"
  end

  add_index "product_images", ["product_id"], name: "index_product_images_on_product_id", using: :btree
  add_index "product_images", ["upload_id"], name: "index_product_images_on_upload_id", using: :btree

  create_table "product_option_items", force: true do |t|
    t.integer  "product_option_id"
    t.string   "title"
    t.integer  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_option_items", ["product_option_id"], name: "index_product_option_items_on_product_option_id", using: :btree

  create_table "product_options", force: true do |t|
    t.integer  "product_id"
    t.string   "title"
    t.integer  "amount"
    t.string   "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_options", ["product_id"], name: "index_product_options_on_product_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_category_id"
    t.string   "product_type"
    t.string   "country_of_origin"
    t.date     "date_of_manufacture"
    t.text     "detail_info"
    t.string   "price"
    t.integer  "amount"
    t.string   "delivery_charge"
    t.boolean  "is_cash_on_arrival"
    t.text     "take_back_address"
    t.boolean  "is_public"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "deleted_at"
    t.string   "slug"
    t.integer  "supplier_id"
    t.string   "sub_title"
    t.string   "is_crawled"
    t.string   "url"
    t.string   "currency"
    t.string   "brand"
    t.string   "domain"
  end

  add_index "products", ["product_category_id"], name: "index_products_on_product_category_id", using: :btree
  add_index "products", ["slug"], name: "index_products_on_slug", using: :btree
  add_index "products", ["supplier_id"], name: "index_products_on_supplier_id", using: :btree

  create_table "profits", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "orders_item_id"
    t.string   "charge_rate"
    t.string   "price"
    t.string   "profit"
    t.integer  "order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "seller_profit"
    t.integer  "collection_owner_id"
    t.integer  "collection_item_id"
    t.string   "collection_profit"
    t.string   "collection_charge_rate"
    t.string   "final_profit"
    t.boolean  "is_cancel"
    t.integer  "collection_id"
  end

  add_index "profits", ["collection_id"], name: "index_profits_on_collection_id", using: :btree
  add_index "profits", ["collection_item_id"], name: "index_profits_on_collection_item_id", using: :btree
  add_index "profits", ["collection_owner_id"], name: "index_profits_on_collection_owner_id", using: :btree
  add_index "profits", ["order_id"], name: "index_profits_on_order_id", using: :btree
  add_index "profits", ["orders_item_id"], name: "index_profits_on_orders_item_id", using: :btree
  add_index "profits", ["product_id"], name: "index_profits_on_product_id", using: :btree
  add_index "profits", ["user_id"], name: "index_profits_on_user_id", using: :btree

  create_table "que_jobs", id: false, force: true do |t|
    t.integer  "priority",              default: 100, null: false
    t.datetime "run_at",                              null: false
    t.integer  "job_id",      limit: 8,               null: false
    t.text     "job_class",                           null: false
    t.json     "args",                  default: [],  null: false
    t.integer  "error_count",           default: 0,   null: false
    t.text     "last_error"
  end

  create_table "request_user_destroys", force: true do |t|
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "request_user_destroys", ["user_id"], name: "index_request_user_destroys_on_user_id", using: :btree

  create_table "reviews", force: true do |t|
    t.string   "title"
    t.string   "content"
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "deleted_at"
    t.integer  "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reviews", ["product_id"], name: "index_reviews_on_product_id", using: :btree
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id", using: :btree

  create_table "selected_interest_categories", force: true do |t|
    t.integer  "product_category_id"
    t.integer  "normal_profile_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "selected_interest_categories", ["normal_profile_id"], name: "index_selected_interest_categories_on_normal_profile_id", using: :btree
  add_index "selected_interest_categories", ["product_category_id"], name: "index_selected_interest_categories_on_product_category_id", using: :btree

  create_table "selected_product_categories", force: true do |t|
    t.integer  "product_category_id"
    t.integer  "seller_profile_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "selected_product_categories", ["product_category_id"], name: "index_selected_product_categories_on_product_category_id", using: :btree
  add_index "selected_product_categories", ["seller_profile_id"], name: "index_selected_product_categories_on_seller_profile_id", using: :btree

  create_table "selected_talent_categories", force: true do |t|
    t.integer  "normal_profile_id"
    t.integer  "talent_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "selected_talent_categories", ["normal_profile_id"], name: "index_selected_talent_categories_on_normal_profile_id", using: :btree
  add_index "selected_talent_categories", ["talent_category_id"], name: "index_selected_talent_categories_on_talent_category_id", using: :btree

  create_table "seletect_product_categories", force: true do |t|
    t.integer  "product_category_id"
    t.integer  "seller_profile_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "seletect_product_categories", ["product_category_id"], name: "index_seletect_product_categories_on_product_category_id", using: :btree
  add_index "seletect_product_categories", ["seller_profile_id"], name: "index_seletect_product_categories_on_seller_profile_id", using: :btree

  create_table "seller_profiles", force: true do |t|
    t.integer  "user_id"
    t.string   "gender"
    t.string   "age"
    t.string   "site"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "facebook"
    t.string   "country"
    t.string   "business_registration_number"
    t.string   "business_representative"
    t.string   "company_name"
    t.string   "twitter"
    t.text     "description"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "deleted_at"
  end

  add_index "seller_profiles", ["user_id"], name: "index_seller_profiles_on_user_id", using: :btree

  create_table "suppliers", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "address"
    t.text     "take_back_address"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "suppliers", ["created_by_id"], name: "index_suppliers_on_created_by_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  create_table "talent_categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "uploads", force: true do |t|
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_id"
    t.string   "type"
    t.string   "user_type"
    t.datetime "deleted_at"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "zipcodes", force: true do |t|
    t.string   "zipcode"
    t.string   "sido"
    t.string   "gugun"
    t.string   "dong"
    t.string   "ri"
    t.text     "bldg"
    t.string   "seq"
    t.string   "bunji"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "zipcodes", ["dong", "ri"], name: "index_zipcodes_on_dong_and_ri", using: :btree
  add_index "zipcodes", ["dong"], name: "index_zipcodes_on_dong", using: :btree
  add_index "zipcodes", ["ri"], name: "index_zipcodes_on_ri", using: :btree
  add_index "zipcodes", ["seq"], name: "index_zipcodes_on_seq", using: :btree

end
