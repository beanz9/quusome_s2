class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title
      t.string :content
      t.references :product, index: true
      t.references :user, index: true
      t.datetime :deleted_at
      t.integer :rating

      t.timestamps
    end
  end
end
