class ChangeDescriptionType < ActiveRecord::Migration
  def change
  	change_column :seller_profiles, :description, :text
  end
end
