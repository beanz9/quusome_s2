class AddBreadcrumbIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, :breadcrumb, using: 'gin'
  end
end
