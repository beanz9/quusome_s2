class AddColumnToNormalProfile < ActiveRecord::Migration
  def change
  	add_column :normal_profiles, :gender, :string
  	add_column :normal_profiles, :age, :string
  	add_column :normal_profiles, :country, :string
  end
end
