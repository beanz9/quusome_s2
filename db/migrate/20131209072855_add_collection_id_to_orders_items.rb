class AddCollectionIdToOrdersItems < ActiveRecord::Migration
  def change
    add_reference :orders_items, :collection, index: true
  end
end
