class AddSavedMoneyToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :saved_money, :decimal
    add_column :orders_items, :saved_money_rate, :decimal
  end
end
