class AddDeletedAtToSelectedTalentCategory < ActiveRecord::Migration
  def change
    add_column :selected_talent_categories, :deleted_at, :datetime
  end
end
