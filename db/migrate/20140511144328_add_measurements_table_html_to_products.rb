class AddMeasurementsTableHtmlToProducts < ActiveRecord::Migration
  def change
    add_column :products, :measurements_table_html, :text
  end
end
