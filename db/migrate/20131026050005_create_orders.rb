class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :recipient
      t.string :zipcode
      t.text :address
      t.text :detail_address
      t.string :mobile_phone_1
      t.string :mobile_phone_2
      t.string :mobile_phone_3
      t.string :phone_1
      t.string :phone_2
      t.string :phone_3
      t.text :shipping_note
      t.string :payment_method
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end
