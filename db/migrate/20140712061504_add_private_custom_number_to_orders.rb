class AddPrivateCustomNumberToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :private_custom_number, :string
  end
end
