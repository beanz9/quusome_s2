class CreateFeaturedCollections < ActiveRecord::Migration
  def change
    create_table :featured_collections do |t|
      t.integer :collection_id
      t.text :title
      t.text :description
      t.boolean :is_public
      t.datetime :start_date
      t.datetime :end_date
      t.integer :sort

      t.timestamps
    end
  end
end
