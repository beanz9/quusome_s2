class AddTelNo1ToProfitWithdrawRequests < ActiveRecord::Migration
  def change
    add_column :profit_withdraw_requests, :tel_no_1, :string
    add_column :profit_withdraw_requests, :tel_no_2, :string
    add_column :profit_withdraw_requests, :tel_no_3, :string
  end
end
