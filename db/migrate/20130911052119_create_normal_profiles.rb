class CreateNormalProfiles < ActiveRecord::Migration
  def change
    create_table :normal_profiles do |t|
      t.references :user, index: true

      t.timestamps
    end
  end
end
