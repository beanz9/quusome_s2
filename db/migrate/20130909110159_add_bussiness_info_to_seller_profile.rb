class AddBussinessInfoToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :business_registration_number, :string
    add_column :seller_profiles, :business_representative, :string
    add_column :seller_profiles, :company_name, :string
  end
end
