class AddAvatarColumnsToSellerProfile < ActiveRecord::Migration
  def change
  	add_attachment :seller_profiles, :avatar
  end
end
