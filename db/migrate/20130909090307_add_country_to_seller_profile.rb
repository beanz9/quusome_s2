class AddCountryToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :country, :string
  end
end
