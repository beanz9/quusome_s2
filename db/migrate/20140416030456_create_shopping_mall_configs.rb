class CreateShoppingMallConfigs < ActiveRecord::Migration
  def change
    create_table :shopping_mall_configs do |t|
      t.string :domain
      t.integer :delivery_charge
      t.integer :free_condition

      t.timestamps
    end
  end
end
