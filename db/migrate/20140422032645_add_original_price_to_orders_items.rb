class AddOriginalPriceToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :original_price, :decimal
  end
end
