class AddExchangeRateToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :exchange_rate, :decimal
  end
end
