class AddDescriptionToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :description, :string
  end
end
