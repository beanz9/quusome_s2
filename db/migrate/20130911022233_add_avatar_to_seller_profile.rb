class AddAvatarToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :avatar, :string
  end
end
