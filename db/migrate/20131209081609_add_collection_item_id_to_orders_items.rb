class AddCollectionItemIdToOrdersItems < ActiveRecord::Migration
  def change
    add_reference :orders_items, :collection_item, index: true
  end
end
