class AddInitPriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :init_price, :decimal
  end
end
