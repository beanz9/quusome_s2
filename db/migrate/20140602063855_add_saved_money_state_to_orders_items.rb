class AddSavedMoneyStateToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :saved_money_state, :string
  end
end
