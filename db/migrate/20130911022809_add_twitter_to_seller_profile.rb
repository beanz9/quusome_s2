class AddTwitterToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :twitter, :string
  end
end
