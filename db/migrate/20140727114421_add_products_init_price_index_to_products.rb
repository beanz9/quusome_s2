class AddProductsInitPriceIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:init_price]
  	add_index :products, [:category_str, :init_price]
  end
end
