class CreateSeletectProductCategories < ActiveRecord::Migration
  def change
    create_table :seletect_product_categories do |t|
      t.references :product_category, index: true
      t.references :seller_profile, index: true

      t.timestamps
    end
  end
end
