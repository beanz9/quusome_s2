class CreateProductOptionItems < ActiveRecord::Migration
  def change
    create_table :product_option_items do |t|
      t.references :product_option, index: true
      t.string :title
      t.integer :amount

      t.timestamps
    end
  end
end
