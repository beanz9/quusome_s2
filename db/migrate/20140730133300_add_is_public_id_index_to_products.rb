class AddIsPublicIdIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:id, :is_public]
  	add_index :products, [:category_str, :is_public]
  	add_index :products, [:created_at, :is_public]
  	add_index :products, [:product_category_id, :is_public]
  	add_index :products, [:product_category_id, :created_at, :is_public], :name => 'product_category_id_and_created_at_and_is_public'
  end
end
