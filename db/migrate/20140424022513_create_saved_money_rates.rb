class CreateSavedMoneyRates < ActiveRecord::Migration
  def change
    create_table :saved_money_rates do |t|
      t.decimal :saved_money_rate
      t.text :desc

      t.timestamps
    end
  end
end
