class AddSaltToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :_salt, :integer
    add_column :orders, :_iv, :integer
  end
end
