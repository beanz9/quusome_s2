class AddCollectionOwnerIdToProfits < ActiveRecord::Migration
  def change
    add_column :profits, :collection_owner_id, :integer
    add_index :profits, :collection_owner_id
  end
end
