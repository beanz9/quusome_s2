class AddUserIdToOrdersItem < ActiveRecord::Migration
  def change
    add_reference :orders_items, :user, index: true
  end
end
