class AddKeywordsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :keywords, :string, array: true, default: '{}'
  end
end
