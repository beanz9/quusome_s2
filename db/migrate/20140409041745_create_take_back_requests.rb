class CreateTakeBackRequests < ActiveRecord::Migration
  def change
    create_table :take_back_requests do |t|
      t.integer :orders_item_id
      t.string :reason_type
      t.text :reason

      t.timestamps
    end
  end
end
