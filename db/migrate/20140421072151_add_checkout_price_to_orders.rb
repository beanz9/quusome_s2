class AddCheckoutPriceToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :checkout_price, :decimal
  end
end
