class CreateCreditCardCompanies < ActiveRecord::Migration
  def change
    create_table :credit_card_companies do |t|
      t.string :name

      t.timestamps
    end
  end
end
