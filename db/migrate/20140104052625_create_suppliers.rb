class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.text :description
      t.text :address
      t.text :take_back_address
      t.integer :created_by_id
      t.integer :updated_by_id

      t.timestamps
    end
  end
end
