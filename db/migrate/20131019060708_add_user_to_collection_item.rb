class AddUserToCollectionItem < ActiveRecord::Migration
  def change
    add_reference :collection_items, :user, index: true
  end
end
