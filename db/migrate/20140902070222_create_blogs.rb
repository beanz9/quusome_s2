class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :body
      t.text :description
      t.boolean :is_public
      t.references :blog_category, index: true

      t.timestamps
    end
  end
end
