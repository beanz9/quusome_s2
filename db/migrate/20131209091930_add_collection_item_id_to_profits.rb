class AddCollectionItemIdToProfits < ActiveRecord::Migration
  def change
    add_reference :profits, :collection_item, index: true
  end
end
