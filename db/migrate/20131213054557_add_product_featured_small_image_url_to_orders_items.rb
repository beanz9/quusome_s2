class AddProductFeaturedSmallImageUrlToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :product_featured_small_image_url, :string
  end
end
