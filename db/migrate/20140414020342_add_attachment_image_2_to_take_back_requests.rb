class AddAttachmentImage2ToTakeBackRequests < ActiveRecord::Migration
  def self.up
    change_table :take_back_requests do |t|
      t.attachment :image_2
    end
  end

  def self.down
    drop_attached_file :take_back_requests, :image_2
  end
end
