class AddPositionToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :position, :integer
  end
end
