class CreateOrdersItems < ActiveRecord::Migration
  def change
    create_table :orders_items do |t|
      t.references :product, index: true
      # t.string :title
      # t.integer :product_category_id
      # t.string :product_type
      # t.string :country_of_origin
      # t.date :date_of_manufacture
      # t.text :detail_info
      # t.string :price
      # t.string :delivery_charge
      # t.boolean :is_cash_on_arrival
      # t.text :take_back_address
      t.references :product_option, index: true
      # t.string :product_option_title
      # t.string :product_option_price
      t.integer :created_by_id
      t.integer :update_by_id
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
