class AddSaleRateToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sale_rate, :integer
  end
end
