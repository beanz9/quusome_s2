class AddDeletedAtToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :deleted_at, :datetime
  end
end
