class AddInfoToProducts < ActiveRecord::Migration
  def change
    add_reference :products, :product_category, index: true
    add_column :products, :product_type, :string
    add_column :products, :country_of_origin, :string
    add_column :products, :date_of_manufacture, :datetime
    add_column :products, :detail_info, :text
    add_column :products, :price, :string
    add_column :products, :amount, :integer
    add_column :products, :delivery_charge, :string
    add_column :products, :is_cash_on_arrival, :boolean
    add_column :products, :take_back_address, :text
    add_column :products, :is_public, :boolean
  end
end
