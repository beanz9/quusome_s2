class AddProductsBrandIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, :brand
  end
end
