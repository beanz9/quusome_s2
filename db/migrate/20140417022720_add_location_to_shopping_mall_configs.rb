class AddLocationToShoppingMallConfigs < ActiveRecord::Migration
  def change
    add_column :shopping_mall_configs, :location, :string
  end
end
