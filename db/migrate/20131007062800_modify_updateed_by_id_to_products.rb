class ModifyUpdateedByIdToProducts < ActiveRecord::Migration
  def change
  	rename_column :products, :updateed_by_id, :updated_by_id
  end
end
