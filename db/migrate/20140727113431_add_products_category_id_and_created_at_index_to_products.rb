class AddProductsCategoryIdAndCreatedAtIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:product_category_id, :created_at]
  end
end
