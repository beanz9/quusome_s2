class AddCodeToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :code, :string
  end
end
