class AddDeletedAtToBoards < ActiveRecord::Migration
  def change
    add_column :boards, :deleted_at, :datetime
  end
end
