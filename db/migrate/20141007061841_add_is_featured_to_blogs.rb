class AddIsFeaturedToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :is_featured, :boolean, :default => false
  end
end
