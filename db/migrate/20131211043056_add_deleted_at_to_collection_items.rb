class AddDeletedAtToCollectionItems < ActiveRecord::Migration
  def change
    add_column :collection_items, :deleted_at, :datetime
  end
end
