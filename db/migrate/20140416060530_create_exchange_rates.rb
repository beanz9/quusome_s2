class CreateExchangeRates < ActiveRecord::Migration
  def change
    create_table :exchange_rates do |t|
      t.string :currency
      t.integer :price

      t.timestamps
    end
  end
end
