class AddFeaturedProductCategoryIdToFeaturedProducts < ActiveRecord::Migration
  def change
    add_reference :featured_products, :featured_product_category, index: true
  end
end
