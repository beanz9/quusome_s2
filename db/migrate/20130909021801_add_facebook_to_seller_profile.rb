class AddFacebookToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :facebook, :string
  end
end
