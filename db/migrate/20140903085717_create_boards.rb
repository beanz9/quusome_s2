class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :title
      t.text :body
      t.references :user, index: true
      t.string :type

      t.timestamps
    end
  end
end
