class CreateFeaturedProducts < ActiveRecord::Migration
  def change
    create_table :featured_products do |t|
      t.integer :product_id
      t.boolean :is_public
      t.datetime :start_date
      t.datetime :end_date
      t.integer :sort
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
