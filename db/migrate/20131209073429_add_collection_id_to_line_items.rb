class AddCollectionIdToLineItems < ActiveRecord::Migration
  def change
    add_reference :line_items, :collection, index: true
  end
end
