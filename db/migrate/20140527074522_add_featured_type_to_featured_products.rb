class AddFeaturedTypeToFeaturedProducts < ActiveRecord::Migration
  def change
    add_column :featured_products, :featured_type, :string
  end
end
