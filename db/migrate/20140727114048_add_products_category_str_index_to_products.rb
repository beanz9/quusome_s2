class AddProductsCategoryStrIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:category_str]
  	add_index :products, [:category_str, :created_at]
  end
end
