class AddOrigianlExchangePriceToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :original_exchange_price, :decimal
  end
end
