class AddOptionsToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :options, :hstore
  end
end
