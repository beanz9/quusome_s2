class CreateProfits < ActiveRecord::Migration
  def change
    create_table :profits do |t|
      t.references :product, index: true
      t.references :user, index: true
      t.references :orders_item, index: true
      t.string :charge_rate
      t.string :price
      t.string :profit
      t.references :order, index: true

      t.timestamps
    end
  end
end
