class AddCollectionChargeRateToProfits < ActiveRecord::Migration
  def change
    add_column :profits, :collection_charge_rate, :string
  end
end
