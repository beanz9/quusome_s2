class ChangeFreeConditionForShoppingMallConfigs < ActiveRecord::Migration
  def change
  	change_table :shopping_mall_configs do |t|
  		t.change :free_condition, :decimal
  	end
  end
end
