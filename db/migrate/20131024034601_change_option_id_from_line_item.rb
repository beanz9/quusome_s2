class ChangeOptionIdFromLineItem < ActiveRecord::Migration
  def change
  	rename_column :line_items, :option_id, :product_option_id
  end
end
