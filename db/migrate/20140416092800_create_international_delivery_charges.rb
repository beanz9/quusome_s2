class CreateInternationalDeliveryCharges < ActiveRecord::Migration
  def change
    create_table :international_delivery_charges do |t|
      t.string :location
      t.integer :weight
      t.integer :price

      t.timestamps
    end
  end
end
