class AddCategoriesToProduct < ActiveRecord::Migration
  def change
    add_column :products, :categories, :string, array: true, default: []
  end
end
