class AddDeletedAtToNormalProfile < ActiveRecord::Migration
  def change
    add_column :normal_profiles, :deleted_at, :datetime
  end
end
