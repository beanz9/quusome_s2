class AddDeleteFlagToProducts < ActiveRecord::Migration
  def change
    add_column :products, :delete_flag, :boolean, default: false
  end
end
