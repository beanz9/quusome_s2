class AddCurrencyToShoppingMallConfigs < ActiveRecord::Migration
  def change
    add_column :shopping_mall_configs, :currency, :string
  end
end
