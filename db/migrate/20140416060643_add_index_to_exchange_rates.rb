class AddIndexToExchangeRates < ActiveRecord::Migration
  def change
  	add_index :exchange_rates, :currency
  end
end
