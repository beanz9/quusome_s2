class CreateImageChecks < ActiveRecord::Migration
  def change
    create_table :image_checks do |t|
      t.integer :product_id
      t.string :url

      t.timestamps
    end
  end
end
