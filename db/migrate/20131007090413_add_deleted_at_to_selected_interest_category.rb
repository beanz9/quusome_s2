class AddDeletedAtToSelectedInterestCategory < ActiveRecord::Migration
  def change
    add_column :selected_interest_categories, :deleted_at, :datetime
  end
end
