class AddImageSizeToProducts < ActiveRecord::Migration
  def change
    add_column :products, :image_width, :string
    add_column :products, :image_height, :string
  end
end
