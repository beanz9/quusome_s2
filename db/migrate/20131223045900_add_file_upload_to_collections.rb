class AddFileUploadToCollections < ActiveRecord::Migration
  def self.up
    add_attachment :collections, :file
  end

  def self.down
    remove_attachment :collections, :file
  end
end
