class AddIndexToInternationalDeliveryCharge < ActiveRecord::Migration
  def change
  	add_index :international_delivery_charges, [:location, :weight]
  end
end
