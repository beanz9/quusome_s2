class AddScrapyItems < ActiveRecord::Migration
  def change
  	add_column :products, :sub_title, :string
  	add_column :products, :is_crawled, :string
  	add_column :products, :url, :string
  	add_column :products, :currency, :string
  	add_column :products, :brand, :string
  	add_column :products, :domain, :string
  end

end
