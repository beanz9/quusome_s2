class CreateTalentCategories < ActiveRecord::Migration
  def change
    create_table :talent_categories do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
