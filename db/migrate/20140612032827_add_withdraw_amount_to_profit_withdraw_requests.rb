class AddWithdrawAmountToProfitWithdrawRequests < ActiveRecord::Migration
  def change
    add_column :profit_withdraw_requests, :withdraw_amount, :decimal
  end
end
