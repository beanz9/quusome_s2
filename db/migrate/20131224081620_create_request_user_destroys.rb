class CreateRequestUserDestroys < ActiveRecord::Migration
  def change
    create_table :request_user_destroys do |t|
      t.references :user, index: true
      t.text :description

      t.timestamps
    end
  end
end
