class AddBreadcrumbToProducts < ActiveRecord::Migration
  def change
    add_column :products, :breadcrumb, :string, array: true, default: '{}'
  end
end
