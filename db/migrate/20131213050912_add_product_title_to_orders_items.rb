class AddProductTitleToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :product_title, :string
  end
end
