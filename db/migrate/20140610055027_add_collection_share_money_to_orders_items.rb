class AddCollectionShareMoneyToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :collection_share_money, :decimal
  end
end
