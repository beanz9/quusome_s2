class ChangeDateOfManufactureInProduct < ActiveRecord::Migration
  def change
  	change_column :products, :date_of_manufacture, :date
  end
end
