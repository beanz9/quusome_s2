class AddStateToOrdersItem < ActiveRecord::Migration
  def change
    add_column :orders_items, :state, :string
  end
end
