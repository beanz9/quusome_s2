class AddIsSoldoutToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_soldout, :boolean, :default => false
  end
end
