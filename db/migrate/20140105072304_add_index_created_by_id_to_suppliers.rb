class AddIndexCreatedByIdToSuppliers < ActiveRecord::Migration
  def change
  	add_index :suppliers, :created_by_id
  end
end
