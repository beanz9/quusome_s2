class AddSortToProductImages < ActiveRecord::Migration
  def change
    add_column :product_images, :sort, :integer
  end
end
