class AddAccountOwnerToProfitWithdrawRequests < ActiveRecord::Migration
  def change
    add_column :profit_withdraw_requests, :account_owner, :string
  end
end
