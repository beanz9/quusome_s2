class CreateProductOptions < ActiveRecord::Migration
  def change
    create_table :product_options do |t|
      t.references :product, index: true
      t.string :title
      t.integer :amount
      t.string :price

      t.timestamps
    end
  end
end
