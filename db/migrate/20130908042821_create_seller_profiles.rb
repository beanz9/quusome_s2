class CreateSellerProfiles < ActiveRecord::Migration
  def change
    create_table :seller_profiles do |t|
      t.references :user, index: true
      t.string :sex
      t.string :age
      t.string :site

      t.timestamps
    end
  end
end
