class AddCustomTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :custom_type, :string
  end
end
