class RenameProfitWithdrawRequestsColumns < ActiveRecord::Migration
  def change
  	rename_column :profit_withdraw_requests, :bank_name, :encrypted_bank_name
  	rename_column :profit_withdraw_requests, :account_number, :encrypted_account_number
  	rename_column :profit_withdraw_requests, :account_owner, :encrypted_account_owner
  	rename_column :profit_withdraw_requests, :tel_no_1, :encrypted_tel_no_1
  	rename_column :profit_withdraw_requests, :tel_no_2, :encrypted_tel_no_2
  	rename_column :profit_withdraw_requests, :tel_no_3, :encrypted_tel_no_3
  end
end
