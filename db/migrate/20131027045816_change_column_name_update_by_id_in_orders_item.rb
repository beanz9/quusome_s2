class ChangeColumnNameUpdateByIdInOrdersItem < ActiveRecord::Migration
  def change
  	rename_column :orders_items, :update_by_id, :updated_by_id
  end
end
