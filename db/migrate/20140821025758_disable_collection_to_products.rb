class DisableCollectionToProducts < ActiveRecord::Migration
  def change
  	add_column :products, :disable_collection, :boolean, :default => false
  end
end
