class AddOrdersItemIdToSavedMoney < ActiveRecord::Migration
  def change
    add_column :saved_moneys, :orders_item_id, :integer
  end
end
