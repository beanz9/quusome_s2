class AddCollectionIdToProfits < ActiveRecord::Migration
  def change
    add_reference :profits, :collection, index: true
  end
end
