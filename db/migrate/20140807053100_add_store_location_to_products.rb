class AddStoreLocationToProducts < ActiveRecord::Migration
  def change
    add_column :products, :store_location, :string
  end
end
