class CreateZipcodes < ActiveRecord::Migration
  def change
    create_table :zipcodes do |t|
      t.string :zipcode
      t.string :sido
      t.string :gugun
      t.string :dong
      t.string :ri
      t.text :bldg
      t.string :seq
      t.string :bunji
      t.text :address

      t.timestamps
    end

    add_index :zipcodes, :dong
    add_index :zipcodes, :ri
    add_index :zipcodes, :seq
    add_index :zipcodes, [:dong, :ri]
  end

  
end
