class RemoveAvatarFromSellerProfile < ActiveRecord::Migration
  def change
    remove_column :seller_profiles, :avatar, :string
  end
end
