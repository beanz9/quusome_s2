class RenameKeywordsFromProducts < ActiveRecord::Migration
  def change
  	rename_column :products, :keywords, :keyword_arr
  end
end
