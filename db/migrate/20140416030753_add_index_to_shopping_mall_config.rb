class AddIndexToShoppingMallConfig < ActiveRecord::Migration
  def change
  	add_index :shopping_mall_configs, :domain
  end
end
