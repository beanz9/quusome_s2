class AddOriginalPriceToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :original_price, :decimal
  end
end
