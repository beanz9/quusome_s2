class AddRandomIdIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, :random_id
  end
end
