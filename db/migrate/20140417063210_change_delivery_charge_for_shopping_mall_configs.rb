class ChangeDeliveryChargeForShoppingMallConfigs < ActiveRecord::Migration
  def change
  	change_table :shopping_mall_configs do |t|
  		t.change :delivery_charge, :decimal
  	end
  end
end
