class AddProductsIdIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, :id,                :unique => true
  end
end
