class CreateProfitWithdrawRequests < ActiveRecord::Migration
  def change
    create_table :profit_withdraw_requests do |t|
      t.string :bank_name
      t.string :account_number
      t.integer :user_id
      t.boolean :is_done

      t.timestamps
    end
  end
end
