class AddMoreInfoToPaymentResultData < ActiveRecord::Migration
  def change
    add_column :payment_result_data, :order_id, :integer
    add_column :payment_result_data, :is_success, :boolean
  end
end
