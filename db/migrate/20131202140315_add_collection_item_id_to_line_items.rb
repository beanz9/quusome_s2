class AddCollectionItemIdToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :collection_item_id, :integer
  end
end
