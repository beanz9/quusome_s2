class AddUrlIsPublicDeletedAtIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:url, :is_public, :deleted_at]
  	add_index :products, [:url, :is_public]
  end
end
