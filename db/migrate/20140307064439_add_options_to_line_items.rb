class AddOptionsToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :options, :hstore, array: true
  end
end
