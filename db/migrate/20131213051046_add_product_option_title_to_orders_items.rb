class AddProductOptionTitleToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :product_option_title, :string
  end
end
