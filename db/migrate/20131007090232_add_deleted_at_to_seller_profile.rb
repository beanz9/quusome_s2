class AddDeletedAtToSellerProfile < ActiveRecord::Migration
  def change
    add_column :seller_profiles, :deleted_at, :datetime
  end
end
