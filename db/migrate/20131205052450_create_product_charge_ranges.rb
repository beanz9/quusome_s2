class CreateProductChargeRanges < ActiveRecord::Migration
  def change
    create_table :product_charge_ranges do |t|
      t.references :product_category, index: true
      t.string :from_price
      t.string :to_price
      t.string :charge
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
