class AddSortNumToBlogCategories < ActiveRecord::Migration
  def change
    add_column :blog_categories, :sort_num, :integer
  end
end
