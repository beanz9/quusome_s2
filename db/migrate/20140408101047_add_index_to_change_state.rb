class AddIndexToChangeState < ActiveRecord::Migration
  def change
  	add_index :change_states, :orders_item_id
  end
end
