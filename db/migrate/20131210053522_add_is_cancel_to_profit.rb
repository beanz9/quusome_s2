class AddIsCancelToProfit < ActiveRecord::Migration
  def change
    add_column :profits, :is_cancel, :boolean
  end
end
