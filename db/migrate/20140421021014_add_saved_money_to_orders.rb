class AddSavedMoneyToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :saved_money, :decimal
  end
end
