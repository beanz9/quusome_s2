class AddWeightToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :weight, :integer
  end
end
