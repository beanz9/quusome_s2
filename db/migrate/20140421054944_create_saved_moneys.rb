class CreateSavedMoneys < ActiveRecord::Migration
  def change
    create_table :saved_moneys do |t|
      t.integer :user_id
      t.integer :order_id
      t.integer :money

      t.timestamps
    end
  end
end
