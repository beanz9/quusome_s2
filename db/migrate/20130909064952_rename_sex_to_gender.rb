class RenameSexToGender < ActiveRecord::Migration
  def change
  	rename_column :seller_profiles, :sex, :gender
  end
end
