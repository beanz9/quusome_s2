class CreatePaymentResultData < ActiveRecord::Migration
  def change
    create_table :payment_result_data do |t|
      t.string :site_cd
      t.string :req_tx
      t.string :use_pay_method
      t.string :bSucc
      t.string :panc_mod_mny
      t.string :panc_rem_mny
      t.string :mod_type
      t.string :amount
      t.string :res_cd
      t.text :res_msg
      t.string :ordr_idxx
      t.string :tno
      t.string :good_mny
      t.text :good_name
      t.string :buyr_name
      t.string :buyr_tel1
      t.string :buyr_tel2
      t.string :buyr_mail
      t.string :app_time
      t.string :card_cd
      t.string :card_name
      t.string :noinf
      t.string :app_no
      t.string :quota
      t.string :partcanc_yn
      t.string :card_bin_type_01
      t.string :card_bin_type_02
      t.string :bank_name
      t.string :bank_code
      t.string :bankname
      t.string :depositor
      t.string :account
      t.string :va_date
      t.string :pnt_issue
      t.string :pnt_app_time
      t.string :pnt_app_no
      t.string :pnt_amount
      t.string :add_pnt
      t.string :use_pnt
      t.string :rsv_pnt
      t.string :commid
      t.string :mobile_no
      t.string :tk_van_code
      t.string :tk_app_no
      t.string :cash_yn
      t.string :cash_authno
      t.string :cash_tr_code
      t.string :cash_id_info
      t.string :escw_yn
      t.string :deli_term
      t.string :bask_cntx
      t.text :good_info
      t.string :rcvr_name
      t.string :rcvr_tel1
      t.string :rcvr_tel2
      t.string :rcvr_mail
      t.string :rcvr_zipx
      t.string :rcvr_add1
      t.string :rcvr_add2

      t.timestamps
    end
  end
end
