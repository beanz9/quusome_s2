class AddMultiPriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :multi_price, :hstore
  end
end
