class AddAvatarToNormalProfile < ActiveRecord::Migration
  def change
  	add_attachment :normal_profiles, :avatar
  end
end
