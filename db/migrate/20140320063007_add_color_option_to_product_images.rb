class AddColorOptionToProductImages < ActiveRecord::Migration
  def change
    add_column :product_images, :color_option, :string
  end
end
