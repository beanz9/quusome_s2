class AddChargeToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :charge, :string
  end
end
