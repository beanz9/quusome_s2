class AddMoreInfoToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :exchange_rate, :decimal
    add_column :orders_items, :location, :string
    add_column :orders_items, :currency, :string
    add_column :orders_items, :delivery_charge, :decimal
    add_column :orders_items, :international_delivery_charge, :decimal
    add_column :orders_items, :charge, :decimal
  end
end
