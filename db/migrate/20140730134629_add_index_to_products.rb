class AddIndexToProducts < ActiveRecord::Migration
  def change
  	add_index :products, [:init_price, :is_public]
  	add_index :products, [:init_price, :is_public, :category_str]
  	add_index :products, [:created_at, :is_public, :category_str]
  end
end
