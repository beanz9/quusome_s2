class CreateSelectedTalentCategories < ActiveRecord::Migration
  def change
    create_table :selected_talent_categories do |t|
      t.references :normal_profile, index: true
      t.references :talent_category, index: true

      t.timestamps
    end
  end
end
