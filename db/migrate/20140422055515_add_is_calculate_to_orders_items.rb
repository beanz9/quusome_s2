class AddIsCalculateToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :is_calculate, :boolean, :default => false
  end
end
