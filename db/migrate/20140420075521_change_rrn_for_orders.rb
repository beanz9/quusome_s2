class ChangeRrnForOrders < ActiveRecord::Migration
  def change
  	rename_column :orders, :rrn_1, :encrypted_rrn_1
  	rename_column :orders, :rrn_2, :encrypted_rrn_2
  end
end
