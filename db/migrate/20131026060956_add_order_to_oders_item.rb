class AddOrderToOdersItem < ActiveRecord::Migration
  def change
    add_reference :orders_items, :order, index: true
  end
end
