class AddUsedSavedMoneyToOrdersItems < ActiveRecord::Migration
  def change
    add_column :orders_items, :used_saved_money, :decimal
  end
end
