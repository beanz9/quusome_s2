class CreateFeaturedProductCategories < ActiveRecord::Migration
  def change
    create_table :featured_product_categories do |t|
      t.string :name
      t.integer :sort_num
      t.boolean :is_public

      t.timestamps
    end
  end
end
