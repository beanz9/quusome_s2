class AddPriceToOrdersItem < ActiveRecord::Migration
  def change
    add_column :orders_items, :price, :decimal
  end
end
