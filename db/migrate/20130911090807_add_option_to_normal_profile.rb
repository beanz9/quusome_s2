class AddOptionToNormalProfile < ActiveRecord::Migration
  def change
    add_column :normal_profiles, :facebook, :string
    add_column :normal_profiles, :twitter, :string
    add_column :normal_profiles, :description, :text
  end
end
