class CreateSelectedInterestCategories < ActiveRecord::Migration
  def change
    create_table :selected_interest_categories do |t|
      t.references :product_category, index: true
      t.references :normal_profile, index: true

      t.timestamps
    end
  end
end
