class CreateChangeStates < ActiveRecord::Migration
  def change
    create_table :change_states do |t|
      t.string :state
      t.string :state_name
      t.text :desc
      t.integer :orders_item_id

      t.timestamps
    end
  end
end
