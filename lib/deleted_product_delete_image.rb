class DeletedProductDeleteImage

	def self.execute
		root = Rails.root.to_s
		Product.only_deleted.find_each do |product|
			p product.id
			
			product_images = product.product_images
			product_images.each do |pi|
				thumb_path = root + "/public" + pi.upload.file.url(:thumb).split('?')[0]
				original_path = root + "/public" + pi.upload.file.url(:original).split('?')[0]
				medium_path = root + "/public" + pi.upload.file.url(:medium).split('?')[0]
				rootshop_index = root + "/public" + pi.upload.file.url(:rootshop_index).split('?')[0]

				if File.exist?(thumb_path)
					p "thumb_path"
					File.delete(thumb_path)
				end

				if File.exist?(original_path)
					p "original_path"
					File.delete(original_path)
				end
				
				if File.exist?(medium_path)
					p "medium_path"
					File.delete(medium_path)
				end

				if File.exist?(rootshop_index)
					p "rootshop_index"
					File.delete(rootshop_index)
				end
			end
		end
	end
end