class ZipcodeImport 

	def self.load_save
		xls = Roo::Spreadsheet.open("#{Rails.root}/db/zipcode/20140314_zipcode.xls")
		
		xls.sheet(0).each_with_index do |row, i|
			if i > 1
				Zipcode.create({
					:zipcode => row[0],
					:seq => row[1],
					:sido => row[2],
					:gugun => row[3],
					:dong => row[4],
					:ri => row[5],
					:bunji => row[7],
					:bldg => row[8],
					:address => row[10]
				})

			end
		end
	end
	
end