class OrderState


	# def confirm(params)
	# 	update_state("order_confirm", params)
	# end

	def cancel(params)
		update_state("order_cancel", params)
	end

	def local_order(params)
		update_state("local_order", params)
	end

	def local_delivery(params)
		update_state("local_delivery", params)
	end

	def international_delivery(params)
		update_state("international_delivery", params)
	end

	def take_back_complete(params)
		update_state("update_state", params)
	end

	def take_back_cancel(params)
		update_state("take_back_cancel", params)
	end

	def take_back_confirm(params)
		update_state("take_back_confirm", params)
	end

	def refund(params)
		update_state("refund", params)
	end

	def refund_hold(params)
		update_state("refund_hold", params)
	end



	def ship_complete(params)
		order_items = update_state("ship_complete", params)

		#수익 계산 후 저장
		order_items.each do |item|
			product = Product.with_deleted.find(item.product_id)
			category = ProductCategory.find(product.product_category_id)
			
			#수수료율
			charge_rate = get_charge(product, category)
			
			#셀러 수익
			seller_profit = product.price.to_i - (product.price.to_f * (charge_rate.to_f/100.to_f)).to_i
			
			#수익
			profit = (product.price.to_f * (charge_rate.to_f/100.to_f)).to_i
			
			#컬렉션 수익
			collection_profit = nil
			
			#컬렉션 수수료율
			collection_charge_rate = nil
			
			#컬렉션 소유자 아이디
			collection_owner_id = nil

			#최종수익
			final_profit = profit

			unless item.collection_item_id.nil?
				collection_item = CollectionItem.find(item.collection_item_id)
				collection_owner_id = collection_item.user.id
				collection_id = collection_item.collection_id
				collection_charge_rate = 20
				collection_profit = (profit.to_f * (collection_charge_rate.to_f/100.to_f)).to_i
				final_profit = profit - collection_profit
			end

			Profit.create(
				:product_id => product.id,
				:user_id => product.created_by_id,
				:order_id => item.order_id,
				:orders_item_id => item.id,
				:price => product.price,
				:charge_rate => charge_rate,
				:seller_profit => seller_profit,
				:profit => profit,
				:collection_id => collection_id,
				:collection_item_id => item.collection_item_id,
				:collection_owner_id => collection_owner_id,
				:collection_profit => collection_profit,
				:collection_charge_rate => collection_charge_rate,
				:final_profit => final_profit,
				:is_cancel => false

			)

		end
	end

	def take_back_receive(params)
		update_state("take_back_receive", params)
	end

	def take_back_request(params)
		update_state("take_back_request", params)
	end

	def take_back_complete(params)
		order_items = update_state("take_back_complete", params)

		order_items.each do |item|
			cancel_profit = Profit.find_by_orders_item_id(item.id)
			unless cancel_profit.nil?
				collection_profit = nil

				unless cancel_profit.collection_profit.nil?
					collection_profit = "-" + cancel_profit.collection_profit
				end

				Profit.create(
					:product_id => cancel_profit.product_id,
					:user_id => cancel_profit.user_id,
					:order_id => item.order_id,
					:orders_item_id => item.id,
					:price => cancel_profit.price,
					:charge_rate => cancel_profit.charge_rate,
					:seller_profit => "-" + cancel_profit.seller_profit,
					:profit => "-" + cancel_profit.profit,
					:collection_item_id => item.collection_item_id,
					:collection_owner_id => cancel_profit.collection_owner_id,
					:collection_profit => collection_profit,
					:collection_charge_rate => cancel_profit.collection_charge_rate,
					:final_profit => "-" + cancel_profit.final_profit,
					:is_cancel => true

				)
			end
		end
	end

	def get_charge(product, category)
			
		product_charge_ranges = category.product_charge_ranges
		
		charge_rate = 10

		if product_charge_ranges.count > 0 
			product_charge_ranges.each do |range|
				from_price = range.from_price.to_i
				to_price = range.to_price.to_i
				product_price = product.price.to_i

				if product_price >= from_price and product_price < to_price
					charge_rate = range.charge
					break
				end
			end
		else
			if category.charge.nil?
				unless category.root?
					charge_rate = get_charge(product, category.parent)
				end
				
			else
				charge_rate = category.charge.to_i
			end
		end

		charge_rate
	end

	

	def update_state(state, params)
		orders_item = OrdersItem.find(params[:id])
		options = orders_item.options.to_s.gsub('{', '').gsub('}', '') if orders_item.options

		orders_items = OrdersItem.where(
			:product_id => orders_item.product_id,
			:options => options,
			:order_id => orders_item.order_id,
			:state => orders_item.state
		)

		orders_items.each do |item|
			eval("item.#{state}")
			ChangeState.create(
				:state => item.state,
				:state_name => item.human_state_name,
				:desc => params[:desc],
				:orders_item_id => item.id
			)
		end

		
		# if state == 'order_cancel' || state == 'take_back_complete'
		# 	order_cancel_rollback(state, orders_item, cancel_amount)
		# end


		# if state == 'order_cancel'
		# 	if orders_item.product_option_id.present?
		# 		product_option = orders_item.product_option
		# 		product_option.amount = product_option.amount + cancel_amount
		# 		product_option.save
		# 	else
		# 		product = orders_item.product
		# 		product.amount = product.amount + cancel_amount
		# 		product.save
		# 	end
		# end

		orders_items

	end

	# def order_cancel_rollback(state, orders_item, cancel_amount)

	# 	product = Product.find(orders_item.product_id)
	
	# 	#주문 옵션 있는 경우
	# 	if orders_item.product_option_id.present?
	# 		product_option = orders_item.product_option
	# 		#주문한 상품의 옵션이 존재할 경우
	# 		unless product_option.nil?
	# 			product_option.amount = product_option.amount + cancel_amount
	# 			product_option.save
	# 		end
	# 	else
	# 		#주문 옵션이 없을 경우
	# 		product = orders_item.product
	# 		product.amount = product.amount + cancel_amount
	# 		product.save
	# 	end
	
	# end


end