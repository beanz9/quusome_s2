require 'zipcode_import'

namespace :db do
	
	desc "import zipcode"
	task :import_zipcode => :environment do
		ZipcodeImport.load_save
	end
end