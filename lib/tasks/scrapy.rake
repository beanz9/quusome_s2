require 'scrapy_no_image_del'
require 'deleted_product_delete_image'
require 'product_image_size_update'
require 'soldout_check'

namespace :db do
	task :scrapy_no_image_del => :environment do
		ScrapyNoImageDel.execute
	end

	task :deleted_product_delete_image => :environment do
		DeletedProductDeleteImage.execute
	end

	task :product_image_size_update => :environment do
		ProductImageSizeUpdate.execute
	end

	task :soldout_check => :environment do
		SoldoutCheck.execute
	end
end